package endpoint;

import information.WikipediaArticle;
import org.jetbrains.annotations.NotNull;
import rendering.WikipediaGraphRenderModel;
import rendering.WikipediaGraphRenderer;
import subscription.Notifiable;
import wikipedia.WikipediaGraphEnvironment;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BackendEndpoint {
    private final WikipediaGraphRenderer graphRenderer;
    private final WikipediaGraphEnvironment graphEnvironment;
    private final List<Notifiable> subscribers;

    public BackendEndpoint(@NotNull WikipediaGraphEnvironment graphEnvironment) {
        this.graphRenderer = new WikipediaGraphRenderer(graphEnvironment);
        this.graphEnvironment = graphEnvironment;
        this.subscribers = new ArrayList<>();
    }

    public void start(){
        graphEnvironment.start();
    }

    public void stop(){
        graphEnvironment.stop();
    }

    public WikipediaGraphRenderModel getCurrentRender(){
        return graphRenderer.getCurrentRender();
    }

    public List<WikipediaArticle> findArticlesByTitle(@NotNull String title){
        return graphEnvironment.findArticlesByTitle(title);
    }

    public List<String> getTitlesOfLoadedArticles(){
        return graphEnvironment.getLoadedArticles().stream().map(WikipediaArticle::getTitle).collect(Collectors.toList());
    }

    public List<WikipediaArticle> getLoadedArticles(){
        return graphEnvironment.getLoadedArticles();
    }

    public void show(@NotNull String title){
        graphEnvironment.show(title);
    }
    public void show(int id){
        graphEnvironment.show(id);
    }

    public void loadArticlesByTitle(@NotNull String title){
        graphEnvironment.loadArticleByTitle(title);
    }
    public void loadArticleByID(int id){
        graphEnvironment.loadArticleByID(id);
    }

    public void hide(@NotNull String title){
        graphEnvironment.hide(title);
    }
    public void hide(int id){
        graphEnvironment.hide(id);
    }



    public WikipediaGraphRenderer getGraphRenderer(){
        return graphRenderer;
    }

    public void subscribeToGraphRenderer(@NotNull Notifiable subscriber){
        graphRenderer.subscribe(subscriber);
    }

}
