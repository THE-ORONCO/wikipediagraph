package rendering;

public interface RenderedVertex {
    public int getX();

    public int getY();

    public int getSize();
}
