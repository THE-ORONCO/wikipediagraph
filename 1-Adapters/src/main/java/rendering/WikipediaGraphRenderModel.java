package rendering;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class WikipediaGraphRenderModel {
    private final List<RenderedWikipediaVertex> renderedVertices;
    private final List<RenderedWikipediaEdge> renderedEdges;


    /**
     * This object represents a rendered view of a graph.
     *
     * @param renderedVertices all Vertices that will be rendered
     * @param renderedEdges    all Edges that will be rendered
     */
    public WikipediaGraphRenderModel(@NotNull List<RenderedWikipediaVertex> renderedVertices,
                                     @NotNull List<RenderedWikipediaEdge> renderedEdges) {
        this.renderedVertices = renderedVertices;
        this.renderedEdges = renderedEdges;
    }

    public List<RenderedWikipediaVertex> getRenderedVertices() {
        return renderedVertices;
    }

    public List<RenderedWikipediaEdge> getRenderedEdges() {
        return renderedEdges;
    }
}
