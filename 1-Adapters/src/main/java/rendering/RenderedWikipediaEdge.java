package rendering;

import information.WikipediaEdge;

public class RenderedWikipediaEdge implements RenderedEdge {
    private final WikipediaEdge edge;

    /**
     * This class functions as a wrapper for a wikipedia edge. It automatically formats the coordinates and only gives
     * write access to the variables inside the vertex.
     *
     * @param edge actual edge that is represented by this object
     */
    public RenderedWikipediaEdge(WikipediaEdge edge) {
        this.edge = edge;
    }

    public int getOriginX() {
        return Math.round(edge.getOriginX());
    }

    public int getOriginY() {
        return Math.round(edge.getOriginY());
    }

    public int getTargetX() {
        return Math.round(edge.getTargetX());
    }

    public int getTargetY() {
        return Math.round(edge.getTargetY());
    }

    public int getWidth() {
        return Math.round(edge.getWeight());
    }
}
