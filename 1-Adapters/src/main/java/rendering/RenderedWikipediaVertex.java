package rendering;

import information.WikipediaArticle;
import information.WikipediaVertex;

public class RenderedWikipediaVertex implements RenderedVertex {
    private final WikipediaVertex vertex;

    /**
     * This class functions as a wrapper for a wikipedia vertex. It automatically formats the coordinates and only gives
     * write access to the variables inside the vertex.
     *
     * @param vertex actual vertex that is represented by this object
     */
    public RenderedWikipediaVertex(WikipediaVertex vertex) {
        this.vertex = vertex;
    }

    public int getX() {
        return Math.round(vertex.getX());
    }

    public int getY() {
        return Math.round(vertex.getY());
    }

    public int getSize() {
        return Math.round(vertex.getWeight());
    }

    public WikipediaArticle getAssociatedArticle() {
        return vertex.getAssociatedArticle();
    }

    public int getArticleID() {
        return vertex.getArticleId();
    }

    public String getArticleTitle() {
        return vertex.getArticleTitle();
    }
}
