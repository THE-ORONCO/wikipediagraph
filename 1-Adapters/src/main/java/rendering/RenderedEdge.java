package rendering;

public interface RenderedEdge {
    public int getOriginX();

    public int getOriginY();

    public int getTargetX();

    public int getTargetY();

    public int getWidth();
}
