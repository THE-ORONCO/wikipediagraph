package rendering;

import org.jetbrains.annotations.NotNull;
import subscription.Notifiable;
import subscription.Subscribable;
import wikipedia.WikipediaGraphEnvironment;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class WikipediaGraphRenderer implements Notifiable, Subscribable {
    private final WikipediaGraphEnvironment graphEnvironment;
    private WikipediaGraphRenderModel currentRender;
    private final List<Notifiable> subscribedViewPorts;

    public WikipediaGraphRenderer(@NotNull WikipediaGraphEnvironment graphEnvironment) {
        this.graphEnvironment = graphEnvironment;
        this.subscribedViewPorts = new ArrayList<>();
        graphEnvironment.getGraph().subscribe(this);
        this.render();
    }

    public void render() {
        List<RenderedWikipediaVertex> renderedVertices = graphEnvironment.getAllVertices().stream()
                .map(RenderedWikipediaVertex::new).collect(Collectors.toList());
        List<RenderedWikipediaEdge> renderedEdges = graphEnvironment.getAllEdges().stream()
                .map(RenderedWikipediaEdge::new).collect(Collectors.toList());

        this.currentRender = new WikipediaGraphRenderModel(renderedVertices, renderedEdges);
    }


    public WikipediaGraphRenderModel getCurrentRender() {
        return currentRender;
    }

    /**
     * This method should be used to notify the renderer of a topological change in the graph so that it renders the
     * graph again and notifies all watching components of the change in the rendered model.
     */
    @Override
    public void sendNotification() {
        render();
        notifyAllViewPorts();
    }

    @Override
    public void subscribe(Notifiable notifiable) {
        subscribedViewPorts.add(notifiable);
    }

    @Override
    public void unsubscribe(Notifiable notifiable) {
        subscribedViewPorts.remove(notifiable);
    }

    private void notifyAllViewPorts() {
        subscribedViewPorts.forEach(Notifiable::sendNotification);
    }
}
