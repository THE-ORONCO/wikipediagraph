package simulation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import physics.Simulatable;

import static org.mockito.Mockito.*;

class SimulationRunnerTest {

    @Mock
    private Simulatable simulatableMock;
    private final long timeStep = 20;
    private SimulationRunner testedSimulation;
    private final int ThreeTimes = 3;

    @BeforeEach
    void setup() {
        simulatableMock = mock(Simulatable.class);
        testedSimulation = new SimulationRunner(simulatableMock);
    }

    @Test
    @DisplayName("test if the simulation can be stepped through")
    void testStep() {
        testedSimulation = new SimulationRunner(simulatableMock);
        testedSimulation.step();
        verify(simulatableMock, times(1)).step();
    }

    @Test
    @DisplayName("test if the simulation can be started and stopped")
    void testStartStop() {
        runSimulationFor(timeStep);
        verify(simulatableMock, atLeastOnce()).step();
    }

    @Test
    @DisplayName("test if the simulation doesn't break when stopping without starting")
    void testStopWithoutStart() {
        testedSimulation.stop();
        testedSimulation.stop();
        testedSimulation.stop();
        verify(simulatableMock, never()).step();
        runSimulationFor(timeStep);
        verify(simulatableMock, atLeastOnce()).step();
    }

    @Test
    @DisplayName("test if multiple starts and stops don't confuse the simulation")
    void testMultipleStartStop() {
        verify(simulatableMock, never()).step();
        runSimulationFor(timeStep);
        verify(simulatableMock, atLeastOnce()).step();

        runSimulationFor(timeStep * 2);
        verify(simulatableMock, atLeast(ThreeTimes)).step();
    }

    private void runSimulationFor(long timeStep) {
        this.testedSimulation.start();
        try {
            Thread.sleep(timeStep);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        testedSimulation.stop();
    }
}