package wikipedia.factories;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import wikipedia.query.ArticleIdentifier;
import wikipedia.query.types.WikipediaQuery;
import wikipedia.query.types.WikipediaQueryInterface;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class WikipediaQueryFactoryTest {
    private WikipediaQueryFactory testedFactory;
    private List<String> listOfValidTitles;
    private List<Integer> listOfValidIDs;

    @BeforeEach
    void setUp() {
        testedFactory = new WikipediaQueryFactory();
        listOfValidTitles = new ArrayList<>() {{
            add("Albert Einstein");
            add("Duck");
            add("Tree");
        }};
        listOfValidIDs = new ArrayList<>() {{
            add(736);
            add(37674);
            add(18955875);
        }};
    }

    @Test
    @DisplayName("test if a query with a valid title is created correctly")
    void testCreateValidTitleQuery() {
        String validTitle = "Albert Einstein";
        WikipediaQueryInterface createdQuery = testedFactory.createTitleQuery(validTitle);
        assertNotNull(createdQuery);
        assertEquals(createdQuery.getArticleIdentifier(), ArticleIdentifier.TITLE);
        assertTrue(createdQuery.getIdentifierValues().contains(validTitle));
    }

    @Test
    @DisplayName("test if a query with an empty title is still created")
    void testCreateEmptyTitleQuery() {
        String emptyTitle = "";
        WikipediaQueryInterface createdQuery = testedFactory.createTitleQuery(emptyTitle);
        assert createdQuery instanceof WikipediaQuery;
        assertEquals(createdQuery.getArticleIdentifier(), ArticleIdentifier.TITLE);
        assertTrue(createdQuery.getIdentifierValues().contains(emptyTitle));

    }

    @Test
    @DisplayName("test if a query for multiple Titles can be created")
    void testCreateMultipleTitleQuery() {
        WikipediaQueryInterface createdQuery = testedFactory.createTitleQuery(listOfValidTitles);
        assert createdQuery instanceof WikipediaQuery;
        assertEquals(createdQuery.getArticleIdentifier(), ArticleIdentifier.TITLE);
        for (String validTitle : listOfValidTitles)
            assertTrue(createdQuery.getIdentifierValues().contains(validTitle));
    }

    @Test
    @DisplayName("test if a query with a valid String id is created correctly")
    void testCreateStringIDQuery() {
        String validStringID = "736";
        WikipediaQueryInterface createdQuery = testedFactory.createIDQuery(validStringID);
        assert createdQuery instanceof WikipediaQuery;
        assertEquals(createdQuery.getArticleIdentifier(), ArticleIdentifier.PAGE_ID);
        assertTrue(createdQuery.getIdentifierValues().contains(validStringID));

    }

    @Test
    @DisplayName("test if a query with a valid int id is created correctly")
    void testCreateIntIDQuery() {
        int validIntID = 736;
        WikipediaQueryInterface createdQuery = testedFactory.createIDQuery(validIntID);
        assert createdQuery instanceof WikipediaQuery;
        assertEquals(createdQuery.getArticleIdentifier(), ArticleIdentifier.PAGE_ID);
        assertTrue(createdQuery.getIdentifierValues().contains(String.valueOf(validIntID)));
    }

    @Test
    @DisplayName("test if a query with multiple IDs can be created")
    void testCreateMultipleIDQuery() {
        WikipediaQueryInterface createdQuery = testedFactory.createIDQuery(listOfValidIDs);
        assert createdQuery instanceof WikipediaQuery;
        assertEquals(createdQuery.getArticleIdentifier(), ArticleIdentifier.PAGE_ID);
        for (int validTitle : listOfValidIDs)
            assertTrue(createdQuery.getIdentifierValues().contains(String.valueOf(validTitle)));
    }
}