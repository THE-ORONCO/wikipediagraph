package wikipedia;

import information.WikipediaArticle;
import information.WikipediaArticleMerger;
import information.WikipediaEdge;
import information.WikipediaVertex;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import wikipedia.query.WikipediaQueryEngine;
import wikipedia.result.WikipediaQueryResult;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

class WikipediaGraphEnvironmentTest {
    // TODO these tests seem to fail sometimes because of concurrency stuff

    private final int searchedID = 123;
    private final String searchedTitle = "cool title";
    @Mock
    private WikipediaGraph graphMock;
    @Mock
    private WikipediaArticleMerger articleMergerMock;
    @Mock
    private WikipediaQueryEngine queryEngineMock;
    @Mock
    private WikipediaArticle articleWithSearchedID;
    @Mock
    private WikipediaVertex vertexWithSearchedID;
    @Mock
    private WikipediaVertex vertexWithSearchedTitle;
    @Mock
    private WikipediaArticle articleWithSearchedTitle;
    @Mock
    private final WikipediaArticle article1ThatLinksToArticle2 = mock(WikipediaArticle.class);
    private final String article1Title = "Article 1";
    @Mock
    private final WikipediaVertex article1Vertex = mock(WikipediaVertex.class);
    @Mock
    private final WikipediaArticle article2ThatLinksToArticle1 = mock(WikipediaArticle.class);
    private final String article2Title = "Article 2";
    @Mock
    private final WikipediaVertex article2Vertex = mock(WikipediaVertex.class);


    private List<WikipediaArticle> articlesWithSearchedID;
    private List<WikipediaArticle> articlesWithSearchedTitle;

    private WikipediaGraphEnvironment testedGraphEnvironment;

    @BeforeEach
    void setup() {
        articleWithSearchedID = mock(WikipediaArticle.class);
        when(articleWithSearchedID.getPageID()).thenReturn(searchedID);

        vertexWithSearchedID = mock(WikipediaVertex.class);
        when(vertexWithSearchedID.getAssociatedArticle()).thenReturn(articleWithSearchedID);

        articleWithSearchedTitle = mock(WikipediaArticle.class);
        when(articleWithSearchedTitle.getTitle()).thenReturn(searchedTitle);

        vertexWithSearchedTitle = mock(WikipediaVertex.class);
        when(vertexWithSearchedTitle.getAssociatedArticle()).thenReturn(articleWithSearchedTitle);


        graphMock = mock(WikipediaGraph.class);
        articleMergerMock = mock(WikipediaArticleMerger.class);
        queryEngineMock = mock(WikipediaQueryEngine.class);

        WikipediaQueryResult idSearchResult = mock(WikipediaQueryResult.class);
        articlesWithSearchedID = Collections.singletonList(articleWithSearchedID);
        when(idSearchResult.getArticles()).thenReturn(articlesWithSearchedID);
        when(queryEngineMock.queryArticleByID(searchedID)).thenReturn(idSearchResult);

        WikipediaQueryResult titleSearchResult = mock(WikipediaQueryResult.class);
        articlesWithSearchedTitle = Collections.singletonList(articleWithSearchedTitle);
        when(titleSearchResult.getArticles()).thenReturn(articlesWithSearchedTitle);
        when(queryEngineMock.queryArticleByTitle(searchedTitle)).thenReturn(titleSearchResult);

        WikipediaQueryResult article1SearchResult = mock(WikipediaQueryResult.class);
        when(article1SearchResult.getArticles()).thenReturn(Collections.singletonList(article1ThatLinksToArticle2));
        when(queryEngineMock.queryArticleByTitle(article1Title)).thenReturn(article1SearchResult);

        WikipediaQueryResult article2SearchResult = mock(WikipediaQueryResult.class);
        when(article2SearchResult.getArticles()).thenReturn(Collections.singletonList(article2ThatLinksToArticle1));
        when(queryEngineMock.queryArticleByTitle(article2Title)).thenReturn(article2SearchResult);

        testedGraphEnvironment = new WikipediaGraphEnvironment(graphMock, articleMergerMock, queryEngineMock);
    }

    @AfterEach
    void teardown() {
        testedGraphEnvironment.stop();
    }

    // TODO tests

    @Test
    @DisplayName("test if articles can be loaded with only an id")
    void loadArticleByID() {
        testedGraphEnvironment.loadArticleByID(searchedID);
        verify(queryEngineMock, atLeastOnce()).queryArticleByID(searchedID);
        assertEquals(testedGraphEnvironment.getLoadedArticles().size(), 1);
        assertTrue(testedGraphEnvironment.getLoadedArticles().contains(articleWithSearchedID));
    }

    @Test
    @DisplayName("test if articles can be loaded with only a title")
    void loadArticleByTitle() {
        testedGraphEnvironment.loadArticleByTitle(searchedTitle);
        verify(queryEngineMock, atLeastOnce()).queryArticleByTitle(searchedTitle);
        assertEquals(testedGraphEnvironment.getLoadedArticles().size(), 1);
        assertTrue(testedGraphEnvironment.getLoadedArticles().contains(articleWithSearchedTitle));
    }

    @Test
    void removeArticleByTitle() {
    }

    @Test
    void removeArticleByID() {
    }

    @Test
    void findArticleByTitle() {
    }

    @Test
    void findArticleByID() {
    }

    @Test
    @DisplayName("test if an article can be shown when an id is given and the article is not in graph")
    void testShowByID() {
        testedGraphEnvironment.loadArticleByID(searchedID);
        testedGraphEnvironment.show(searchedID);
        verify(graphMock, atLeastOnce()).add(any(WikipediaVertex.class));
    }

    @Test
    @DisplayName("test if an article can be shown when an id is given and the article is already in graph")
    void testShowByIDWithAlreadyShownArticle() {
        when(graphMock.findArticleByID(searchedID)).thenReturn(Optional.of(articleWithSearchedID));
        when(graphMock.findVertexWithID(searchedID)).thenReturn(Optional.of(vertexWithSearchedID));
        when(articleMergerMock.mergeArticles(articleWithSearchedID, articleWithSearchedID)).thenReturn(articleWithSearchedID);

        testedGraphEnvironment.loadArticleByID(searchedID);
        testedGraphEnvironment.show(searchedID);

        verify(articleMergerMock, atLeastOnce()).mergeArticles(articleWithSearchedID, articleWithSearchedID);
        verify(graphMock, never()).add(any(WikipediaVertex.class));
    }

    @Test
    @DisplayName("test if an article can be shown when an title is given and the article is not in graph")
    void testShowByTitle() {
        testedGraphEnvironment.loadArticleByTitle(searchedTitle);
        testedGraphEnvironment.show(searchedTitle);
        verify(graphMock, atLeastOnce()).add(any(WikipediaVertex.class));
    }

    @Test
    @DisplayName("test if an article can be shown when an id is given and the article is already in graph")
    void testShowByTitleWithAlreadyShownArticle() {
        when(graphMock.findArticlesByTitle(searchedTitle)).thenReturn(articlesWithSearchedTitle);
        when(graphMock.findVerticesWithTitle(searchedTitle)).thenReturn(Collections.singletonList(vertexWithSearchedTitle));
        when(graphMock.findArticleByID(searchedID)).thenReturn(Optional.of(articleWithSearchedTitle));
        when(graphMock.findVertexWithID(searchedID)).thenReturn(Optional.of(vertexWithSearchedTitle));
        when(articleMergerMock.mergeArticles(articleWithSearchedTitle, articleWithSearchedTitle)).thenReturn(articleWithSearchedTitle);
        when(articleWithSearchedTitle.getPageID()).thenReturn(searchedID);

        testedGraphEnvironment.loadArticleByTitle(searchedTitle);
        testedGraphEnvironment.show(searchedTitle);

        verify(articleMergerMock, atLeastOnce()).mergeArticles(articleWithSearchedTitle, articleWithSearchedTitle);
        verify(graphMock, never()).add(any(WikipediaVertex.class));
    }

    @Test
    @DisplayName("test if edges between vertices are created when they are shown and links exist between them")
    void testShowEdgesCreation() {
        when(article1ThatLinksToArticle2.getTitlesOfLinkedArticles()).thenReturn(Collections.singletonList(article2Title));
        when(article1ThatLinksToArticle2.getTitle()).thenReturn(article1Title);
        when(article1Vertex.getArticleTitle()).thenReturn(article1Title);

        when(article2ThatLinksToArticle1.getTitlesOfLinkedArticles()).thenReturn(Collections.singletonList(article1Title));
        when(article2ThatLinksToArticle1.getTitle()).thenReturn(article2Title);
        when(article2Vertex.getArticleTitle()).thenReturn(article2Title);

        when(graphMock.findArticlesByTitle(article1Title)).thenReturn(Collections.singletonList(article1ThatLinksToArticle2));
        when(graphMock.findVerticesWithTitle(article1Title)).thenReturn(Collections.singletonList(article1Vertex));
        when(graphMock.findArticlesByTitle(article2Title)).thenReturn(Collections.singletonList(article2ThatLinksToArticle1));
        when(graphMock.findVerticesWithTitle(article2Title)).thenReturn(Collections.singletonList(article2Vertex));

        testedGraphEnvironment.loadArticleByTitle(article1Title);
        testedGraphEnvironment.loadArticleByTitle(article2Title);

        testedGraphEnvironment.show(article1Title);
        testedGraphEnvironment.show(article2Title);

        verify(graphMock, times(2)).add(any(WikipediaEdge.class));
        for (WikipediaEdge createdEdge : testedGraphEnvironment.getAllEdges()) {
            assertTrue(createdEdge.getOriginVertex().equals(article1Vertex) && createdEdge.getTargetVertex().equals(article2Vertex));
            assertTrue(createdEdge.getOriginVertex().equals(article2Vertex) && createdEdge.getTargetVertex().equals(article1Vertex));
        }
    }

    // TODO test articles can be removed from graph with hide

    @Test
    void hide() {
    }

    @Test
    void testHide() {
    }
}