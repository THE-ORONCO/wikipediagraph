package wikipedia;

import information.WikipediaArticle;
import information.WikipediaEdge;
import information.WikipediaVertex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import subscription.Notifiable;
import wikipedia.factories.WikipediaGraphFactory;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


class WikipediaGraphTest {

    private WikipediaGraph testedGraph;
    private WikipediaGraphFactory graphFactory;
    @Mock
    private WikipediaArticle targetArticle;
    @Mock
    private WikipediaArticle articleWithSameIdAsTargetArticle;
    @Mock
    private WikipediaArticle articleWithSameTitleAsTargetArticle;
    @Mock
    private WikipediaArticle completelyDifferentArticle;

    @Mock
    private WikipediaVertex targetVertex;
    @Mock
    private WikipediaVertex vertexWithSameID;
    @Mock
    private WikipediaVertex vertexWithSameTitle;
    @Mock
    private WikipediaVertex completelyDifferentVertex;

    @Mock
    private Notifiable notifiableMock = mock(Notifiable.class);

    private final String targetTitle = "Title 1";
    private final String differentTitle1 = "Title 2";
    private final String sameTitle = "Title 1";
    private final String differentTitle2 = "Title 4";
    private final String[] titles = {targetTitle, differentTitle1, sameTitle, differentTitle2};
    private final int targetID = 1;
    private final int sameID = 1;
    private final int differentID1 = 3;
    private final int differentID2 = 4;
    private final int missingArticleID = 736;
    private final int[] articleIds = {targetID, sameID, differentID1, differentID2};


    private List<WikipediaVertex> listWithTargetArticle;
    private List<WikipediaVertex> listWithTwoCompletelyDifferentArticles;
    private List<WikipediaVertex> listWithUniquelyTitledArticles;
    private List<WikipediaVertex> listWithMultipleArticlesWithTheSameTitle;
    private List<WikipediaVertex> listWithUniquelyIDdArticles;
    private List<WikipediaEdge> emptyEdgeList;

    @BeforeEach
    void setup() {
        graphFactory = new WikipediaGraphFactory();

        emptyEdgeList = new ArrayList<>();

        targetArticle = mock(WikipediaArticle.class);
        articleWithSameIdAsTargetArticle = mock(WikipediaArticle.class);
        articleWithSameTitleAsTargetArticle = mock(WikipediaArticle.class);
        completelyDifferentArticle = mock(WikipediaArticle.class);

        targetVertex = mock(WikipediaVertex.class);
        vertexWithSameID = mock(WikipediaVertex.class);
        vertexWithSameTitle = mock(WikipediaVertex.class);
        completelyDifferentVertex = mock(WikipediaVertex.class);

        List<WikipediaArticle> listOfAllMockedArticles = new ArrayList<>(Arrays.asList(
                targetArticle, articleWithSameIdAsTargetArticle, articleWithSameTitleAsTargetArticle, completelyDifferentArticle));
        List<WikipediaVertex> listOfAllMockedVertices = new ArrayList<>(Arrays.asList(
                targetVertex, vertexWithSameID, vertexWithSameTitle, completelyDifferentVertex));

        for (int i = 0; i < articleIds.length; i++) {
            WikipediaArticle currentArticle = listOfAllMockedArticles.get(i);
            WikipediaVertex currentVertex = listOfAllMockedVertices.get(i);

            when(currentArticle.getTitle()).thenReturn(titles[i]);
            when(currentArticle.getPageID()).thenReturn(articleIds[i]);

            when(currentVertex.getArticleTitle()).thenReturn(titles[i]);
            when(currentVertex.getArticleId()).thenReturn(articleIds[i]);
            when(currentVertex.getAssociatedArticle()).thenReturn(currentArticle);
        }

        listWithTargetArticle = new ArrayList<>(Collections.singletonList(targetVertex));
        listWithTwoCompletelyDifferentArticles = new ArrayList<>(Arrays.asList(targetVertex, completelyDifferentVertex));
        listWithUniquelyTitledArticles = new ArrayList<>(Arrays.asList(targetVertex, completelyDifferentVertex));
        listWithUniquelyIDdArticles = new ArrayList<>(Arrays.asList(targetVertex, vertexWithSameTitle, completelyDifferentVertex));
        listWithMultipleArticlesWithTheSameTitle = new ArrayList<>(Arrays.asList(targetVertex, vertexWithSameTitle, completelyDifferentVertex));
    }

    @Test
    @DisplayName("test if articles can be found by their title")
    void testFindArticlesByTitle() {
        testedGraph = graphFactory.create(listWithUniquelyTitledArticles, emptyEdgeList);
        List<WikipediaArticle> foundArticles = testedGraph.findArticlesByTitle(targetTitle);

        assertEquals(foundArticles.size(), 1);
        assertTrue(foundArticles.contains(targetArticle));
        assertFalse(foundArticles.contains(completelyDifferentArticle));
    }

    @Test
    @DisplayName("test if articles can be found by their title")
    void testFindMultipleArticlesByTitle() {
        testedGraph = graphFactory.create(listWithMultipleArticlesWithTheSameTitle, emptyEdgeList);
        List<WikipediaArticle> foundArticles = testedGraph.findArticlesByTitle(targetTitle);

        assertEquals(foundArticles.size(), 2);
        assertTrue(foundArticles.contains(targetArticle));
        assertTrue(foundArticles.contains(articleWithSameTitleAsTargetArticle));
        assertFalse(foundArticles.contains(completelyDifferentArticle));
    }

    @Test
    @DisplayName("test if an article can be found by their ID")
    void testFindArticleByID() {
        testedGraph = graphFactory.create(listWithTwoCompletelyDifferentArticles, emptyEdgeList);
        Optional<WikipediaArticle> foundArticle = testedGraph.findArticleByID(targetID);
        assertTrue(foundArticle.isPresent());
        assertEquals(foundArticle.get(), targetArticle);
    }

    @Test
    @DisplayName("test if an article can't be found if it doesn't exist")
    void testFindMissingArticleByID() {
        testedGraph = graphFactory.create(listWithTwoCompletelyDifferentArticles, emptyEdgeList);
        Optional<WikipediaArticle> foundArticle = testedGraph.findArticleByID(missingArticleID);
        assertTrue(foundArticle.isEmpty());
    }

    @Test
    @DisplayName("test if an article can be removed")
    void testRemoveArticle() {
        testedGraph = graphFactory.create(listWithTargetArticle, emptyEdgeList);
        assertEquals(testedGraph.findArticlesByTitle(targetTitle).size(), 1);
        testedGraph.removeArticle(completelyDifferentArticle);
        assertEquals(testedGraph.findArticlesByTitle(targetTitle).size(), 1);
        testedGraph.removeArticle(targetArticle);
        assertEquals(testedGraph.findArticlesByTitle(targetTitle).size(), 0);
    }

    @Test
    @DisplayName("test if an article can be removed by its title")
    void testRemoveByTitle() {
        testedGraph = graphFactory.create(listWithTargetArticle, emptyEdgeList);
        assertEquals(testedGraph.findArticlesByTitle(targetTitle).size(), 1);
        testedGraph.removeByTitle(differentTitle1);
        assertEquals(testedGraph.findArticlesByTitle(targetTitle).size(), 1);
        testedGraph.removeByTitle(targetTitle);
        assertEquals(testedGraph.findArticlesByTitle(targetTitle).size(), 0);
    }

    @Test
    @DisplayName("test if an article can be removed by its ID")
    void testRemoveByID() {
        testedGraph = graphFactory.create(listWithTargetArticle, emptyEdgeList);
        assertTrue(testedGraph.findArticleByID(targetID).isPresent());
        testedGraph.removeByID(differentID1);
        assertTrue(testedGraph.findArticleByID(targetID).isPresent());
        testedGraph.removeByID(targetID);
        assertTrue(testedGraph.findArticleByID(targetID).isEmpty());
    }

    @Test
    @DisplayName("test notification of subscribers when the topology of the graph is changed")
    void testNotificationOnTopologyChange() {
        testedGraph = graphFactory.create();
        testedGraph.subscribe(notifiableMock);
        verify(notifiableMock, times(0)).sendNotification();
        testedGraph.add(targetVertex);
        verify(notifiableMock, times(1)).sendNotification();
        testedGraph.add(completelyDifferentVertex);
        verify(notifiableMock, times(2)).sendNotification();
        testedGraph.remove(completelyDifferentVertex);
        verify(notifiableMock, times(3)).sendNotification();
        testedGraph.findVertexWithID(targetID);
        verify(notifiableMock, times(3)).sendNotification();
    }
}