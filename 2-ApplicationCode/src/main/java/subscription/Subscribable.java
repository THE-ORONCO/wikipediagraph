package subscription;

public interface Subscribable {
    public void subscribe(Notifiable notifiable);
    public void unsubscribe(Notifiable notifiable);
}
