package subscription;

public interface Notifiable {
    public void sendNotification();
}
