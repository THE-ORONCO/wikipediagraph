package wikipedia.factories;

import information.WikipediaArticle;
import wikipedia.result.GeneralWikipediaQueryResult;
import wikipedia.result.WikipediaQueryResult;

import java.util.Collection;

public class WikipediaQueryResultFactory {
    public WikipediaQueryResultFactory() {
    }

    public WikipediaQueryResult createFrom(Collection<WikipediaArticle> articles) {
        return new GeneralWikipediaQueryResult(articles);
    }
}
