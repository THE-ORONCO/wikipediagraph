package wikipedia.factories;

import information.WikipediaEdge;
import information.WikipediaVertex;
import org.jetbrains.annotations.NotNull;
import wikipedia.WikipediaGraph;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class WikipediaGraphFactory {

    public WikipediaGraph create() {
        List<WikipediaVertex> emptyVertexList = new ArrayList<>();
        List<WikipediaEdge> emptyEdgeList = new ArrayList<>();
        return create(emptyVertexList, emptyEdgeList);
    }

    public WikipediaGraph create(@NotNull List<WikipediaVertex> vertices,
                                 @NotNull List<WikipediaEdge> edges) {
        List<WikipediaVertex> missingVertices = findAllVerticesContainedInEdgesButNotContainedInVertexList(vertices, edges);
        vertices.addAll(missingVertices);
        return new WikipediaGraph(vertices, edges);
    }

    private List<WikipediaVertex> findAllVerticesContainedInEdgesButNotContainedInVertexList(List<WikipediaVertex> vertices, List<WikipediaEdge> edges) {
        Set<WikipediaVertex> missedVertices = new HashSet<>();
        for (WikipediaEdge edge : edges) {
            WikipediaVertex originVertex = edge.getOriginVertex();
            WikipediaVertex targetVertex = edge.getTargetVertex();
            if (!vertices.contains(originVertex))
                missedVertices.add(originVertex);
            if (!vertices.contains(targetVertex))
                missedVertices.add(targetVertex);
        }
        return new ArrayList<>(missedVertices);
    }
}
