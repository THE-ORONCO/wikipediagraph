package wikipedia.factories;

import information.WikipediaArticleMerger;
import org.jetbrains.annotations.NotNull;
import wikipedia.WikipediaGraph;
import wikipedia.WikipediaGraphEnvironment;
import wikipedia.query.WikipediaQueryEngine;

public class WikipediaGraphEnvironmentFactory {
    private final WikipediaGraphFactory DEFAULT_GRAPH_FACTORY = new WikipediaGraphFactory();
    private final WikipediaArticleMerger DEFAULT_ARTICLE_MERGER = new WikipediaArticleMerger();

    public WikipediaGraphEnvironment create(@NotNull WikipediaQueryEngine queryEngine) {
        return create(DEFAULT_GRAPH_FACTORY.create(), DEFAULT_ARTICLE_MERGER, queryEngine);
    }

    public WikipediaGraphEnvironment create(@NotNull WikipediaGraph graph,
                                            @NotNull WikipediaArticleMerger articleMerger,
                                            @NotNull WikipediaQueryEngine queryEngine) {
        return new WikipediaGraphEnvironment(graph, articleMerger, queryEngine);
    }
}
