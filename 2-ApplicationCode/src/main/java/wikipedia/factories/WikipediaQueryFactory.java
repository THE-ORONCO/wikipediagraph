package wikipedia.factories;

import org.jetbrains.annotations.NotNull;
import wikipedia.query.ArticleIdentifier;
import wikipedia.query.types.WikipediaQuery;
import wikipedia.query.types.WikipediaQueryInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class WikipediaQueryFactory {
    private final static ArticleIdentifier TITLE_IDENTIFIER = ArticleIdentifier.TITLE;
    private final static ArticleIdentifier ID_IDENTIFIER = ArticleIdentifier.PAGE_ID;

    public WikipediaQueryInterface createTitleQuery(@NotNull String title) {
        List<String> titleList = new ArrayList<>(Collections.singletonList(title));
        return new WikipediaQuery(TITLE_IDENTIFIER, titleList, new HashMap<>());
    }

    public WikipediaQueryInterface createTitleQuery(@NotNull List<String> titleList) {
        return new WikipediaQuery(TITLE_IDENTIFIER, titleList, new HashMap<>());
    }

    public WikipediaQueryInterface createIDQuery(int articleID) {
        List<String> titleList = new ArrayList<>(Collections.singletonList(String.valueOf(articleID)));
        return new WikipediaQuery(ID_IDENTIFIER, titleList, new HashMap<>());
    }

    public WikipediaQueryInterface createIDQuery(@NotNull String articleID) {
        List<String> titleList = new ArrayList<>(Collections.singletonList(articleID));
        return new WikipediaQuery(ID_IDENTIFIER, titleList, new HashMap<>());
    }

    public WikipediaQueryInterface createIDQuery(@NotNull List<Integer> articleIDList) {
        List<String> stringIDList = articleIDList.stream().map(String::valueOf).collect(Collectors.toList());
        return new WikipediaQuery(ID_IDENTIFIER, stringIDList, new HashMap<>());
    }
}
