package wikipedia.query;

import wikipedia.query.types.WikipediaQueryInterface;
import wikipedia.result.WikipediaQueryResult;

public interface QueryExecutor {
    public WikipediaQueryResult execute(WikipediaQueryInterface query);

}
