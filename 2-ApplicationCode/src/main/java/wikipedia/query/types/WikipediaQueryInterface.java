package wikipedia.query.types;

import wikipedia.query.ArticleIdentifier;

import java.util.List;
import java.util.Map;

public interface WikipediaQueryInterface {
    Map<String, String> getExtraParameters();
    ArticleIdentifier getArticleIdentifier();
    List<String> getIdentifierValues();
}
