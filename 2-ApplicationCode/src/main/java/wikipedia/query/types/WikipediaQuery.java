package wikipedia.query.types;

import wikipedia.query.ArticleIdentifier;

import java.util.List;
import java.util.Map;

public class WikipediaQuery implements WikipediaQueryInterface {
    private final ArticleIdentifier identifier;
    private final List<String> identifierValues;
    private final Map<String, String> extraParameters;

    public WikipediaQuery(ArticleIdentifier identifier, List<String> identifierValues, Map<String, String> extraParameters) {
        this.identifier = identifier;
        this.identifierValues = identifierValues;
        this.extraParameters = extraParameters;
    }

    @Override
    public Map<String, String> getExtraParameters() {
        return extraParameters;
    }

    @Override
    public ArticleIdentifier getArticleIdentifier() {
        return identifier;
    }

    @Override
    public List<String> getIdentifierValues() {
        return identifierValues;
    }
}
