package wikipedia.query;

public enum ArticleIdentifier {
    TITLE, PAGE_ID;
}
