package wikipedia.query;

import org.jetbrains.annotations.NotNull;
import wikipedia.factories.WikipediaQueryFactory;
import wikipedia.query.types.WikipediaQueryInterface;
import wikipedia.result.WikipediaQueryResult;

public class WikipediaQueryEngine {
    private QueryExecutor queryExecutor;
    private WikipediaQueryFactory queryFactory;

    public WikipediaQueryEngine(@NotNull QueryExecutor queryExecutor, @NotNull WikipediaQueryFactory queryFactory) {
        this.queryExecutor = queryExecutor;
        this.queryFactory = queryFactory;
    }

    public WikipediaQueryResult queryArticleByTitle(@NotNull String title) {
        WikipediaQueryInterface newQuery = queryFactory.createTitleQuery(title);
        return queryExecutor.execute(newQuery);
    }

    public WikipediaQueryResult queryArticleByID(int articleID) {
        WikipediaQueryInterface newQuery = queryFactory.createIDQuery(articleID);
        return queryExecutor.execute(newQuery);
    }


    public QueryExecutor getQueryExecutor() {
        return queryExecutor;
    }

    public void setQueryExecutor(QueryExecutor queryExecutor) {
        this.queryExecutor = queryExecutor;
    }

    public WikipediaQueryFactory getQueryFactory() {
        return queryFactory;
    }

    public void setQueryFactory(WikipediaQueryFactory queryFactory) {
        this.queryFactory = queryFactory;
    }
}
