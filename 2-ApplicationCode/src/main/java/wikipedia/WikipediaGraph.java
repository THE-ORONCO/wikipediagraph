package wikipedia;

import information.WikipediaArticle;
import information.WikipediaEdge;
import information.WikipediaVertex;
import org.jetbrains.annotations.NotNull;
import physics.ForceDirectedGraph;
import subscription.Notifiable;
import subscription.Subscribable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class WikipediaGraph extends ForceDirectedGraph<WikipediaVertex, WikipediaEdge> implements Subscribable {
    private final List<Notifiable> subscribedObjects;

    public WikipediaGraph(@NotNull List<WikipediaVertex> vertices,
                          @NotNull List<WikipediaEdge> edges) {
        super(vertices, edges);
        this.subscribedObjects = new ArrayList<>();
    }

    @Override
    public boolean add(@NotNull WikipediaVertex vertex) {
        // TODO make it that adding the same vertex multiple times decreases its weight
        boolean topologyChanged = super.add(vertex);
        if (topologyChanged) notifyAllSubscribers();
        return topologyChanged;
    }

    @Override
    public boolean add(@NotNull WikipediaEdge edge) {
        // TODO make it that adding the same vertex multiple times increases its weight (spring force)
        boolean topologyChanged = super.add(edge);
        if (topologyChanged) notifyAllSubscribers();
        return topologyChanged;
    }

    @Override
    public boolean remove(@NotNull WikipediaVertex vertex) {
        boolean topologyChanged = super.remove(vertex);
        if (topologyChanged) notifyAllSubscribers();
        return topologyChanged;
    }
    @Override
    public boolean remove(@NotNull WikipediaEdge edge) {
        boolean topologyChanged = super.remove(edge);
        if (topologyChanged) notifyAllSubscribers();
        return topologyChanged;
    }

    public List<WikipediaArticle> findArticlesByTitle(@NotNull String title) {
        List<WikipediaArticle> articlesWithTitle = new ArrayList<>();
        for (WikipediaVertex vertex : findVerticesWithTitle(title)) {
            articlesWithTitle.add(vertex.getAssociatedArticle());
        }
        return articlesWithTitle;
    }

    public List<WikipediaVertex> findVerticesWithTitle(@NotNull String title) {
        List<WikipediaVertex> verticesWithTitle = new ArrayList<>();
        for (WikipediaVertex vertex : this.getVertices()) {
            if (vertex.getArticleTitle().equals(title)) {
                verticesWithTitle.add(vertex);
            }
        }
        return verticesWithTitle;
    }

    public Optional<WikipediaArticle> findArticleByID(int pageID) {
        Optional<WikipediaVertex> optionalVertexWithID = findVertexWithID(pageID);
        return optionalVertexWithID.map(WikipediaVertex::getAssociatedArticle);
    }

    public Optional<WikipediaVertex> findVertexWithID(int articleID) {
        for (WikipediaVertex vertex : this.getVertices()) {
            if (vertex.getArticleId() == articleID)
                return Optional.of(vertex);
        }
        return Optional.empty();
    }

    public void removeArticle(@NotNull WikipediaArticle article) {
        for (WikipediaVertex vertex : this.getVertices()) {
            if ((vertex).getAssociatedArticle().equals(article))
                this.remove(vertex);
        }
        notifyAllSubscribers();
    }

    public void removeByTitle(@NotNull String title) {
        List<WikipediaVertex> foundArticleVertices = findVerticesWithTitle(title);
        for (WikipediaVertex foundArticleVertex : foundArticleVertices) {
            this.remove(foundArticleVertex);
        }
        notifyAllSubscribers();
    }

    public void removeByID(int articleID) {
        Optional<WikipediaVertex> foundArticleVertex = findVertexWithID(articleID);
        foundArticleVertex.ifPresent(this::remove);
        notifyAllSubscribers();
    }

    public List<Integer> getAllIDs() {
        List<Integer> ids = new ArrayList<>();
        for (WikipediaVertex vertex : this.getVertices())
            ids.add(vertex.getArticleId());
        return ids;
    }

    public List<WikipediaArticle> getAllArticles() {
        return this.getVertices().stream().map(WikipediaVertex::getAssociatedArticle).collect(Collectors.toList());
    }

    @Override
    public void subscribe(Notifiable notifiable) {
        subscribedObjects.add(notifiable);
    }

    @Override
    public void unsubscribe(Notifiable notifiable) {
        subscribedObjects.remove(notifiable);
    }

    private void notifyAllSubscribers() {
        subscribedObjects.forEach(Notifiable::sendNotification);
    }


}
