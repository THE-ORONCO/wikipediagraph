package wikipedia;

import factories.PhysicsGraphSimulationFactory;
import factories.forcedirectedComponents.WikipediaGraphComponentFactory;
import information.WikipediaArticle;
import information.WikipediaArticleMerger;
import information.WikipediaEdge;
import information.WikipediaVertex;
import org.jetbrains.annotations.NotNull;
import physics.PhysicsGraphSimulation;
import simulation.SimulationRunner;
import wikipedia.query.WikipediaQueryEngine;
import wikipedia.result.WikipediaQueryResult;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WikipediaGraphEnvironment {
    private final SimulationRunner simulation;
    private final WikipediaGraph graph;
    private final WikipediaQueryEngine queryEngine;
    private Set<WikipediaArticle> loadedArticles;
    private final WikipediaGraphComponentFactory componentFactory;
    private final WikipediaArticleMerger articleMerger;

    private static Logger logger;

    public WikipediaGraphEnvironment(@NotNull WikipediaGraph graph,
                                     @NotNull WikipediaArticleMerger articleMerger,
                                     @NotNull WikipediaQueryEngine queryEngine) {
        logger = Logger.getLogger(WikipediaGraphEnvironment.class.getName());

        this.graph = graph;
        this.articleMerger = articleMerger;
        this.loadedArticles = new HashSet<>(graph.getAllArticles());
        this.queryEngine = queryEngine;
        this.componentFactory = new WikipediaGraphComponentFactory();

        PhysicsGraphSimulationFactory<WikipediaVertex, WikipediaEdge> simulationFactory = new PhysicsGraphSimulationFactory<>();
        PhysicsGraphSimulation<WikipediaVertex, WikipediaEdge> wrappedGraph = simulationFactory.create(graph);
        this.simulation = new SimulationRunner(wrappedGraph);
    }

    public void loadArticleByID(int articleID) {
        WikipediaQueryResult queryResult = queryEngine.queryArticleByID(articleID);
        loadedArticles.addAll(queryResult.getArticles());
        logger.info("added " + queryResult.getArticles().size() + " articles");
    }

    public void loadArticleByTitle(@NotNull String title) {
        WikipediaQueryResult queryResult = queryEngine.queryArticleByTitle(title);
        loadedArticles.addAll(queryResult.getArticles());
        logger.info("added " + queryResult.getArticles().size() + " articles");
    }

    public void removeArticleByTitle(@NotNull String title) {
        hide(title);
        loadedArticles = loadedArticles.stream().filter(article -> !article.getTitle().equals(title))
                .collect(Collectors.toSet());
    }

    public void removeArticleByID(int articleID) {
        hide(articleID);
        loadedArticles = loadedArticles.stream().filter(article -> article.getPageID() != articleID)
                .collect(Collectors.toSet());
    }

    public List<WikipediaArticle> findArticlesByTitle(@NotNull String title) {
        return loadedArticles.stream().filter(article -> article.getTitle().equals(title))
                .collect(Collectors.toList());
    }

    public Optional<WikipediaArticle> findArticleByID(int articleID) {
        for (WikipediaArticle article : loadedArticles)
            if (article.getPageID() == articleID)
                return Optional.of(article);
        return Optional.empty();
    }

    public void show(int articleID) {
        Optional<WikipediaArticle> shownArticle = findArticleByID(articleID);
        shownArticle.ifPresent(this::show);
    }

    public void show(@NotNull String title) {
        List<WikipediaArticle> shownArticles = findArticlesByTitle(title);
        shownArticles.forEach(this::show);
    }

    private void show(@NotNull WikipediaArticle article) {
        synchronized (graph) {
            WikipediaVertex associatedVertex = createOrMergeArticleVertex(article);
            List<WikipediaEdge> edgesResultingFromAddition = createAllEdgesThatAreResultOfTheAdditionOf(associatedVertex);
            edgesResultingFromAddition.forEach(graph::add);
        }
        logger.info("showing article \"" + article.getTitle() + "\"");
    }

    private WikipediaVertex createOrMergeArticleVertex(@NotNull WikipediaArticle newArticle) {
        Optional<WikipediaVertex> optionalVertexWithSameID = graph.findVertexWithID(newArticle.getPageID());
        if (optionalVertexWithSameID.isEmpty()) {
            WikipediaVertex createdVertex = componentFactory.createVertexWithRandomOffset(newArticle);
            graph.add(createdVertex);
            return createdVertex;
        } else {
            WikipediaVertex existingVertex = optionalVertexWithSameID.get();
            WikipediaArticle mergedArticle = articleMerger.mergeArticles(existingVertex.getAssociatedArticle(), newArticle);
            existingVertex.setAssociatedArticle(mergedArticle);
            return existingVertex;
        }
    }

    private List<WikipediaEdge> createAllEdgesThatAreResultOfTheAdditionOf(@NotNull WikipediaVertex vertexOfArticle) {
        List<WikipediaVertex> linksToVertices = findAllVerticesThatAreLinkedToBy(vertexOfArticle);
        List<WikipediaEdge> edgesFromArticle
                = linksToVertices.stream()
                .map(linkedVertex -> componentFactory.createEdge(vertexOfArticle, linkedVertex))
                .collect(Collectors.toList());

        List<WikipediaVertex> linksFromVertices = findAllVerticesThatWouldLinkTo(vertexOfArticle);
        List<WikipediaEdge> edgesToArticle
                = linksFromVertices.stream()
                .map(linkingVertex -> componentFactory.createEdge(linkingVertex, vertexOfArticle))
                .collect(Collectors.toList());
        return Stream.of(edgesFromArticle, edgesToArticle).flatMap(Collection::stream).collect(Collectors.toList());
    }

    private List<WikipediaVertex> findAllVerticesThatAreLinkedToBy(@NotNull WikipediaVertex vertex) {
        List<WikipediaVertex> verticesGivenArticleLinksTo = new ArrayList<>();
        for (String title : vertex.getAssociatedArticle().getTitlesOfLinkedArticles())
            verticesGivenArticleLinksTo.addAll(graph.findVerticesWithTitle(title));
        return verticesGivenArticleLinksTo;
    }

    private List<WikipediaVertex> findAllVerticesThatWouldLinkTo(@NotNull WikipediaVertex vertex) {
        List<WikipediaVertex> verticesThatWouldLinkToGivenVertex = new ArrayList<>();
        for (WikipediaVertex otherVertex : graph.getVertices()) {
            if (otherVertex.getAssociatedArticle().getTitlesOfLinkedArticles().contains(vertex.getArticleTitle())) {
                verticesThatWouldLinkToGivenVertex.add(otherVertex);
            }
        }
        return verticesThatWouldLinkToGivenVertex;
    }

    public void hide(String title) {
        synchronized (graph) {
            graph.removeByTitle(title);
        }
        logger.info("hiding article \"" + title + "\"");
    }

    public void hide(int articleID) {
        synchronized (graph) {
            graph.removeByID(articleID);
        }
        logger.info("hiding article \"" + articleID + "\"");
    }

    public void stop() {
        simulation.stop();
    }

    public void start() {
        simulation.start();
    }

    public List<WikipediaVertex> getAllVertices() {
        return graph.getVertices(); // this would look nicer if WPG would extend FDG
    }

    public List<WikipediaEdge> getAllEdges() {
        return graph.getEdges();
    }

    public List<WikipediaArticle> getLoadedArticles() {
        return new ArrayList<>(loadedArticles);
    }

    public WikipediaGraph getGraph() {
        return graph;
    }
}
