package wikipedia.result;

import information.WikipediaArticle;

import java.util.List;

public interface WikipediaQueryResult {
    List<WikipediaArticle> getArticles();
}
