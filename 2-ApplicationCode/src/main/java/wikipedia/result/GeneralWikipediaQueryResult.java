package wikipedia.result;

import information.WikipediaArticle;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GeneralWikipediaQueryResult implements WikipediaQueryResult{
    private final List<WikipediaArticle> articles;

    public GeneralWikipediaQueryResult(@NotNull Collection<WikipediaArticle> articles) {
        this.articles = new ArrayList<>(articles);

    }

    @Override
    public List<WikipediaArticle> getArticles() {
        return articles;
    }

}
