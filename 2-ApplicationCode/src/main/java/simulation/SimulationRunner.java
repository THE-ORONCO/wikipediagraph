package simulation;

import physics.Simulatable;

import java.util.logging.Logger;

public class SimulationRunner {
    private final Simulatable simulatableObject;
    private boolean running;
    private Thread runnerThread;

    public SimulationRunner(Simulatable simulatableObject) {
        this.simulatableObject = simulatableObject;
        running = false;
    }

    public void start() {
        if (!running) {
            running = true;

            SimulationThread simulationThread = new SimulationThread(simulatableObject);
            runnerThread = new Thread(simulationThread);
            runnerThread.start();
            Logger.getLogger(SimulationRunner.class.getName()).info("Physics simulation started in parallel thread.");
        }
    }

    public void stop() {
        if (running) {
            Logger.getLogger(SimulationRunner.class.getName()).info("Physics simulation stopped.");
            this.running = false;
            try {
                runnerThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void step() {
        this.simulatableObject.step();
    }

    private class SimulationThread implements Runnable {
        private final Simulatable simulatable;

        private SimulationThread(Simulatable simulatable) {
            this.simulatable = simulatable;
        }

        @Override
        public void run() {
            while (running) {
                synchronized (simulatable) {
                    simulatable.step();
                }
            }
        }

    }
}