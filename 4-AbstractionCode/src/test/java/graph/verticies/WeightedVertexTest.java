package graph.verticies;

import geometry.Vector;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class WeightedVertexTest {

    Vector positionedAtOrigin = new Vector(0f, 0f);
    Vector positionedAtOneOne = new Vector(1f, 1f);
    Vector positionedAtMinusThreeMinusFive = new Vector(-3f, -5f);
    Vector positionedAtTwoTwo = new Vector(2f, 2f);

    float withDefaultWeight = 5f;

    @Test
    @DisplayName("test if vertices are created correctly")
    public void testVertexCreation() {
        Vector position = new Vector(0f, 0f);
        float weight = 5f;
        WeightedVertex vertexCreatedWithoutCoordinatesOrWeigth = new WeightedVertex(position, weight);
        assertEquals(vertexCreatedWithoutCoordinatesOrWeigth.getX(), position.getX());
        assertEquals(vertexCreatedWithoutCoordinatesOrWeigth.getY(), position.getY());
        assertEquals(vertexCreatedWithoutCoordinatesOrWeigth.getWeight(), weight);

        WeightedVertex vertexCreatedAt1_2WithAMassOf3 = new WeightedVertex(1f, 2f, 3f);
        assertEquals(vertexCreatedAt1_2WithAMassOf3.getX(), 1f);
        assertEquals(vertexCreatedAt1_2WithAMassOf3.getY(), 2f);
        assertEquals(vertexCreatedAt1_2WithAMassOf3.getWeight(), 3f);
    }

    @Test
    @DisplayName("test if translation of a vertex works as expected")
    public void testTranslate() {
        Vector originalPosition = new Vector(1f, 1f);
        Vector translationVector = new Vector(23f, 3.5f);
        Vector newPositionAfterTranslation = originalPosition.add(translationVector);

        WeightedVertex vertexToBeTranslatedByNumbers = new WeightedVertex(originalPosition, withDefaultWeight);
        vertexToBeTranslatedByNumbers.translate(translationVector.getX(), translationVector.getY());
        assertEquals(vertexToBeTranslatedByNumbers.getPosition(), newPositionAfterTranslation);

        WeightedVertex vertexToBeTranslatedByVector = new WeightedVertex(originalPosition, withDefaultWeight);
        vertexToBeTranslatedByVector.translate(translationVector);
        assertEquals(vertexToBeTranslatedByVector.getPosition(), newPositionAfterTranslation);
    }

    @Test
    @DisplayName("test if the distance between two vertices is calculated correctly")
    public void testDistanceTo() {
        float withinTolerance = 0.00001f;

        WeightedVertex vertexAtOrigin = new WeightedVertex(positionedAtOrigin, withDefaultWeight);
        WeightedVertex vertexAtOneOne = new WeightedVertex(positionedAtOneOne, withDefaultWeight);
        WeightedVertex vertexAtMinusThreeMinusFive = new WeightedVertex(positionedAtMinusThreeMinusFive, withDefaultWeight);
        WeightedVertex vertexAtTwoTwo = new WeightedVertex(positionedAtTwoTwo, withDefaultWeight);


        compare(vertexAtOrigin.distanceTo(vertexAtOneOne), toActualDistanceFromOriginToOneOne(), withinTolerance);
        compare(vertexAtOrigin.distanceTo(vertexAtTwoTwo), toActualDistanceFromOriginToTwoTwo(), withinTolerance);
        compare(vertexAtOneOne.distanceTo(vertexAtMinusThreeMinusFive), toActualDistanceFromOneOneToMinusThreeMinusFive(), withinTolerance);

    }

    private float toActualDistanceFromOneOneToMinusThreeMinusFive() {
        return positionedAtOneOne.subtract(positionedAtMinusThreeMinusFive).length();
    }

    private float toActualDistanceFromOriginToTwoTwo() {
        return positionedAtTwoTwo.subtract(positionedAtOrigin).length();
    }

    private float toActualDistanceFromOriginToOneOne() {
        return positionedAtOneOne.subtract(positionedAtOrigin).length();
    }

    private void compare(float calculatedDistance, float comparisonValue, float tolerance) {
        assert (comparisonValue - tolerance <= calculatedDistance);
        assert (calculatedDistance <= comparisonValue + tolerance);
    }

    @Test
    @DisplayName("test if the vector between two vertices is calculated correctly")
    void testVectorialDistanceTo() {
        WeightedVertex vertexAtOrigin = new WeightedVertex(positionedAtOrigin, withDefaultWeight);
        WeightedVertex vertexAtOneOne = new WeightedVertex(positionedAtOneOne, withDefaultWeight);
        WeightedVertex vertexAtMinusThreeMinusFive = new WeightedVertex(positionedAtMinusThreeMinusFive, withDefaultWeight);
        WeightedVertex vertexAtTwoTwo = new WeightedVertex(positionedAtTwoTwo, withDefaultWeight);

        compareTheVectors(vertexAtOrigin.vectorialDistanceTo(vertexAtOneOne), toActualVectorFromOriginToOneOne());
        compareTheVectors(vertexAtOrigin.vectorialDistanceTo(vertexAtTwoTwo), toActualVectorFromOriginToTwoTwo());
        compareTheVectors(vertexAtOneOne.vectorialDistanceTo(vertexAtMinusThreeMinusFive), toActualVectorFromOneOneToMinusThreeMinusFive());
    }

    private Vector toActualVectorFromOriginToOneOne() {
        return positionedAtOneOne.subtract(positionedAtOrigin);
    }

    private Vector toActualVectorFromOriginToTwoTwo() {
        return positionedAtTwoTwo.subtract(positionedAtOrigin);
    }

    private Vector toActualVectorFromOneOneToMinusThreeMinusFive() {
        return positionedAtMinusThreeMinusFive.subtract(positionedAtOneOne);
    }

    private void compareTheVectors(Vector vector1, Vector vector2) {
        assertEquals(vector1, vector2);
    }

    @Test
    @DisplayName("test if the kinetic energy is calculated as expected")
    void testKineticEnergyCalculation() {
        WeightedVertex lightVertex = new WeightedVertex(positionedAtOrigin, 0.01f);
        WeightedVertex heavyVertex = new WeightedVertex(positionedAtOrigin, 1000f);

        Vector velocityInXDirectionWithMagnitudeOfTwo = new Vector(2f, 0);
        lightVertex.setVelocity(velocityInXDirectionWithMagnitudeOfTwo);
        heavyVertex.setVelocity(velocityInXDirectionWithMagnitudeOfTwo);

        assert lightVertex.getKineticEnergy() < heavyVertex.getKineticEnergy();
    }
}