package graph.edges;

import geometry.Vector;
import graph.verticies.WeightedVertex;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class WeightedEdgeTest {
    Vector originPosition = new Vector(0f, 0f);
    Vector positionAtOneOne = new Vector(1f, 1f);
    float DEFAULT_WEIGHT = 1f;

    @Mock
    WeightedVertex originVertexMock = mock(WeightedVertex.class);
    WeightedVertex oneOneVertexMock = mock(WeightedVertex.class);

    @Test
    @DisplayName("test if edges with default weight are created correctly")
    public void testCreateWeightedEdgeDefault() {
        when(originVertexMock.getPosition()).thenReturn(originPosition);
        when(oneOneVertexMock.getPosition()).thenReturn(positionAtOneOne);

        WeightedEdge<WeightedVertex> createdEdge = new WeightedEdge<>(originVertexMock, oneOneVertexMock, DEFAULT_WEIGHT);
        assertEquals(createdEdge.getOriginVertex().getPosition(), originPosition);
        assertEquals(createdEdge.getTargetVertex().getPosition(), positionAtOneOne);

        float defaultWeight = 1f;
        assertEquals(createdEdge.getWeight(), defaultWeight);
    }

    @Test
    @DisplayName("test if edges with a specific weight are created correctly")
    public void testCreateWeightedEdge() {
        when(originVertexMock.getPosition()).thenReturn(originPosition);
        when(oneOneVertexMock.getPosition()).thenReturn(positionAtOneOne);
        float weight = 5f;
        WeightedEdge<WeightedVertex> createdEdgeWithTheWeightOfFive = new WeightedEdge<>(originVertexMock, oneOneVertexMock, weight);
        assertEquals(createdEdgeWithTheWeightOfFive.getOriginVertex().getPosition(), originPosition);
        assertEquals(createdEdgeWithTheWeightOfFive.getTargetVertex().getPosition(), positionAtOneOne);

        assertEquals(createdEdgeWithTheWeightOfFive.getWeight(), weight);
    }

    @Test
    @DisplayName("test if the vertex length is calculated correctly")
    public void testLength() {
        when(originVertexMock.getPosition()).thenReturn(originPosition);
        when(oneOneVertexMock.getPosition()).thenReturn(positionAtOneOne);
        WeightedEdge<WeightedVertex> edgeFromOriginToOneOne = new WeightedEdge<>(originVertexMock, oneOneVertexMock, DEFAULT_WEIGHT);

        float actualDistanceBetweenTheVertices =
                oneOneVertexMock.getPosition().subtract(originVertexMock.getPosition())
                        .length();

        when(originVertexMock.distanceTo(oneOneVertexMock)).thenReturn((float) Math.sqrt(2));
        float lengthOfEdge = edgeFromOriginToOneOne.length();

        float calculationTolerance = 0.0001f;
        assertTrue(actualDistanceBetweenTheVertices - calculationTolerance <= lengthOfEdge &&
                lengthOfEdge <= actualDistanceBetweenTheVertices + calculationTolerance);
    }


    @Test
    @DisplayName("test if the vector pointing from the origin vertex to the target vertex is calculated correctly")
    void testEdgeDirectionVector() {
        when(originVertexMock.getPosition()).thenReturn(originPosition);
        when(oneOneVertexMock.getPosition()).thenReturn(positionAtOneOne);
        WeightedEdge<WeightedVertex> edgeFromOriginToOneOne = new WeightedEdge<>(originVertexMock, oneOneVertexMock, DEFAULT_WEIGHT);

        Vector expectedEdgeDirectionVector = oneOneVertexMock.getPosition().subtract(originVertexMock.getPosition());

        Vector actualEdgeDirectionVector = edgeFromOriginToOneOne.edgeDirectionVector();

        assertEquals(expectedEdgeDirectionVector, actualEdgeDirectionVector);
    }

    @Test
    @DisplayName("test if the coordinates of the connected vertices can be retrieved")
    void testGetXYOfVertices() {
        WeightedVertex originVertexMock = mock(WeightedVertex.class);
        WeightedVertex targetVertexMock = mock(WeightedVertex.class);
        float originXPosition = 0f;
        float originYPosition = 1f;
        float targetXPosition = 2f;
        float targetYPosition = 3f;
        when(originVertexMock.getX()).thenReturn(originXPosition);
        when(originVertexMock.getY()).thenReturn(originYPosition);
        when(targetVertexMock.getX()).thenReturn(targetXPosition);
        when(targetVertexMock.getY()).thenReturn(targetYPosition);


        WeightedEdge<WeightedVertex> edge = new WeightedEdge<>(originVertexMock, targetVertexMock, 4f);

        assertEquals(edge.getOriginX(), originXPosition);
        assertEquals(edge.getOriginY(), originYPosition);
        assertEquals(edge.getTargetX(), targetXPosition);
        assertEquals(edge.getTargetY(), targetYPosition);
    }
}