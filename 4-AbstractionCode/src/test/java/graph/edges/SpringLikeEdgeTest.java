package graph.edges;

import graph.verticies.WeightedVertex;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class SpringLikeEdgeTest {

    @Mock
    private final WeightedVertex originVertex = mock(WeightedVertex.class);
    @Mock
    private final WeightedVertex targetVertex = mock(WeightedVertex.class);
    private final float weightOfOne = 5f;

    @Test
    @DisplayName("test if an SpringLikeEdge can be created")
    void testEdgeCreation() {
        float andNaturalSpringLengthOfOne = 1f;
        SpringLikeEdge<WeightedVertex> createdEdge =
                new SpringLikeEdge<>(originVertex, targetVertex, weightOfOne, andNaturalSpringLengthOfOne);
        assertEquals(createdEdge.getNaturalSpringLength(), andNaturalSpringLengthOfOne);
        assertEquals(createdEdge.getOriginVertex(), originVertex);
        assertEquals(createdEdge.getTargetVertex(), targetVertex);
        assertEquals(createdEdge.getWeight(), weightOfOne);
    }

    @Test
    @DisplayName("test if an edge with negative natural spring length can't be created without exception")
    void testNegativeNaturalSpringLength() {
        float andNegativeNaturalSpringLength = -42f;
        assertThrows(IllegalArgumentException.class, () ->
                new SpringLikeEdge<>(originVertex, targetVertex, weightOfOne, andNegativeNaturalSpringLength)
        );
    }
}