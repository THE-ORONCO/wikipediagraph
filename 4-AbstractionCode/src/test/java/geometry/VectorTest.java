package geometry;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VectorTest {

    private final float APPROXIMATE_SQUARE_ROOT_OF_TWO = 1.4142135f;

    @Test
    @DisplayName("test if vectors can be created as expected")
    public void testVectorCreation() {
        Vector createdVector = new Vector(1f, 2f);
        assertEquals(createdVector.getX(), 1f);
        assertEquals(createdVector.getY(), 2f);
    }

    @Test
    @DisplayName("test if the length of vectors is calculated correctly")
    public void testVectorLength() {
        Vector horizontalVector = new Vector(0f, 3f);
        assertEquals(horizontalVector.length(), 3f);

        Vector slantingVector = new Vector(1f, 1f);
        float lengthOfSlantedVector = slantingVector.length();
        assertTrue(approximatelyEqual(lengthOfSlantedVector, APPROXIMATE_SQUARE_ROOT_OF_TWO));
    }

    @Test
    @DisplayName("test if the unit vector conversion of vectors works correctly")
    public void testToUnitVector() {
        Vector shortHorizontalVector = new Vector(0f, 2f);
        Vector longHorizontalVector = new Vector(0f, 6f);
        Vector unitVectorOfShortHorizontalVector = shortHorizontalVector.toUnitVector();
        Vector unitVectorOfLongHorizontalVector = longHorizontalVector.toUnitVector();
        assert unitVectorOfShortHorizontalVector.equals(unitVectorOfLongHorizontalVector);
        testIfGivenVectorIsAUnitVectorPointingToTheRight(unitVectorOfShortHorizontalVector);
        testIfGivenVectorIsAUnitVectorPointingToTheRight(unitVectorOfLongHorizontalVector);

        Vector slantedVector = new Vector(1f, 1f);
        Vector slantedUnitVector = slantedVector.toUnitVector();
        testIfGivenVectorIsAUnitVectorPointingToTheTopRight(slantedUnitVector);
    }

    private void testIfGivenVectorHasLengthOfOne(Vector vector){
       assert approximatelyEqual(vector.length(), 1f);
    }

    private void testIfGivenVectorIsAUnitVectorPointingToTheRight(Vector vector){
        testIfGivenVectorHasLengthOfOne(vector);
        assertEquals(vector.getX(), 0f);
        assertEquals(vector.getY(), 1f);
    }

    private void testIfGivenVectorIsAUnitVectorPointingToTheTopRight(Vector vector) {
        testIfGivenVectorHasLengthOfOne(vector);
        assertTrue(approximatelyEqual(vector.getX(), APPROXIMATE_SQUARE_ROOT_OF_TWO / 2));
        assertTrue(approximatelyEqual(vector.getY(), APPROXIMATE_SQUARE_ROOT_OF_TWO / 2));
    }

    private boolean approximatelyEqual(float value, float referenceValue) {
        float lowestAcceptableValue = referenceValue - (float) 1.0E-4;
        float largestAcceptableValue = referenceValue + (float) 1.0E-4;
        return lowestAcceptableValue <= value && value <= largestAcceptableValue;
    }


    @Test
    @DisplayName("test mathematical vector operation addition")
    void testAddingVectors() {
        Vector horizontalVector = new Vector(0f, 5f);
        Vector verticalVector = new Vector(3f, 0f);
        Vector diagonalVector = horizontalVector.add(verticalVector);
        assertEquals(diagonalVector, new Vector(3f, 5f));

        Vector diagonalVectorInNegativeDirection = new Vector(-10f, -50f);
        Vector diagonalVectorAddedToOtherDiagonalVector = diagonalVector.add(diagonalVectorInNegativeDirection);
        assertEquals(diagonalVectorAddedToOtherDiagonalVector, new Vector(-7f, -45f));
    }

    @Test
    @DisplayName("test mathematical vector operation division")
    void testDividedBy() {
        Vector horizontalVectorOfLengthTwo = new Vector(0f, 2f);
        Vector horizontalVectorOfLengthOne = new Vector(0f, 1f);
        assertEquals(horizontalVectorOfLengthOne, horizontalVectorOfLengthTwo.dividedBy(2f));
        assertEquals(horizontalVectorOfLengthOne.dividedBy(0.5f), horizontalVectorOfLengthTwo);
    }

    @Test
    @DisplayName("test if a division by zero throws an exception")
    void testForDivisionByZero(){
        Vector oneOneVector = new Vector(1f,1f);
        Throwable expectedException = assertThrows(ArithmeticException.class, () -> oneOneVector.dividedBy(0f));
        String messageOfTheExpectedException = expectedException.getMessage();
        assert(messageOfTheExpectedException.equals("divided by zero"));
    }

    @Test
    @DisplayName("test mathematical vector operation subtraction")
    void testSubtract() {
        Vector firstVector = new Vector(4f, 4f);
        Vector secondVector = new Vector(7f, 1f);
        Vector vectorFromEndpointOfFirstVectorToEndPointOfSecondVector = new Vector(3f, -3f);

        assertEquals(vectorFromEndpointOfFirstVectorToEndPointOfSecondVector, secondVector.subtract(firstVector));
    }

    @Test
    @DisplayName("test mathematical vector operation multiplication")
    void testMultipliedBy() {
        Vector horizontalUnitVector = new Vector(1f, 0f);
        Vector horizontalVectorOfLengthTwo = new Vector(2f, 0f);

        assertEquals(horizontalVectorOfLengthTwo, horizontalUnitVector.multipliedBy(2f));

        Vector diagonalVectorWithLengthSqrtOfTwo = new Vector(1f, 1f);
        Vector diagonalVectorWithLengthSqrtOfEight = new Vector(2f, 2f);

        assertEquals(diagonalVectorWithLengthSqrtOfEight, diagonalVectorWithLengthSqrtOfTwo.multipliedBy(2f));
    }
}