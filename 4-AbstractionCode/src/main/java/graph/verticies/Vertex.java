package graph.verticies;
import geometry.Vector;

public interface Vertex {

    public void setPositionTo(float x, float y);

    public void setPositionTo(Vector newPosition);

    public float getX();

    public float getY();

    public Vector getPosition();

    public void translate(float deltaX, float deltaY);

    public void translate(Vector translationVector);

    public float distanceTo(Vertex otherVertex);

    public Vector vectorialDistanceTo(Vertex otherVertex);
}
