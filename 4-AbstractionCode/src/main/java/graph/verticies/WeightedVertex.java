package graph.verticies;

import geometry.Vector;

public class WeightedVertex implements Vertex {

    private Vector position;
    private Vector velocity;
    private float weight;

    //SPECIAL "neutral" constructors that only take coordinates. That means other classes using weighted Vertex don't need to use Vector.
    public WeightedVertex(float x, float y, float weight) {
        this(new Vector(x, y), weight);
    }

    public WeightedVertex(Vector position, float weight) {
        this.position = position;
        this.weight = weight;
        this.velocity = new Vector(0, 0);
    }

    @Override
    public void setPositionTo(float x, float y) {
        this.position = new Vector(x, y);
    }

    @Override
    public void setPositionTo(Vector newPosition) {
        this.position = newPosition;
    }

    @Override
    public void translate(float deltaX, float deltaY) {
        Vector translationVector = new Vector(deltaX, deltaY);
        this.translate(translationVector);
    }

    @Override
    public void translate(Vector translationVector) {
        this.position = this.position.add(translationVector);
    }

    @Override
    public float distanceTo(Vertex otherVertex) {
        return otherVertex.getPosition().subtract(this.position).length();
    }

    @Override
    public Vector vectorialDistanceTo(Vertex otherVertex) {
        return otherVertex.getPosition().subtract(this.getPosition());
    }

    /**
     * this is probably not needed
     *
     * @return the magnitude of the kinetic energy
     */
    public float getKineticEnergy() {
        float absoluteVelocity = velocity.length();
        return (float) (0.5 * weight * absoluteVelocity * absoluteVelocity);
    }

    @Override
    public float getX() {
        return position.getX();
    }

    @Override
    public float getY() {
        return position.getY();
    }

    @Override
    public Vector getPosition() {
        return new Vector(this.getX(), this.getY());
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public Vector getVelocity() {
        return velocity;
    }

    public void setVelocity(Vector velocity) {
        this.velocity = velocity;
    }

    public void setVelocity(float dx, float dy) {
        this.velocity = new Vector(dx, dy);
    }

    @Override
    public String toString() {
        return "WeightedVertex{" +
                "position=" + position +
                ", velocity=" + velocity +
                ", weight=" + weight +
                '}';
    }
}
