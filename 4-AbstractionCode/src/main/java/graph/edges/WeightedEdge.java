package graph.edges;

import geometry.Vector;
import graph.verticies.WeightedVertex;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class WeightedEdge<VertexType extends WeightedVertex> implements Edge<VertexType> {

    @NotNull
    private VertexType originVertex;
    @NotNull
    private VertexType targetVertex;

    private float weight;

    public WeightedEdge(@NotNull VertexType originVertex, @NotNull VertexType targetVertex, float weight) {
        this.originVertex = originVertex;
        this.targetVertex = targetVertex;
        this.weight = weight;
    }

    @Override
    public float length() {
        return originVertex.distanceTo(targetVertex);
    }

    @NotNull
    public VertexType getOtherVertex(@NotNull WeightedVertex vertex)throws IllegalArgumentException{
        if (vertex.equals(originVertex))
            return targetVertex;
        else if (vertex.equals(targetVertex))
            return originVertex;
        else throw new IllegalArgumentException("Vertex not contained in edge.");
    }

    @Override
    public Vector edgeDirectionVector() {
        return targetVertex.getPosition().subtract(originVertex.getPosition());
    }

    @NotNull
    @Override
    public VertexType getOriginVertex() {
        return originVertex;
    }

    @Override
    public void setOriginVertex(@NotNull VertexType originVertex) {
        this.originVertex = originVertex;
    }

    @NotNull
    @Override
    public VertexType getTargetVertex() {
        return targetVertex;
    }

    @Override
    public float getOriginX() {
        return originVertex.getX();
    }

    @Override
    public float getOriginY() {
        return originVertex.getY();
    }

    @Override
    public float getTargetX() {
        return targetVertex.getX();
    }

    @Override
    public float getTargetY() {
        return targetVertex.getY();
    }

    @Override
    public void setTargetVertex(@NotNull VertexType targetVertex) {
        this.targetVertex = targetVertex;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WeightedEdge)) return false;
        WeightedEdge<?> that = (WeightedEdge<?>) o;
        return getOriginVertex().equals(that.getOriginVertex()) && getTargetVertex().equals(that.getTargetVertex());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOriginVertex(), getTargetVertex());
    }
}
