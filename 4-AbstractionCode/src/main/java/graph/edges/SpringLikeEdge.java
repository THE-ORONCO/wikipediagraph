package graph.edges;

import graph.verticies.WeightedVertex;
import org.jetbrains.annotations.NotNull;

public class SpringLikeEdge<VertexType extends WeightedVertex> extends WeightedEdge<VertexType> {

    private float naturalSpringLength;

    public SpringLikeEdge(@NotNull VertexType originVertex, @NotNull VertexType targetVertex, float weight, float naturalSpringLength) throws IllegalArgumentException{
        super(originVertex, targetVertex, weight);
        isBiggerThenZero(naturalSpringLength);
        this.naturalSpringLength = naturalSpringLength;
    }

    public float getNaturalSpringLength() {
        return naturalSpringLength;
    }

    public void setNaturalSpringLength(float naturalSpringLength) throws IllegalArgumentException{
        isBiggerThenZero(naturalSpringLength);
        this.naturalSpringLength = naturalSpringLength;
    }

    private void isBiggerThenZero(float springLength) throws  IllegalArgumentException{
        if (springLength < 0) throw new IllegalArgumentException("The natural spring length must be bigger then zero");
    }
}
