package graph.edges;

import geometry.Vector;
import graph.verticies.Vertex;

public interface Edge<VertexType extends Vertex> {

    public VertexType getOriginVertex();

    public VertexType getTargetVertex();

    public float getOriginX();
    public float getOriginY();
    public float getTargetX();
    public float getTargetY();

    public void setOriginVertex(VertexType vertex);

    public void setTargetVertex(VertexType vertex);

    public float length();

    public Vector edgeDirectionVector();
}
