package graph;

import graph.edges.Edge;
import graph.verticies.Vertex;

import java.util.List;

public interface Graph<VertexType extends Vertex, EdgeType extends Edge<VertexType>> {
    public boolean add(VertexType vertex);
    public boolean add(EdgeType edge);

    public boolean remove(VertexType vertex);
    public boolean remove(EdgeType edge);

    public boolean contains(VertexType vertex);
    public boolean contains(EdgeType edge);

    public List<VertexType> getVertices();
    public List<EdgeType> getEdges();

    public List<EdgeType> findEdgesBetween(VertexType originVertex, VertexType targetVertex);
    public List<EdgeType> findAllEdgesConnectedTo(VertexType vertex);
    public List<EdgeType> findAllEdgesOriginatingFrom(VertexType originVertex);
    public List<EdgeType> findAllEdgesGoingTo(VertexType targetVertex);
    public List<VertexType> findAllNeighbouringVertices(VertexType vertex);

    public List<VertexType> findAllVerticesExceptFor(VertexType vertex);
}
