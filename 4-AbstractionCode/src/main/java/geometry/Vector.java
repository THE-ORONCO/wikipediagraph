package geometry;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

/**
 * This class represents a 2D vector as described in geometry. Thus all basic operations (addition, subtraction,
 * multiplication, division) can be used on it. Equality between vectors is given by their equal orientation and length.
 * When the defined operations are used on a given vector it is not modified but instead a new vector with the resulting
 * dimensions is created and returned thereby allowing multiple uses of different operations on a vector.
 */
public final class Vector {
    private final float x;
    private final float y;

    /**
     * creates a Vector with given x and y values
     * @param x x value of the vector
     * @param y y value of the vector
     */
    public Vector(float x, float y) {
        this.x = x;
        this.y = y;
    }

    /**
     * creates a Vector pointing to (0,0)
     */
    public Vector() {
        this(0f, 0f);
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float length() {
        return (float) Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    }

    /**
     * This Method calculates a unit vector pointing in the same direction as the vector it is used on.
     *
     * @return a Vector in the same direction as the original vector but with the length of one.
     */
    public Vector toUnitVector() {
        float vectorLength = this.length();
        return new Vector(x / vectorLength, y / vectorLength);
    }

    public Vector add(@NotNull Vector otherVector) {
        float newX = x + otherVector.getX();
        float newY = y + otherVector.getY();
        return new Vector(newX, newY);
    }

    public Vector subtract(@NotNull Vector otherVector) {
        float newX = x - otherVector.getX();
        float newY = y - otherVector.getY();
        return new Vector(newX, newY);
    }

    public Vector multipliedBy(float factor) {
        float newX = x * factor;
        float newY = y * factor;
        return new Vector(newX, newY);
    }

    public Vector dividedBy(float divisor) {
        if (divisor == 0) {
            throw new ArithmeticException("divided by zero");
        }
        float newX = this.x / divisor;
        float newY = this.y / divisor;
        return new Vector(newX, newY);
    }

    /**
     * The equality between vectors is defined by their equal length and orientation.
     *
     * @param o The other vector this vector should be compared to.
     * @return if this vector is equal to the other vector
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vector vector = (Vector) o;
        return Float.compare(vector.x, x) == 0 && Float.compare(vector.y, y) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "Vector{" + "x=" + x + ", y=" + y + '}';
    }
}
