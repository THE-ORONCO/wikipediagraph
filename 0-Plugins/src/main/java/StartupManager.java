import SwingGUIPlugin.ApplicationWindow;
import WikipediaPlugin.factories.WikipediaQueryEngineFactory;
import endpoint.BackendEndpoint;
import wikipedia.WikipediaGraphEnvironment;
import wikipedia.factories.WikipediaGraphEnvironmentFactory;
import wikipedia.query.WikipediaQueryEngine;

public class StartupManager {
    public static void main(String[] args) {
        WikipediaQueryEngine queryEngine;
        if (args.length == 1)
            queryEngine = new WikipediaQueryEngineFactory().create(args[0]);
        else
            queryEngine = new WikipediaQueryEngineFactory().create();

        WikipediaGraphEnvironment graphEnvironment = new WikipediaGraphEnvironmentFactory().create(queryEngine);
        BackendEndpoint backendEndpoint = new BackendEndpoint(graphEnvironment);

        ApplicationWindow framy = new ApplicationWindow(backendEndpoint);
    }
}
