package SwingGUIPlugin;

import endpoint.BackendEndpoint;
import org.jetbrains.annotations.NotNull;
import subscription.Notifiable;
import subscription.Subscribable;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class ArticleLoader extends JPanel implements Subscribable, Notifiable {
    private final List<Notifiable> subscribers;
    private final ArticleSearchBar searchBar;

    public ArticleLoader(@NotNull BackendEndpoint backendEndpoint) {
        this.setLayout(new BorderLayout());
        this.subscribers = new ArrayList<>();

        JList<String> loadedArticlesGUIElement = new JList<>(backendEndpoint.getTitlesOfLoadedArticles().toArray(new String[0]));
        JScrollPane scrollableLoadedArticles = new JScrollPane(loadedArticlesGUIElement,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollableLoadedArticles.setPreferredSize(new Dimension(300, -1));

        this.searchBar = new ArticleSearchBar(backendEndpoint, loadedArticlesGUIElement);
        this.searchBar.subscribe(this);

        JButton showArticlesButton = new JButton("show selected");
        JButton hideArticlesButton = new JButton("hide selected");

        JPanel showHideButtonsContainer = new JPanel(new GridLayout(1, 2));
        showHideButtonsContainer.add(showArticlesButton);
        showHideButtonsContainer.add(hideArticlesButton);

        JPanel interactablesContainer = new JPanel();
        GroupLayout interactablesLayout = new GroupLayout(interactablesContainer);
        interactablesContainer.setLayout(interactablesLayout);
        interactablesContainer.add(searchBar);
        interactablesContainer.add(showHideButtonsContainer);

        interactablesLayout.setVerticalGroup(
                interactablesLayout.createSequentialGroup()
                        .addComponent(searchBar)
                        .addComponent(showHideButtonsContainer));
        interactablesLayout.setHorizontalGroup(
                interactablesLayout.createSequentialGroup().addGroup(
                        interactablesLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                .addComponent(searchBar)
                                .addComponent(showHideButtonsContainer)));


        loadedArticlesGUIElement.setSelectionMode(DefaultListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        showArticlesButton.addActionListener(actionEvent -> {
            if (actionEvent.getSource() == showArticlesButton) {
                List<String> articlesToBeShown = loadedArticlesGUIElement.getSelectedValuesList();
                articlesToBeShown.forEach(backendEndpoint::show);
            }
        });

        hideArticlesButton.addActionListener(actionEvent -> {
            if (actionEvent.getSource() == hideArticlesButton) {
                List<String> articlesToBeShown = loadedArticlesGUIElement.getSelectedValuesList();
                articlesToBeShown.forEach(backendEndpoint::hide);
            }
        });


        this.add(interactablesContainer, BorderLayout.NORTH);
        this.add(scrollableLoadedArticles, BorderLayout.CENTER);

        this.setBorder(new LineBorder(Color.darkGray, 1, true));
        this.setSize(new Dimension(300, -1));
    }

    @Override
    public void subscribe(@NotNull Notifiable subscriber) {
        subscribers.add(subscriber);
    }

    @Override
    public void unsubscribe(@NotNull Notifiable subscriber) {
        subscribers.remove(subscriber);
    }

    @Override
    public void sendNotification() {
        subscribers.forEach(Notifiable::sendNotification);
    }

    public ArticleSearchBar getSearchBar() {
        return searchBar;
    }
}
