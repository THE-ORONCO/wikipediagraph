package SwingGUIPlugin;

import endpoint.BackendEndpoint;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Timer;
import java.util.TimerTask;

public class ApplicationWindow extends JFrame {

    private final WikipediaGraphViewport graphViewport;
    private final BackendEndpoint backendEndpoint;

    public ApplicationWindow(@NotNull BackendEndpoint backendEndpoint) {

        System.setProperty("awt.useSystemAAFontSettings", "on");

        JPanel startStopButtons = createStartStopResetButtons();


        this.backendEndpoint = backendEndpoint;
        graphViewport = new WikipediaGraphViewport(backendEndpoint);

        JPanel graphViewPortWrapper = new JPanel(new BorderLayout());
        graphViewPortWrapper.add(startStopButtons, BorderLayout.NORTH);
        graphViewPortWrapper.add(graphViewport, BorderLayout.CENTER);

        ArticleLoader articleLoader = new ArticleLoader(backendEndpoint);

        FocusedArticleRepresentation articleRepresentation = new FocusedArticleRepresentation(articleLoader.getSearchBar());


        ArticleChooserComboBox articleChooserComboBox = new ArticleChooserComboBox(backendEndpoint, articleRepresentation);
        articleLoader.subscribe(articleChooserComboBox);

        JPanel articleRepresentationContainer = new JPanel(new BorderLayout());
        articleRepresentationContainer.add(articleChooserComboBox, BorderLayout.NORTH);
        articleRepresentationContainer.add(articleRepresentation, BorderLayout.CENTER);
        articleRepresentationContainer.setBorder(new LineBorder(Color.black, 1, true));
        articleRepresentationContainer.setPreferredSize(new Dimension(300, -1));
        articleRepresentationContainer.setMaximumSize(articleRepresentationContainer.getPreferredSize());


        this.setLayout(new BorderLayout());
        this.add(articleRepresentationContainer, BorderLayout.WEST);
        this.add(graphViewPortWrapper, BorderLayout.CENTER);
        this.add(articleLoader, BorderLayout.EAST);

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                graphViewport.repaint();
            }
        }, 0, 33);

        this.setTitle("Graph Thingy");
        this.setMinimumSize(new Dimension(600, 400));
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.pack();
        this.setLocationByPlatform(true);

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                ApplicationWindow.this.backendEndpoint.stop();
                super.windowClosing(e);
                System.exit(0);
            }
        });
        this.setVisible(true);
    }

    private JPanel createStartStopResetButtons() {
        JButton startButton = new JButton("start");
        startButton.addActionListener(actionEvent -> backendEndpoint.start());
        JButton stopButton = new JButton("stop");
        stopButton.addActionListener(actionEvent -> backendEndpoint.stop());

        JButton resetButton = new JButton("reset");
        resetButton.addActionListener(actionEvent -> graphViewport.resetOffsetToOrigin());

        JPanel startStopResetButtonContainer = new JPanel(new GridLayout(1, 4));
        startStopResetButtonContainer.add(startButton);
        startStopResetButtonContainer.add(stopButton);
        startStopResetButtonContainer.add(new JPanel());
        startStopResetButtonContainer.add(resetButton);
        startStopResetButtonContainer.setBorder(new LineBorder(Color.black, 1, true));

        return startStopResetButtonContainer;
    }

}
