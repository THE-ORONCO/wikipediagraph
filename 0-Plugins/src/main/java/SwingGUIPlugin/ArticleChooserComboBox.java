package SwingGUIPlugin;

import endpoint.BackendEndpoint;
import information.WikipediaArticle;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.jetbrains.annotations.NotNull;
import subscription.Notifiable;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.Optional;

public class ArticleChooserComboBox extends JComboBox<String> implements Notifiable {

    private final BackendEndpoint backendEndpoint;
    private final FocusedArticleRepresentation articleRepresentation;

    public ArticleChooserComboBox(@NotNull BackendEndpoint backendEndpoint,
                                  @NotNull FocusedArticleRepresentation articleRepresentation) {
        super(backendEndpoint.getTitlesOfLoadedArticles().toArray(new String[0]));

        this.backendEndpoint = backendEndpoint;
        this.articleRepresentation = articleRepresentation;

        AutoCompleteDecorator.decorate(this);

        this.addActionListener(actionEvent -> updateArticleRepresentation());
        this.setPreferredSize(new Dimension(300, 20));
        this.setMaximumSize(this.getPreferredSize());
    }

    @Override
    public Dimension getMinimumSize() {
        return new Dimension(300, 20);
    }

    public void updateArticleRepresentation() {
        Optional<String> currentlySelectedTitle = Optional.ofNullable((String) this.getSelectedItem());
        if (currentlySelectedTitle.isPresent()) {
            String selectedTitle = currentlySelectedTitle.get();
            List<WikipediaArticle> foundArticles = backendEndpoint.findArticlesByTitle(selectedTitle);
            if (foundArticles.size() > 0)
                articleRepresentation.represent(foundArticles.get(0));
        }
    }

    public void updateSelectableArticles() {
        this.removeAllItems();
        backendEndpoint.getLoadedArticles().forEach(article -> this.addItem(article.getTitle()));
    }

    @Override
    public void sendNotification() {
        updateSelectableArticles();
    }
}
