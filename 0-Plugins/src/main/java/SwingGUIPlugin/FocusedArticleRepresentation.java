package SwingGUIPlugin;

import factories.WikipediaArticleFactory;
import information.WikipediaArticle;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class FocusedArticleRepresentation extends JPanel {
    private final JLabel idLabel;
    private final JLabel titleLabel;
    private final JTextArea articleExtractTextArea;
    private final JLabel thumbnailLabel;
    private final JPanel externalLinksPanel;
    private final JList<String> titlesThisArticleLinksTo;

    public FocusedArticleRepresentation(@NotNull ArticleSearchBar searchBar) {
        this(new WikipediaArticleFactory().create("Dummy article"), searchBar);
    }

    public FocusedArticleRepresentation(@NotNull WikipediaArticle representedArticle,
                                        @NotNull ArticleSearchBar searchBar) throws HeadlessException {

        Dimension defaultDimensions = new Dimension(300, -1);
        this.idLabel = new JLabel("-");
        this.titleLabel = new JLabel("---");
        JScrollPane scrollableTitle = new JScrollPane(titleLabel,
                JScrollPane.VERTICAL_SCROLLBAR_NEVER,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        this.articleExtractTextArea = new JTextArea("---");
        articleExtractTextArea.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 13));
        this.thumbnailLabel = new JLabel();
        this.externalLinksPanel = new JPanel();
        this.externalLinksPanel.setLayout(new GridLayout(0, 1));
        this.titlesThisArticleLinksTo = new JList<>();

        JButton loadSelectedArticlesButton = new JButton("load selected articles");
        loadSelectedArticlesButton.addActionListener(actionEvent -> {
            List<String> toBeLoadedArticles = titlesThisArticleLinksTo.getSelectedValuesList();
            toBeLoadedArticles.forEach(searchBar::trySearchingFor);
        });

        this.setPreferredSize(defaultDimensions);

        scrollableTitle.setPreferredSize(new Dimension(defaultDimensions.width, 10));

        JPanel idContainer = new JPanel();
        JLabel idLabel = new JLabel("id:");

        articleExtractTextArea.setLineWrap(true);
        articleExtractTextArea.setEditable(false);
        articleExtractTextArea.setWrapStyleWord(true);
        JScrollPane scrollableExtract = new JScrollPane(articleExtractTextArea,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollableExtract.setMaximumSize(new Dimension(defaultDimensions.width, 200));

        JScrollPane scrollableTitles = new JScrollPane(titlesThisArticleLinksTo,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollableTitles.setPreferredSize(new Dimension(defaultDimensions.width, 400));

        JScrollPane scrollableExternalLinks = new JScrollPane(externalLinksPanel,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollableExternalLinks.getVerticalScrollBar().setUnitIncrement(16);

        scrollableExternalLinks.setMaximumSize(new Dimension(defaultDimensions.width, 400));

        this.represent(representedArticle);

        this.add(scrollableTitle);
        idContainer.add(idLabel);
        idContainer.add(this.idLabel);
        this.add(idContainer);
        this.add(thumbnailLabel);
        this.add(scrollableExtract);
        this.add(loadSelectedArticlesButton);
        this.add(scrollableTitles);
        this.add(scrollableExternalLinks);


        GroupLayout idLayout = new GroupLayout(idContainer);
        idContainer.setLayout(idLayout);
        idLayout.setHorizontalGroup(
                idLayout.createSequentialGroup()
                        .addComponent(idLabel)
                        .addComponent(this.idLabel));
        idLayout.setVerticalGroup(
                idLayout.createSequentialGroup()
                        .addGroup(idLayout.createParallelGroup()
                                .addComponent(idLabel).addComponent(this.idLabel)));


        GroupLayout representationLayout = new GroupLayout(this);
        this.setLayout(representationLayout);
        representationLayout.setVerticalGroup(
                representationLayout.createSequentialGroup()
                        .addComponent(scrollableTitle)
                        .addComponent(idContainer)
                        .addComponent(thumbnailLabel)
                        .addComponent(scrollableExtract)
                        .addComponent(loadSelectedArticlesButton)
                        .addComponent(scrollableTitles)
                        .addComponent(scrollableExternalLinks)
        );
        representationLayout.setHorizontalGroup(
                representationLayout.createSequentialGroup()
                        .addGroup(representationLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                .addComponent(scrollableTitle)
                                .addComponent(idContainer)
                                .addComponent(thumbnailLabel)
                                .addComponent(scrollableExtract)
                                .addComponent(loadSelectedArticlesButton)
                                .addComponent(scrollableTitles)
                                .addComponent(scrollableExternalLinks)));
    }

    public void represent(@NotNull WikipediaArticle article) {
        this.titleLabel.setText(article.getTitle());
        this.idLabel.setText(String.valueOf(article.getPageID()));
        this.articleExtractTextArea.setText(article.getExtract());
        articleExtractTextArea.setCaretPosition(0);
        Image scaledImage = article.getThumbnail().getScaledInstance(200, -1, Image.SCALE_SMOOTH);
        this.thumbnailLabel.setIcon(new ImageIcon(scaledImage));

        this.titlesThisArticleLinksTo.setListData(article.getTitlesOfLinkedArticles().toArray(new String[0]));
        List<JButton> clickableLinkButtons = transformStringListToGUIURLList(article.getExternalLinks());
        clickableLinkButtons.forEach(this.externalLinksPanel::add);
    }

    public List<JButton> transformStringListToGUIURLList(List<String> externalLinks) {
        List<JButton> clickableLinkButtons = new ArrayList<>();
        for (String externalLink : externalLinks) {
            JButton linkButton = new JButton("<HTML><FONT color=\"#000099\">" + externalLink + "</HTML>");
            linkButton.setHorizontalAlignment(SwingConstants.LEFT);
            linkButton.setBorderPainted(false);
            linkButton.setOpaque(false);
            linkButton.setBackground(Color.WHITE);
            linkButton.setToolTipText(externalLink);
            linkButton.addActionListener(actionEvent -> {
                System.out.println("link clicked");
                try {
                    URI uri = new URI(externalLink);
                    Desktop.getDesktop().browse(uri);
                } catch (URISyntaxException | IOException e) {
                    e.printStackTrace();
                    System.out.println(externalLink);
                }
            });
            clickableLinkButtons.add(linkButton);
        }
        return clickableLinkButtons;
    }

    @Override
    public Dimension getMinimumSize() {
        return new Dimension(300, -1);
    }

}
