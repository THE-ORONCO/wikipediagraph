package SwingGUIPlugin;

import endpoint.BackendEndpoint;
import org.jetbrains.annotations.NotNull;
import rendering.RenderedEdge;
import rendering.RenderedWikipediaVertex;
import rendering.WikipediaGraphRenderModel;
import subscription.Notifiable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

public class WikipediaGraphViewport extends JPanel implements Notifiable {
    private WikipediaGraphRenderModel currentGraphRender;
    private final BackendEndpoint backendEndpoint;
    private final float minViewingScale = 0.1f;
    private float viewingScale;

    private int dragStartX;
    private int dragStartY;
    private int dragEndX;
    private int dragEndY;
    private int dragDeltaX;
    private int dragDeltaY;

    private final Font articleFont;

    public WikipediaGraphViewport(@NotNull BackendEndpoint backendEndpoint) {
        super();

        articleFont = new Font(Font.SANS_SERIF, Font.PLAIN, 16);

        dragStartX = dragStartY = dragDeltaX = dragDeltaY = dragEndX = dragEndY = 0;
        this.viewingScale = 1;
        this.backendEndpoint = backendEndpoint;
        this.currentGraphRender = this.backendEndpoint.getCurrentRender();
        backendEndpoint.subscribeToGraphRenderer(this);

        this.addMouseWheelListener(mouseWheelEvent -> {
            int wheelRotationAmount = mouseWheelEvent.getWheelRotation();

            if (wheelRotationAmount < 0) viewingScale += 0.1f;
            else viewingScale -= 0.1f;
            if (viewingScale < minViewingScale) viewingScale = minViewingScale;
        });
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                dragStartX = e.getXOnScreen();
                dragStartY = e.getYOnScreen();
            }
        });
        this.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                super.mouseDragged(e);
                dragEndX = e.getXOnScreen();
                dragEndY = e.getYOnScreen();
                dragDeltaX += (dragEndX - dragStartX) / viewingScale;
                dragDeltaY += (dragEndY - dragStartY) / viewingScale;
                dragStartX = dragEndX;
                dragStartY = dragEndY;
            }
        });
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                System.out.println("clicked at (" + e.getX() + "," + e.getY() + ")");
            }
        });

    }

    public void resetOffsetToOrigin() {
        dragDeltaX = 0;
        dragDeltaY = 0;
        viewingScale = 1;
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        Graphics2D drawingSurface = (Graphics2D) graphics;

        translateViewportAccordingToZoom(drawingSurface);
        turnAntiAliasingOnIn(drawingSurface);

        drawCenter(drawingSurface);
        drawEdges(drawingSurface);
        drawVertices(drawingSurface);

    }

    private void drawCenter(@NotNull Graphics2D graphics) {
        graphics.setColor(Color.lightGray);
        graphics.drawLine(-5, 0, 5, 0);
        graphics.drawLine(0, 5, 0, -5);
    }

    private void translateViewportAccordingToZoom(@NotNull Graphics2D drawingSurface) {
        int viewportTranslationX = this.getWidth() / 2;
        int viewportTranslationY = this.getHeight() / 2;

        drawingSurface.translate(viewportTranslationX, viewportTranslationY);
        drawingSurface.scale(viewingScale, viewingScale);
        drawingSurface.translate(dragDeltaX, dragDeltaY);
    }

    private void drawEdges(@NotNull Graphics2D graphics) {
        graphics.setColor(Color.GRAY);
        for (RenderedEdge edge : currentGraphRender.getRenderedEdges()) {
            int originX = edge.getOriginX();
            int originY = edge.getOriginY();
            int targetX = edge.getTargetX();
            int targetY = edge.getTargetY();
            graphics.drawLine(originX, originY, targetX, targetY);
        }
    }

    private void drawVertices(@NotNull Graphics graphics) {
        graphics.setColor(new Color(100, 100, 100));
        for (RenderedWikipediaVertex vertex : currentGraphRender.getRenderedVertices()) {
            int x = vertex.getX();
            int y = vertex.getY();
            drawTextCenteredOn(graphics, x, y, vertex.getArticleTitle());
        }
    }

    private void turnAntiAliasingOnIn(@NotNull Graphics2D graphics) {
        graphics.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
    }

    public void drawTextCenteredOn(@NotNull Graphics graphics, int x, int y, @NotNull String text) {
        FontMetrics metrics = graphics.getFontMetrics(articleFont);
        graphics.setFont(articleFont);

        int centeredX = x - metrics.stringWidth(text) / 2;
        int centeredY = y + metrics.getAscent() / 2;

        graphics.setColor(Color.black);
        graphics.drawString(text, centeredX, centeredY);
    }

    @Override
    public void sendNotification() {
        currentGraphRender = backendEndpoint.getCurrentRender();
    }
}
