package SwingGUIPlugin;

import endpoint.BackendEndpoint;
import org.jdesktop.swingx.prompt.PromptSupport;
import org.jetbrains.annotations.NotNull;
import subscription.Notifiable;
import subscription.Subscribable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

public class ArticleSearchBar extends JPanel implements Subscribable {
    private final JList<String> loadedArticles;
    private final JTextField searchField;
    private final JButton searchButton;
    private final JCheckBox isIDCheckbox;
    private final BackendEndpoint backendEndpoint;
    private final List<Notifiable> subscribers;

    public ArticleSearchBar(@NotNull BackendEndpoint backendEndpoint,
                            @NotNull JList<String> loadedArticlesGUIElement) {
        this.backendEndpoint = backendEndpoint;
        this.loadedArticles = loadedArticlesGUIElement;
        this.subscribers = new ArrayList<>();

        this.setLayout(new FlowLayout());

        searchField = new JTextField();
        searchField.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 16));
        searchField.setPreferredSize(new Dimension(200, 23));
        searchField.setMinimumSize(searchField.getPreferredSize());
        searchField.setMaximumSize(searchField.getPreferredSize());
        PromptSupport.setPrompt("enter something", searchField);

        searchButton = new JButton("search");
        searchButton.setSize(new Dimension(-1, searchField.getHeight()));
        isIDCheckbox = new JCheckBox("isID", false);

        this.add(searchField);
        this.add(searchButton);
        this.add(isIDCheckbox);

        this.setSize(new Dimension(300, -1));

        searchButton.addActionListener(actionEvent -> {
            if (actionEvent.getSource() == searchButton)
                trySearchingFor(searchField.getText());
        });

        searchField.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {
            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {
                if (keyEvent.getSource() == searchField && keyEvent.getKeyCode() == KeyEvent.VK_ENTER)
                    trySearchingFor(searchField.getText());
            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {
            }
        });
    }

    public void trySearchingFor(@NotNull String searchedString) {
        // TODO make this asynchronous because it stops the whole gui from rendering otherwise
        try {
            if (isIDCheckbox.isSelected())
                searchForID(searchedString);
            else
                searchForTitle(searchedString);

            subscribers.forEach(Notifiable::sendNotification);

            loadedArticles.removeAll();
            loadedArticles.setListData(backendEndpoint.getTitlesOfLoadedArticles().toArray(new String[0]));

        } catch (Exception e) {
            e.printStackTrace();
            JFrame containingWindow = (JFrame) SwingUtilities.getWindowAncestor(this);
            JOptionPane.showMessageDialog(containingWindow,
                    "Query failed. Please check the wikipedia-API-query-endpoint or if the searched article " +
                            "actually exists and restart the application with an existing endpoint.",
                    "query failed", JOptionPane.WARNING_MESSAGE);

        }
    }

    private void searchForTitle(@NotNull String title) {
        backendEndpoint.loadArticlesByTitle(title);
    }

    private void searchForID(@NotNull String id) {
        int idInt = Integer.parseInt(id);
        backendEndpoint.loadArticleByID(idInt);
    }

    @Override
    public void subscribe(@NotNull Notifiable subscriber) {
        subscribers.add(subscriber);
    }

    @Override
    public void unsubscribe(@NotNull Notifiable subscriber) {
        subscribers.remove(subscriber);
    }
}
