package WikipediaPlugin.URLParameters;

public enum ImageParams implements PropertySchema {
    PROPERTY_NAME("pageimages"),
    IMAGE_TYPE("piprop"),
    IMAGE_WIDTH("pithumbsize"),
    TYPE_THUMBNAIL("thumbnail");

    private final String propertyName;
    ImageParams(String propertyName) {
        this.propertyName = propertyName;
    }

    @Override
    public String value() {
        return propertyName;
    }
}
