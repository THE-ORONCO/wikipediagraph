package WikipediaPlugin.URLParameters;

public enum GeneralProps implements PropertySchema{
    PROPERTY_NAME("prop"),
    EXTERNAL_LINKS("extlinks"),
    INTERNAL_LINKS("links"),
    ARTICLE_EXTRACT("extracts"),
    IMAGES("pageimages"),
    PARAMETER_SEPARATOR("|");

    private final String propertyName;
    GeneralProps(String propertyName) {
        this.propertyName = propertyName;
    }

    @Override
    public String value() {
        return propertyName;
    }
}
