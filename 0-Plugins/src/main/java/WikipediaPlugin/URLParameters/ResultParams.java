package WikipediaPlugin.URLParameters;

public enum ResultParams implements PropertySchema{
    CONTINUE("continue"),
    URL("url"),
    WILD_CARD("*"),
    IMAGE_SOURCE("source"),
    FOUND_PAGES("pages");

    private final String propertyName;

    ResultParams(String propertyName) {
        this.propertyName = propertyName;
    }

    @Override
    public String value() {
        return propertyName;
    }
}
