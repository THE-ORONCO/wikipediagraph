package WikipediaPlugin.URLParameters;

public enum PageParams implements PropertySchema {
    TITLE("title"),
    ID("pageid"),
    TITLES("titles"),
    IDS("pageids"),
    EXTRACT("extract");

    private final String propertyName;

    PageParams(String propertyName) {
        this.propertyName = propertyName;
    }

    @Override
    public String value() {
        return propertyName;
    }
}
