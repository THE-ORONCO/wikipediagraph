package WikipediaPlugin.URLParameters;

public enum ActionParams implements PropertySchema {
    PROPERTY_NAME("action"),
    QUERY("query");

    private final String propertyName;

    ActionParams(String propertyName) {
        this.propertyName = propertyName;
    }

    @Override
    public String value() {
        return propertyName;
    }
}
