package WikipediaPlugin.URLParameters;

public enum FormatParams implements PropertySchema {
    PROPERTY_NAME("format"),
    JSON("json");

    private final String propertyName;
    FormatParams(String propertyName) {
        this.propertyName = propertyName;
    }

    @Override
    public String value() {
        return propertyName;
    }
}
