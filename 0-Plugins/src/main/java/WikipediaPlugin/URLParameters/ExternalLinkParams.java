package WikipediaPlugin.URLParameters;

public enum ExternalLinkParams implements PropertySchema{
    PROPERTY_NAME("extlinks"),
    LINK_LIMIT("ellimit"),
    MAX_LIMIT("max");

    private final String propertyName;
    ExternalLinkParams(String propertyName) {
        this.propertyName = propertyName;
    }

    @Override
    public String value() {
        return propertyName;
    }

}
