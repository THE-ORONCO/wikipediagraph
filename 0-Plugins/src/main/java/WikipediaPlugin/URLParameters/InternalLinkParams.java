package WikipediaPlugin.URLParameters;

public enum InternalLinkParams implements PropertySchema{
    PROPERTY_NAME("links"),
    LINK_LIMIT("pllimit"),
    MAX_LIMIT("max");

    private final String propertyName;
    InternalLinkParams(String propertyName) {
        this.propertyName = propertyName;
    }

    @Override
    public String value() {
        return propertyName;
    }
}
