package WikipediaPlugin.URLParameters;

public enum ExtractParams implements PropertySchema{
    PROPERTY_NAME("extracts"),
    SENTENCE_LIMIT("exsentences"),
    EXPLAIN_TEXT("explaintext"),
    EXTRACT_COUNT("exlimit"),
    FORMAT("exsectionformat"),
    PLAIN_FORMAT("plain");

    private final String propertyName;
    ExtractParams(String propertyName) {
        this.propertyName = propertyName;
    }

    @Override
    public String value() {
        return propertyName;
    }
}
