package WikipediaPlugin;

import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class WikipediaAPIUrlConstructor {

    private Map<String, String> parameters;
    private final String apiUrl;

    public WikipediaAPIUrlConstructor(String apiUrl) {
        parameters = new HashMap<>();
        this.apiUrl = apiUrl;
    }

    public void putParameterInUrl(String key, String value) {
        parameters.put(key, value);
    }

    public String construct() {
        StringBuilder concatenatedParameters = new StringBuilder("?");
        for (Map.Entry<String, String> parameter : parameters.entrySet()) {
            if (concatenatedParameters.charAt(concatenatedParameters.length() - 1) != '?') {
                concatenatedParameters.append("&");
            }
            String parameterString = String.format("%s=%s", parameter.getKey(), parameter.getValue());
            concatenatedParameters.append(parameterString);
        }
        StringBuilder apiUrlWithParameters = new StringBuilder(apiUrl).append(concatenatedParameters);

        try {
            String decodedURL = URLDecoder.decode(apiUrlWithParameters.toString(), StandardCharsets.UTF_8);

            URL url = new URL(decodedURL);

            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());

            return uri.toASCIIString();
        } catch (MalformedURLException | URISyntaxException e) {
            e.printStackTrace();
            return apiUrl;
        }
    }

    public void clear() {
        parameters = new HashMap<>();
    }
}
