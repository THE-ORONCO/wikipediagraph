package WikipediaPlugin;

import WikipediaPlugin.URLParameters.*;
import factories.WikipediaArticleFactory;
import information.WikipediaArticle;
import information.WikipediaArticleMerger;
import org.jetbrains.annotations.NotNull;
import wikipedia.factories.WikipediaQueryResultFactory;
import wikipedia.query.types.WikipediaQueryInterface;
import wikipedia.result.WikipediaQueryResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WikipediaQueryHandler {
    private static final Map<String, Map<String, String>> DEFAULT_QUERY_PARAMETERS = new HashMap<>() {{
        put(ExternalLinkParams.PROPERTY_NAME.value(), new HashMap<>() {{
            put(ExternalLinkParams.LINK_LIMIT.value(), ExternalLinkParams.MAX_LIMIT.value());
        }});
        put(InternalLinkParams.PROPERTY_NAME.value(), new HashMap<>() {{
            put(InternalLinkParams.LINK_LIMIT.value(), InternalLinkParams.MAX_LIMIT.value());
        }});
        put(ExtractParams.PROPERTY_NAME.value(), new HashMap<>() {{
            put(ExtractParams.SENTENCE_LIMIT.value(), "10");
            put(ExtractParams.EXPLAIN_TEXT.value(), "1");
            put(ExtractParams.FORMAT.value(), ExtractParams.PLAIN_FORMAT.value());
            put(ExtractParams.EXTRACT_COUNT.value(), "1");
        }});
        put(ImageParams.PROPERTY_NAME.value(), new HashMap<>() {{
            put(ImageParams.IMAGE_TYPE.value(), ImageParams.TYPE_THUMBNAIL.value());
            put(ImageParams.IMAGE_WIDTH.value(), "300");
        }});
    }};
    private final String PARAM_SEPARATOR = GeneralProps.PARAMETER_SEPARATOR.value();
    private Map<String, String> continueParameters;
    private Map<String, String> queryParameters;
    private Map<Integer, WikipediaArticle> articles;

    final private WikipediaQueryResultFactory resultFactory;
    final private WikipediaJsonParser jsonParser;
    final private WikipediaArticleFactory articleFactory;
    final private WikipediaArticleMerger articleMerger;
    final private QueryParameterExtractor parameterExtractor;

    public WikipediaQueryHandler(@NotNull QueryParameterExtractor parameterExtractor,
                                 @NotNull WikipediaQueryResultFactory resultFactory,
                                 @NotNull WikipediaJsonParser jsonParser,
                                 @NotNull WikipediaArticleFactory articleFactory,
                                 @NotNull WikipediaArticleMerger articleMerger) {

        this.parameterExtractor = parameterExtractor;
        this.resultFactory = resultFactory;
        this.jsonParser = jsonParser;
        this.articleFactory = articleFactory;
        this.articleMerger = articleMerger;

        this.continueParameters = new HashMap<>();
        this.queryParameters = new HashMap<>();
        this.articles = new HashMap<>();
    }

    public void handle(@NotNull WikipediaQueryInterface query) {
        this.continueParameters = new HashMap<>();
        this.queryParameters = new HashMap<>();
        this.articles = new HashMap<>();
        Map<String, String> extractedParameters = parameterExtractor.extractParametersFrom(query);
        this.queryParameters = addMissingQueryParametersTo(extractedParameters);
    }

    private Map<String, String> addMissingQueryParametersTo(Map<String, String> parameters) {
        if (parameters.containsKey(GeneralProps.PROPERTY_NAME.value())) {
            for (String property : parameters.get(GeneralProps.PROPERTY_NAME.value()).split(PARAM_SEPARATOR)) {
                for (Map.Entry<String, String> additionalPropertyValues : DEFAULT_QUERY_PARAMETERS.get(property).entrySet()) {
                    if (!parameters.containsKey(additionalPropertyValues.getKey())) {
                        parameters.put(additionalPropertyValues.getKey(), additionalPropertyValues.getValue());
                    }
                }
            }
        } else {
            String queriedParameters  = String.join(PARAM_SEPARATOR, DEFAULT_QUERY_PARAMETERS.keySet());
            parameters.put(GeneralProps.PROPERTY_NAME.value(), queriedParameters);
            for (Map.Entry<String, Map<String, String>> parameterSet : DEFAULT_QUERY_PARAMETERS.entrySet()) {
                parameters.putAll(parameterSet.getValue());
            }
        }
        parameters.put(FormatParams.PROPERTY_NAME.value(), FormatParams.JSON.value());
        return parameters;
    }

    public boolean queryIsIncomplete() {
        return continueParameters.size() != 0;
    }

    public Map<String, String> nextQuery() {
        Map<String, String> allQueryParameters = new HashMap<>(queryParameters);
        for (Map.Entry<String, String> continueParameter : continueParameters.entrySet()) {
            allQueryParameters.put(continueParameter.getKey(), continueParameter.getValue());
        }
        return allQueryParameters;
    }

    public void interpretJsonQueryResult(@NotNull String resultString) {
        continueParameters = jsonParser.parseContinueParameters(resultString);
        List<WikipediaArticle> newArticles = jsonParser.parseArticles(resultString, articleFactory);
        this.mergeIntoCurrentResult(newArticles);
    }

    private void mergeIntoCurrentResult(@NotNull List<WikipediaArticle> newArticles) {
        for (WikipediaArticle newArticle : newArticles) {
            int pageID = newArticle.getPageID();
            if (articles.containsKey(pageID)) {
                newArticle = articleMerger.mergeArticles(articles.get(pageID), newArticle);
            }
            articles.put(newArticle.getPageID(), newArticle);
        }
    }


    public WikipediaQueryResult getResult() {
        return resultFactory.createFrom(articles.values());
    }
}
