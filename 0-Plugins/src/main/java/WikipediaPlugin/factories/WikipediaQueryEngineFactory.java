package WikipediaPlugin.factories;

import WikipediaPlugin.*;
import factories.WikipediaArticleFactory;
import information.WikipediaArticleMerger;
import org.jetbrains.annotations.NotNull;
import wikipedia.factories.WikipediaQueryFactory;
import wikipedia.factories.WikipediaQueryResultFactory;
import wikipedia.query.QueryExecutor;
import wikipedia.query.WikipediaQueryEngine;

public class WikipediaQueryEngineFactory {
    private final String DEFAULT_WIKIPEDIA_API_ENDPOINT = "https://en.wikipedia.org/w/api.php";
    private final WikipediaAPIUrlConstructor DEFAULT_URL_CONSTRUCTOR = new WikipediaAPIUrlConstructor(DEFAULT_WIKIPEDIA_API_ENDPOINT);
    private final WikipediaQueryHandler DEFAULT_QUERY_HANDLER = new WikipediaQueryHandler(
            new QueryParameterExtractor(), new WikipediaQueryResultFactory(),
            new WikipediaJsonParser(), new WikipediaArticleFactory(), new WikipediaArticleMerger()
    );
    private final QueryExecutor DEFAULT_QUERY_EXECUTOR = new WikipediaAPIQueryExecutor(DEFAULT_URL_CONSTRUCTOR, DEFAULT_QUERY_HANDLER);
    private final WikipediaQueryFactory DEFAULT_QUERY_FACTORY = new WikipediaQueryFactory();

    public WikipediaQueryEngine create() {
        return create(DEFAULT_QUERY_EXECUTOR, DEFAULT_QUERY_FACTORY);
    }

    public WikipediaQueryEngine create(@NotNull QueryExecutor queryExecutor) {
        return create(queryExecutor, DEFAULT_QUERY_FACTORY);
    }

    public WikipediaQueryEngine create(@NotNull WikipediaQueryFactory queryFactory) {
        return create(DEFAULT_QUERY_EXECUTOR, queryFactory);
    }


    public WikipediaQueryEngine create(@NotNull QueryExecutor queryExecutor,
                                       @NotNull WikipediaQueryFactory queryFactory) {
        return new WikipediaQueryEngine(queryExecutor, queryFactory);
    }

    public WikipediaQueryEngine create(@NotNull String apiEndpointURL){
        WikipediaAPIUrlConstructor urlConstructor = new WikipediaAPIUrlConstructor(apiEndpointURL);
        WikipediaAPIQueryExecutor queryExecutorWithGivenURL = new WikipediaAPIQueryExecutor(urlConstructor, DEFAULT_QUERY_HANDLER);
        return create(queryExecutorWithGivenURL);
    }
}
