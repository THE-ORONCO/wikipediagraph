package WikipediaPlugin;

import WikipediaPlugin.URLParameters.*;
import factories.WikipediaArticleFactory;
import information.WikipediaArticle;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.jetbrains.annotations.NotNull;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.*;
import java.util.logging.Logger;

public class WikipediaJsonParser {

    private static final String PARAMETER_PAGES = ResultParams.FOUND_PAGES.value();
    private static final String PARAMETER_PAGE_ID = PageParams.ID.value();
    private static final String PARAMETER_TITLE = PageParams.TITLE.value();
    private static final String PARAMETER_EXTRACT = PageParams.EXTRACT.value();
    private static final String PARAMETER_LINKS = InternalLinkParams.PROPERTY_NAME.value();
    private static final String PARAMETER_EXTERNAL_LINKS = ExternalLinkParams.PROPERTY_NAME.value();
    private static final String PARAMETER_CONTINUE = ResultParams.CONTINUE.value();
    private static final String PARAMETER_URL = ResultParams.URL.value();
    private static final String PARAMETER_STAR = ResultParams.WILD_CARD.value();
    private static final String PARAMETER_THUMBNAIL = ImageParams.TYPE_THUMBNAIL.value();
    private static final String PARAMETER_SOURCE = ResultParams.IMAGE_SOURCE.value();
    private static final String PARAMETER_QUERY = ActionParams.QUERY.value();

    private final Logger logger = Logger.getLogger(WikipediaJsonParser.class.getName());

    @NotNull
    private JsonNode parseToJsonNode(@NotNull String json) {
        JsonFactory factory = new JsonFactory();
        ObjectMapper mapper = new ObjectMapper(factory);
        final JsonNode rootNode;

        try {
            rootNode = mapper.readTree(json);
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("The given Json was of an unknown format or not Json at all!");
        }
        return rootNode;
    }

    @NotNull
    public List<WikipediaArticle> parseArticles(@NotNull String json, WikipediaArticleFactory articleFactory) throws IllegalArgumentException {
        JsonNode rootNode = parseToJsonNode(json);
        ArrayList<WikipediaArticle> parsedArticles = new ArrayList<>();
        try {
            JsonNode pagesNode = findOneInNode(rootNode, PARAMETER_QUERY).get(PARAMETER_PAGES);

            for (Iterator<JsonNode> it = pagesNode.getElements(); it.hasNext(); ) {
                JsonNode pageNode = it.next();

                String title = pageNode.get(PARAMETER_TITLE).getTextValue();
                int pageID = pageNode.get(PARAMETER_PAGE_ID).getIntValue();
                if (title.isEmpty() && pageID == 0)
                    continue;

                String extract = returnOptionalString(PARAMETER_EXTRACT, pageNode);
                List<String> externalLinks = extractLinksFrom(pageNode.get(PARAMETER_EXTERNAL_LINKS));
                List<String> titlesOfLinkedArticles = extractLinksFrom(pageNode.get(PARAMETER_LINKS));
                Image thumbnail = tryToExtractThumbnailFrom(pageNode);

                WikipediaArticle parsedWikipediaArticle =
                        articleFactory.create(
                                title,
                                pageID,
                                extract,
                                externalLinks,
                                titlesOfLinkedArticles,
                                thumbnail
                        );

                parsedArticles.add(parsedWikipediaArticle);
            }
        } catch (Exception e) {
            logger.info("JSON could not be read. Assume that the query result was empty");
        }

        return parsedArticles;
    }

    private Image tryToExtractThumbnailFrom(JsonNode pageNode) {
        Image thumbnail = null;
        if (pageNode.has(PARAMETER_THUMBNAIL)) {
            JsonNode thumbnailNode = pageNode.get(PARAMETER_THUMBNAIL);
            if (thumbnailNode.has(PARAMETER_SOURCE)) {
                try {
                    URL url = new URL(thumbnailNode.get(PARAMETER_SOURCE).asText());
                    thumbnail = ImageIO.read(url);
                } catch (IOException e) {
                    e.printStackTrace();
                    Logger.getLogger(WikipediaJsonParser.class.getName()).info("Couldn't load thumbnail.");
                }
            }
        }
        return thumbnail;
    }

    private String returnOptionalString(@NotNull String parameterName, @NotNull JsonNode node) {
        if (node.has(parameterName)) {
            return node.get(parameterName).getTextValue();
        } else return null;
    }

    @NotNull
    private List<String> extractLinksFrom(JsonNode listNode) {
        // TODO maybe sanitize the links before adding them (some are returned without http:)
        List<String> linksLists = new ArrayList<>();
        if (listNode != null) {
            for (Iterator<JsonNode> it = listNode.getElements(); it.hasNext(); ) {
                JsonNode linkNode = it.next();
                Optional<String> extractedLink = Optional.empty();
                if (linkNode.has(PARAMETER_TITLE))
                    extractedLink = Optional.of(linkNode.get(PARAMETER_TITLE).asText());
                 else if (linkNode.has(PARAMETER_URL))
                    extractedLink = Optional.of(linkNode.get(PARAMETER_URL).asText());
                 else if (linkNode.has(PARAMETER_STAR))
                    extractedLink = Optional.of(linkNode.get(PARAMETER_STAR).asText());

                 if (extractedLink.isPresent()){
                     String sanitizedLink = sanitizeURI(extractedLink.get());
                     linksLists.add(sanitizedLink);
                 }

            }
        }
        return linksLists;
    }

    private String sanitizeURI(@NotNull String uri) {
        if (uri.startsWith("//"))
            uri = "https:" + uri;
        return uri;
    }

    @NotNull
    public Map<String, String> parseContinueParameters(@NotNull String json) {
        JsonNode rootNode = parseToJsonNode(json);
        Map<String, String> continueParameters = new HashMap<>();

        if (rootNode.has(PARAMETER_CONTINUE)) {
            JsonNode continueNode = rootNode.get(PARAMETER_CONTINUE);
            for (Iterator<String> nodeIterator = continueNode.getFieldNames(); nodeIterator.hasNext(); ) {
                String continueParameterName = nodeIterator.next();
                continueParameters.put(
                        continueParameterName,
                        continueNode.get(continueParameterName).asText());
            }
        }
        return continueParameters;
    }

    private JsonNode findOneInNode(@NotNull JsonNode node, @NotNull String... names) {
        for (String name : names) {
            if (node.has(name)) {
                return node.get(name);
            }
        }
        throw new IllegalArgumentException("Unknown Wikipedia query response JSON format.");
    }

}
