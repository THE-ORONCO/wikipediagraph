package WikipediaPlugin;

import WikipediaPlugin.URLParameters.ActionParams;
import WikipediaPlugin.URLParameters.GeneralProps;
import WikipediaPlugin.URLParameters.PageParams;
import wikipedia.query.ArticleIdentifier;
import wikipedia.query.types.WikipediaQueryInterface;

import java.util.Map;

public class QueryParameterExtractor {
    public Map<String, String> extractParametersFrom(WikipediaQueryInterface query) {
        Map<String, String> queryParameters = query.getExtraParameters();
        queryParameters.put(ActionParams.PROPERTY_NAME.value(), ActionParams.QUERY.value());
        String joinedIdentifiers = String.join(GeneralProps.PARAMETER_SEPARATOR.value(), query.getIdentifierValues());

        if (query.getArticleIdentifier().equals(ArticleIdentifier.TITLE))
            queryParameters.put(PageParams.TITLES.value(), joinedIdentifiers);

        else if (query.getArticleIdentifier().equals(ArticleIdentifier.PAGE_ID))
            queryParameters.put(PageParams.IDS.value(), joinedIdentifiers);

        else
            throw new IllegalArgumentException("unknown identifier for article");


        return queryParameters;
    }
}
