package WikipediaPlugin;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

public class GETRequestExecutor {
    private static final Logger logger= Logger.getLogger(GETRequestExecutor.class.getName());;

    public String makeGetRequestTo(@NotNull String url) {

        HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).build();

        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(url))
                .build();

        try {
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            logger.info(String.format("Http request to \"%s\" came back with status code %d and %d bytes of content",
                    response.uri(), response.statusCode(), response.body().getBytes(StandardCharsets.UTF_8).length));

            return response.body();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            return "";
        }
    }
}
