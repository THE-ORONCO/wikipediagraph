package WikipediaPlugin;

import org.jetbrains.annotations.NotNull;
import wikipedia.query.QueryExecutor;
import wikipedia.query.types.WikipediaQueryInterface;
import wikipedia.result.WikipediaQueryResult;

import java.util.Map;
import java.util.logging.Logger;

public class WikipediaAPIQueryExecutor implements QueryExecutor {
    private static final Logger logger = Logger.getLogger(WikipediaAPIQueryExecutor.class.getName());
    private final WikipediaAPIUrlConstructor urlConstructor;
    private final WikipediaQueryHandler queryHandler;
    private final GETRequestExecutor getExecutor;

    public WikipediaAPIQueryExecutor(@NotNull WikipediaAPIUrlConstructor urlConstructor,
                                     @NotNull WikipediaQueryHandler queryHandler) {
        this.urlConstructor = urlConstructor;
        this.queryHandler = queryHandler;
        this.getExecutor = new GETRequestExecutor();
    }

    @Override
    public WikipediaQueryResult execute(@NotNull WikipediaQueryInterface query) {
        queryHandler.handle(query);
        do {
            Map<String, String> nextQuery = queryHandler.nextQuery();
            String apiRequestUrl = constructApiRequestFrom(nextQuery);
            String resultString = getExecutor.makeGetRequestTo(apiRequestUrl);
            queryHandler.interpretJsonQueryResult(resultString);
        } while (queryHandler.queryIsIncomplete());
        urlConstructor.clear();
        WikipediaQueryResult result = queryHandler.getResult();

        logger.info("retrieved " + result.getArticles().size() + " articles from wikipedia");
        return result;
    }

    private String constructApiRequestFrom(@NotNull Map<String, String> query) {
        for (Map.Entry<String, String> parameter : query.entrySet()) {
            urlConstructor.putParameterInUrl(parameter.getKey(), parameter.getValue());
        }
        return urlConstructor.construct();
    }


}
