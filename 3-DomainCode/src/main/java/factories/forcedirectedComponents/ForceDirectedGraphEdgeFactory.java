package factories.forcedirectedComponents;

import factories.edges.EdgeFactory;
import graph.edges.SpringLikeEdge;
import graph.verticies.WeightedVertex;

public interface ForceDirectedGraphEdgeFactory<EdgeType extends SpringLikeEdge<VertexType>, VertexType extends WeightedVertex>
        extends EdgeFactory<EdgeType, VertexType> {

    EdgeType createEdge(VertexType originVertex, VertexType targetVertex, float weight);

    EdgeType createEdge(VertexType originVertex, VertexType targetVertex, float weight, float naturalSpringLength);

    }
