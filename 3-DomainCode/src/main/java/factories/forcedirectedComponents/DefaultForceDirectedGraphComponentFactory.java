package factories.forcedirectedComponents;

import factories.edges.SpringLikeEdgeFactory;
import factories.vertices.WeightedVertexFactory;
import graph.edges.SpringLikeEdge;
import graph.verticies.WeightedVertex;
import org.jetbrains.annotations.TestOnly;

/**
 * Wrapper for
 * @see ForceDirectedGraphComponentFactory
 * that creates SpringLikeEdges and Weighted vertices. This was created so ForceDirectedGraphStuff can be tested more
 * easily.
 */
public class DefaultForceDirectedGraphComponentFactory extends
        ForceDirectedGraphComponentFactory<WeightedVertex, SpringLikeEdge<WeightedVertex>> {

    @TestOnly
    public DefaultForceDirectedGraphComponentFactory() {
        super(new WeightedVertexFactory(), new SpringLikeEdgeFactory());
    }
}
