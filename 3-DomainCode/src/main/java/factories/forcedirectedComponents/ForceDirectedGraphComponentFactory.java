package factories.forcedirectedComponents;

import factories.GraphComponentFactory;
import geometry.Vector;
import graph.edges.SpringLikeEdge;
import graph.verticies.WeightedVertex;
import org.jetbrains.annotations.NotNull;

public class ForceDirectedGraphComponentFactory<VertexType extends WeightedVertex, EdgeType extends SpringLikeEdge<VertexType>>
        implements GraphComponentFactory<VertexType, EdgeType> {
    private final ForceDirectedGraphVertexFactory<VertexType> vertexFactory;

    private final ForceDirectedGraphEdgeFactory<EdgeType, VertexType> edgeFactory;

    public ForceDirectedGraphComponentFactory(@NotNull ForceDirectedGraphVertexFactory<VertexType> vertexFactory,
                                              @NotNull ForceDirectedGraphEdgeFactory<EdgeType, VertexType> edgeFactory) {
        this.vertexFactory = vertexFactory;
        this.edgeFactory = edgeFactory;
    }

    @Override
    public VertexType createVertex(@NotNull Vector position) {
        return vertexFactory.createVertex(position);
    }

    public VertexType createVertex(@NotNull Vector position, float weight) {
        return vertexFactory.create(position, weight);
    }

    @Override
    public VertexType createVertex(float x, float y) {
        return vertexFactory.createVertex(x, y);
    }


    public VertexType createVertex(float x, float y, float weight) {
        return vertexFactory.create(x, y, weight);
    }

    @Override
    public EdgeType createEdge(@NotNull VertexType originVertex,@NotNull  VertexType targetVertex) {
        return edgeFactory.createEdge(originVertex, targetVertex);
    }

    public EdgeType createEdge(@NotNull VertexType originVertex, @NotNull VertexType targetVertex, float weight) {
        return edgeFactory.createEdge(originVertex, targetVertex, weight);
    }

    public EdgeType createEdge(@NotNull VertexType originVertex,
                               @NotNull VertexType targetVertex,
                               float weight, float naturalSpringLength) {
        return edgeFactory.createEdge(originVertex, targetVertex, weight, naturalSpringLength);
    }
}
