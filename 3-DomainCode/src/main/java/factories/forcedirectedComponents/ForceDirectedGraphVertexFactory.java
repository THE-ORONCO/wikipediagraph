package factories.forcedirectedComponents;

import factories.vertices.VertexFactory;
import geometry.Vector;
import graph.verticies.WeightedVertex;

public interface ForceDirectedGraphVertexFactory<VertexType extends WeightedVertex> extends VertexFactory<VertexType> {

    VertexType create(float x, float y, float weight);

    VertexType create(Vector position, float weight);
}
