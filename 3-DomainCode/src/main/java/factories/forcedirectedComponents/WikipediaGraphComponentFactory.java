package factories.forcedirectedComponents;

import factories.edges.WikipediaEdgeFactory;
import factories.vertices.WikipediaVertexFactory;
import geometry.Vector;
import information.WikipediaArticle;
import information.WikipediaEdge;
import information.WikipediaVertex;
import org.jetbrains.annotations.NotNull;

import java.util.Random;

public class WikipediaGraphComponentFactory {
    private final WikipediaVertexFactory vertexFactory;
    private final WikipediaEdgeFactory edgeFactory;

    public WikipediaGraphComponentFactory(@NotNull WikipediaVertexFactory vertexFactory,
                                          @NotNull WikipediaEdgeFactory edgeFactory) {
        this.vertexFactory = vertexFactory;
        this.edgeFactory = edgeFactory;
    }

    public WikipediaGraphComponentFactory() {
        this(new WikipediaVertexFactory(), new WikipediaEdgeFactory());
    }

    public WikipediaVertex createVertex(@NotNull WikipediaArticle article) {
        return vertexFactory.create(article);
    }

    public WikipediaVertex createVertexWithRandomOffset(@NotNull WikipediaArticle article) {
        Random random = new Random();
        int maxBound = 100;
        float randomX = random.nextInt(maxBound * 2) - maxBound;
        float randomY = random.nextInt(maxBound * 2) - maxBound;
        return createVertex(randomX, randomY, article);
    }

    public WikipediaVertex createVertex(@NotNull Vector position, @NotNull WikipediaArticle article) {
        return vertexFactory.create(position, article);
    }

    public WikipediaVertex createVertex(float x, float y, @NotNull WikipediaArticle article) {
        return vertexFactory.create(x, y, article);
    }

    public WikipediaVertex createVertex(@NotNull Vector position, float weight, @NotNull WikipediaArticle article) {
        return vertexFactory.create(position, weight, article);
    }

    public WikipediaVertex createVertex(float x, float y, float weight, @NotNull WikipediaArticle article) {
        return vertexFactory.create(x, y, weight, article);
    }

    public WikipediaEdge createEdge(@NotNull WikipediaVertex origin, @NotNull WikipediaVertex target) {
        return edgeFactory.createEdge(origin, target);
    }

    public WikipediaEdge createEdge(@NotNull WikipediaVertex origin, @NotNull WikipediaVertex target, float weight) {
        return edgeFactory.createEdge(origin, target, weight);
    }

    public WikipediaEdge createEdge(@NotNull WikipediaVertex origin, @NotNull WikipediaVertex target,
                                    float weight, float naturalSpringLength) {
        return edgeFactory.createEdge(origin, target, weight, naturalSpringLength);
    }
}
