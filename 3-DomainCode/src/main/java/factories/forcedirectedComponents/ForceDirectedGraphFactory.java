package factories.forcedirectedComponents;

import graph.edges.SpringLikeEdge;
import graph.verticies.WeightedVertex;
import org.jetbrains.annotations.NotNull;
import physics.ForceDirectedGraph;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ForceDirectedGraphFactory<VertexType extends WeightedVertex, EdgeType extends SpringLikeEdge<VertexType>> {

    public ForceDirectedGraph<VertexType, EdgeType> create() {
        List<VertexType> emptyVertexList = new ArrayList<>();
        List<EdgeType> emptyEdgeList = new ArrayList<>();
        return create(emptyVertexList, emptyEdgeList);
    }

    public ForceDirectedGraph<VertexType, EdgeType> create(@NotNull List<VertexType> vertices,
                                                           @NotNull List<EdgeType> edges) {
        Set<VertexType> missingVertices = findAllVerticesContainedInEdgesButNotContainedInVertexList(vertices, edges);
        missingVertices.addAll(vertices);
        return new ForceDirectedGraph<>(new ArrayList<>(missingVertices), edges);
    }

    private Set<VertexType> findAllVerticesContainedInEdgesButNotContainedInVertexList(List<VertexType> vertices, List<EdgeType> edges) {
        Set<VertexType> missedVertices = new HashSet<>();
        for (EdgeType edge : edges) {
            VertexType originVertex = edge.getOriginVertex();
            VertexType targetVertex = edge.getTargetVertex();
            if (!vertices.contains(originVertex)) missedVertices.add(originVertex);
            if (!vertices.contains(targetVertex)) missedVertices.add(targetVertex);
        }
        return missedVertices;
    }
}
