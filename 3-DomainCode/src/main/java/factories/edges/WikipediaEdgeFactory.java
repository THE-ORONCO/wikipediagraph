package factories.edges;

import information.WikipediaEdge;
import information.WikipediaVertex;
import org.jetbrains.annotations.NotNull;

public class WikipediaEdgeFactory {
    private final float DEFAULT_NATURAL_SPRING_LENGTH = 100f;
    private final float DEFAULT_EDGE_WEIGHT = 1f;

    public WikipediaEdge createEdge(@NotNull WikipediaVertex originVertex,
                                    @NotNull WikipediaVertex targetVertex) {
        return createEdge(originVertex, targetVertex, DEFAULT_EDGE_WEIGHT);
    }

    public WikipediaEdge createEdge(@NotNull WikipediaVertex originVertex,
                                    @NotNull WikipediaVertex targetVertex,
                                    float edgeWeight) {
        return createEdge(originVertex, targetVertex, edgeWeight, DEFAULT_NATURAL_SPRING_LENGTH);
    }

    public WikipediaEdge createEdge(@NotNull WikipediaVertex originVertex,
                                    @NotNull WikipediaVertex targetVertex,
                                    float edgeWeight,
                                    float naturalSpringLength) {
        return new WikipediaEdge(originVertex, targetVertex, edgeWeight, naturalSpringLength);
    }
}
