package factories.edges;

import factories.forcedirectedComponents.ForceDirectedGraphEdgeFactory;
import graph.edges.SpringLikeEdge;
import graph.verticies.WeightedVertex;

public class SpringLikeEdgeFactory implements ForceDirectedGraphEdgeFactory<SpringLikeEdge<WeightedVertex>, WeightedVertex> {
    private final float DEFAULT_NATURAL_SPRING_LENGTH = 1f;
    private final float DEFAULT_EDGE_WEIGHT = 1f;

    @Override
    public SpringLikeEdge<WeightedVertex> createEdge(WeightedVertex originVertex, WeightedVertex targetVertex) {
        return new SpringLikeEdge<>(originVertex, targetVertex, DEFAULT_EDGE_WEIGHT, DEFAULT_NATURAL_SPRING_LENGTH);
    }

    @Override
    public SpringLikeEdge<WeightedVertex> createEdge(WeightedVertex originVertex, WeightedVertex targetVertex, float weight) {
        return  new SpringLikeEdge<>(originVertex, targetVertex, weight, DEFAULT_NATURAL_SPRING_LENGTH);
    }

    @Override
    public SpringLikeEdge<WeightedVertex> createEdge(WeightedVertex originVertex, WeightedVertex targetVertex, float weight, float naturalSpringLength) {
        return new SpringLikeEdge<>(originVertex, targetVertex, weight, naturalSpringLength);
    }
}
