package factories.edges;

import graph.edges.Edge;
import graph.verticies.Vertex;

public interface EdgeFactory<EdgeType extends Edge<VertexType>, VertexType extends Vertex> {
    EdgeType createEdge(VertexType originVertex, VertexType targetVertex);
}
