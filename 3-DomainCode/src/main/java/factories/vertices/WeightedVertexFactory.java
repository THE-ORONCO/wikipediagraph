package factories.vertices;

import factories.forcedirectedComponents.ForceDirectedGraphVertexFactory;
import geometry.Vector;
import graph.verticies.WeightedVertex;

public class WeightedVertexFactory implements ForceDirectedGraphVertexFactory<WeightedVertex> {
    private final float DEFAULT_X = 0f;
    private final float DEFAULT_Y = 0f;
    private final float DEFAULT_WEIGHT = 1f;

    @Override
    public WeightedVertex createVertex() {
        return new WeightedVertex(DEFAULT_X, DEFAULT_Y, DEFAULT_WEIGHT);
    }

    @Override
    public WeightedVertex createVertex(Vector position) {
        return  new WeightedVertex(position, DEFAULT_WEIGHT);
    }

    @Override
    public WeightedVertex createVertex(float x, float y) {
        return new WeightedVertex(x, y, DEFAULT_WEIGHT);
    }

    @Override
    public WeightedVertex create(float x, float y, float weight){
        return  new WeightedVertex(x, y, weight);

    }

    @Override
    public WeightedVertex create(Vector position, float weight){
        return new WeightedVertex(position, weight);

    }
}
