package factories.vertices;

import geometry.Vector;
import information.WikipediaArticle;
import information.WikipediaVertex;
import org.jetbrains.annotations.NotNull;

public class WikipediaVertexFactory {
    private final float DEFAULT_X = 0f;
    private final float DEFAULT_Y = 0f;
    private final float DEFAULT_WEIGHT = 1f;

    public WikipediaVertex create(@NotNull WikipediaArticle article) {
        return create(DEFAULT_X, DEFAULT_Y, DEFAULT_WEIGHT, article);
    }


    public WikipediaVertex create(@NotNull Vector position, @NotNull WikipediaArticle article) {
        return create(position, DEFAULT_WEIGHT, article);
    }


    public WikipediaVertex create(float x, float y, @NotNull WikipediaArticle article) {
        return create(x, y, DEFAULT_WEIGHT, article);
    }

    public WikipediaVertex create(@NotNull Vector position, float weight, @NotNull WikipediaArticle article) {
        return new WikipediaVertex(position, weight, article);
    }

    public WikipediaVertex create(float x, float y, float weight, @NotNull WikipediaArticle article) {
        return new WikipediaVertex(x, y, weight, article);
    }
}
