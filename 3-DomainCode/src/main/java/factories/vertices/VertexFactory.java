package factories.vertices;

import geometry.Vector;
import graph.verticies.Vertex;

public interface VertexFactory<VertexType extends Vertex> {
    VertexType createVertex();
    VertexType createVertex(Vector position);
    VertexType createVertex(float x, float y);
}
