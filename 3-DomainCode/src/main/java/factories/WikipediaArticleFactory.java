package factories;

import information.WikipediaArticle;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class WikipediaArticleFactory {
    private static final String EMPTY_EXTRACT = "";
    private int DEFAULT_ARTICLE_ID = 0;
    public WikipediaArticle create(@NotNull String title){
        DEFAULT_ARTICLE_ID--;
        return create(title, DEFAULT_ARTICLE_ID, EMPTY_EXTRACT, new ArrayList<>(), new ArrayList<>(), null);
    }

    public WikipediaArticle create(@NotNull String title, int pageID){
        return create(title, pageID, null, null, null, null);
    }

    public WikipediaArticle create(@NotNull String title, int pageID,
                                   @Nullable String extract,
                                   @Nullable List<String> externalLinks,
                                   @Nullable List<String> titlesOfLinkedArticles,
                                   @Nullable Image thumbnail){
        String optionalExtract = Optional.ofNullable(extract).orElse(EMPTY_EXTRACT);
        List<String> optionalExternalLinks = Optional.ofNullable(externalLinks).orElse(new ArrayList<>());
        List<String> optionalTitlesOfLinkedArticles = Optional.ofNullable(titlesOfLinkedArticles).orElse(new ArrayList<>());
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Image newThumbnail = Optional.ofNullable(thumbnail).orElse(toolkit.getImage("missingImage.png"));
        return new WikipediaArticle(
                title,
                pageID,
                optionalExtract,
                optionalExternalLinks,
                optionalTitlesOfLinkedArticles,
                newThumbnail);
    }
}
