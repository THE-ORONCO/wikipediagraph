package factories;

import factories.forcedirectedComponents.ForceDirectedGraphFactory;
import graph.Graph;
import graph.edges.SpringLikeEdge;
import graph.verticies.WeightedVertex;
import org.jetbrains.annotations.NotNull;
import physics.DefaultPhysicalConstants;
import physics.ForceApplier;
import physics.PhysicsGraphSimulation;
import physics.forceCalculators.*;
import physics.vertexMovers.VertexMover;

public class PhysicsGraphSimulationFactory<VertexType extends WeightedVertex, EdgeType extends SpringLikeEdge<VertexType>> {
    private static final float DEFAULT_TIME_STEP = DefaultPhysicalConstants.PHYSICS_TIME_STEP.value();
    private static final float DEFAULT_DAMPING_COEFFICIENT = DefaultPhysicalConstants.DAMPING_COEFFICIENT.value();
    private static final float DEFAULT_COULOMB_CONSTANT = DefaultPhysicalConstants.COULOMB_CONSTANT.value();
    private static final float DEFAULT_SPRING_CONSTANT = DefaultPhysicalConstants.SPRING_CONSTANT.value();
    private static final float DEFAULT_GRAVITATIONAL_ACCELERATION = DefaultPhysicalConstants.GRAVITATIONAL_ACCELERATION.value();

    private static final ForceApplier DEFAULT_FORCE_APPLIER = new ForceApplier();
    private final VertexMover DEFAULT_VERTEX_MOVER = new VertexMover(DEFAULT_DAMPING_COEFFICIENT);
    private final ForceCalculator<VertexType, EdgeType> DEFAULT_FORCE_CALCULATOR =
            new ForceOfGravityCalculatorDecorator<>(DEFAULT_GRAVITATIONAL_ACCELERATION,
                    new SpringForceCalculatorDecorator<>(DEFAULT_SPRING_CONSTANT,
                            new CoulombForceCalculatorDecorator<>(DEFAULT_COULOMB_CONSTANT,
                                    new ConcreteForceCalculator<>())));

    public PhysicsGraphSimulation<VertexType, EdgeType> create() {
        ForceDirectedGraphFactory<VertexType, EdgeType> graphFactory = new ForceDirectedGraphFactory<>();
        Graph<VertexType, EdgeType> emptyGraph = graphFactory.create();
        return create(emptyGraph);
    }

    public PhysicsGraphSimulation<VertexType, EdgeType> create(@NotNull Graph<VertexType, EdgeType> simulatedGraph) {
        return create(simulatedGraph, DEFAULT_TIME_STEP);
    }

    public PhysicsGraphSimulation<VertexType, EdgeType> create(@NotNull Graph<VertexType, EdgeType> simulatedGraph,
                                                               float timeStep) {
        return create(simulatedGraph, timeStep, DEFAULT_FORCE_CALCULATOR);
    }

    public PhysicsGraphSimulation<VertexType, EdgeType> create(@NotNull Graph<VertexType, EdgeType> simulatedGraph,
                                                               float timeStep,
                                                               @NotNull ForceCalculator<VertexType, EdgeType> forceCalculator) {
        return create(simulatedGraph, timeStep, forceCalculator, DEFAULT_FORCE_APPLIER);
    }

    public PhysicsGraphSimulation<VertexType, EdgeType> create(@NotNull Graph<VertexType, EdgeType> simulatedGraph,
                                                               float timeStep,
                                                               @NotNull ForceCalculator<VertexType, EdgeType> forceCalculator,
                                                               @NotNull ForceApplier forceApplier) {
        return create(simulatedGraph, timeStep, forceCalculator, forceApplier, DEFAULT_VERTEX_MOVER);
    }

    public PhysicsGraphSimulation<VertexType, EdgeType> create(@NotNull Graph<VertexType, EdgeType> simulatedGraph,
                                                               float timeStep,
                                                               @NotNull ForceCalculator<VertexType, EdgeType> forceCalculator,
                                                               @NotNull ForceApplier forceApplier,
                                                               @NotNull VertexMover vertexMover) {
        return new PhysicsGraphSimulation<>(simulatedGraph, timeStep, forceCalculator, forceApplier, vertexMover);
    }
}
