package factories;

import geometry.Vector;
import graph.edges.Edge;
import graph.verticies.Vertex;

public interface GraphComponentFactory<VertexType extends Vertex, EdgeType extends Edge<VertexType>> {
    VertexType createVertex(Vector position);
    VertexType createVertex(float x, float y);

    EdgeType createEdge(VertexType originVertex, VertexType targetVertex);
}
