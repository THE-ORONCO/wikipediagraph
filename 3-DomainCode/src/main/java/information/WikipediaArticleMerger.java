package information;

import factories.WikipediaArticleFactory;
import org.jetbrains.annotations.NotNull;

import java.awt.*;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WikipediaArticleMerger {
    public WikipediaArticle mergeArticles(@NotNull WikipediaArticle firstArticle,
                                          @NotNull WikipediaArticle secondArticle,
                                          @NotNull WikipediaArticleFactory articleFactory) throws IllegalArgumentException {
        if (firstArticle.getPageID() != secondArticle.getPageID() ||
                !firstArticle.getTitle().equals(secondArticle.getTitle())) {
            throw new IllegalArgumentException("These articles cannot be merged");
        }
        String newTitle = Optional.of(firstArticle.getTitle()).orElse(secondArticle.getTitle());

        int newPageID;
        if (firstArticle.getPageID() > 0) newPageID = firstArticle.getPageID();
        else newPageID = secondArticle.getPageID();

        String newExtract = firstArticle.getExtract() + secondArticle.getExtract();
        List<String> newExternalLinks = Stream
                .concat(firstArticle.getExternalLinks().stream(),
                        secondArticle.getExternalLinks().stream())
                .collect(Collectors.toList());
        List<String> newTitlesOfLinkedArticles = Stream
                .concat(firstArticle.getTitlesOfLinkedArticles().stream(),
                        secondArticle.getTitlesOfLinkedArticles().stream())
                .collect(Collectors.toList());
        Image newThumbnail = Optional.ofNullable(firstArticle.getThumbnail()).orElse(secondArticle.getThumbnail());

        return articleFactory.create(
                newTitle,
                newPageID,
                newExtract,
                newExternalLinks,
                newTitlesOfLinkedArticles,
                newThumbnail);
    }


    public WikipediaArticle mergeArticles(@NotNull WikipediaArticle firstArticle, @NotNull WikipediaArticle secondArticle) {
        return mergeArticles(firstArticle, secondArticle, new WikipediaArticleFactory());
    }

}
