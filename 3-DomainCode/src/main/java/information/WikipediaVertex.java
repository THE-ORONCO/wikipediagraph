package information;

import geometry.Vector;
import graph.verticies.WeightedVertex;
import org.jetbrains.annotations.NotNull;

public class WikipediaVertex extends WeightedVertex {
    private WikipediaArticle associatedArticle;

    public WikipediaVertex(float x, float y, float weight, @NotNull WikipediaArticle article) {
        super(x, y, weight);
        this.associatedArticle = article;
    }

    public WikipediaVertex(Vector position, float weight, @NotNull WikipediaArticle article) {
        super(position, weight);
        this.associatedArticle = article;
    }

    public WikipediaArticle getAssociatedArticle() {
        return associatedArticle;
    }

    public void setAssociatedArticle(@NotNull WikipediaArticle associatedArticle) {
        this.associatedArticle = associatedArticle;
    }

    public String getArticleTitle(){
        return associatedArticle.getTitle();
    }

    public int getArticleId(){
        return associatedArticle.getPageID();
    }

    @Override
    public String toString() {
        return "WikipediaVertex{" +
                "associatedArticle=" + associatedArticle +
                '}';
    }
}
