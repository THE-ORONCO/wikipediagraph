package information;

import org.jetbrains.annotations.NotNull;

import java.awt.*;
import java.util.List;
import java.util.Objects;

public class WikipediaArticle {
    private final String title;
    private final int pageID;
    private String extract;
    private List<String> externalLinks;
    private List<String> titlesOfLinkedArticles;
    private final Image thumbnail;

    /**
     * This object represents a wikipedia article.
     *
     * @param title                  title of the article
     * @param pageID                 unique pageid that identifies the article in Wikipedia
     * @param extract                beginning or short snipped of the article
     * @param externalLinks          links that link to sources outside of wikipedia
     * @param titlesOfLinkedArticles titles of the articles this article links to (unique for each language)
     * @param thumbnail              thumbnail of the article
     */
    public WikipediaArticle(@NotNull String title,
                            int pageID,
                            @NotNull String extract,
                            @NotNull List<String> externalLinks,
                            @NotNull List<String> titlesOfLinkedArticles,
                            @NotNull Image thumbnail) {
        this.title = title;
        this.pageID = pageID;
        this.extract = extract;
        this.externalLinks = externalLinks;
        this.titlesOfLinkedArticles = titlesOfLinkedArticles;
        this.thumbnail = thumbnail;
    }

    public void setExtract(String extract) {
        this.extract = extract;
    }

    public void setExternalLinks(List<String> externalLinks) {
        this.externalLinks = externalLinks;
    }

    public void setTitlesOfLinkedArticles(List<String> titlesOfLinkedArticles) {
        this.titlesOfLinkedArticles = titlesOfLinkedArticles;
    }

    public String getTitle() {
        return title;
    }

    public int getPageID() {
        return pageID;
    }

    public String getExtract() {
        return extract;
    }

    public List<String> getExternalLinks() {
        return externalLinks;
    }

    public List<String> getTitlesOfLinkedArticles() {
        return titlesOfLinkedArticles;
    }

    public Image getThumbnail() {
        return thumbnail;
    }
    @Override
    public String toString() {
        return "WikipediaArticle:\n" +
                "title: '" + title + '\'' +
                " (" + pageID + ")\n" +
                extract + '\n' +
                "externalLinks: " + externalLinks + "\n" +
                "titlesOfLinkedArticles: " + titlesOfLinkedArticles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WikipediaArticle)) return false;
        WikipediaArticle that = (WikipediaArticle) o;
        return getPageID() == that.getPageID() && getTitle().equals(that.getTitle());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTitle(), getPageID());
    }
}
