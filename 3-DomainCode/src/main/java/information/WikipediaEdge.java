package information;

import graph.edges.SpringLikeEdge;
import org.jetbrains.annotations.NotNull;

/**
 * Wrapper for a SpringLikeEdge to reduce writing effort when creating new Edges for a Wikipedia Graph.
 */
public class WikipediaEdge extends SpringLikeEdge<WikipediaVertex> {
    public WikipediaEdge(@NotNull WikipediaVertex originVertex,
                         @NotNull WikipediaVertex targetVertex,
                         float weight, float naturalSpringLength) throws IllegalArgumentException {
        super(originVertex, targetVertex, weight, naturalSpringLength);
    }
}
