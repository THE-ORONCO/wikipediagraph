package physics.forceCalculators;

import geometry.Vector;
import graph.Graph;
import graph.edges.SpringLikeEdge;
import graph.verticies.WeightedVertex;
import org.jetbrains.annotations.NotNull;

public class ForceOfGravityCalculatorDecorator<VertexType extends WeightedVertex, EdgeType extends SpringLikeEdge<VertexType>>
        extends ForceCalculatorDecorator<VertexType, EdgeType> {
    private float gravitationalAcceleration;

    public ForceOfGravityCalculatorDecorator(float gravitationalAcceleration,
                                             @NotNull ForceCalculator<VertexType, EdgeType> decoratedForceCalculator) {
        super(decoratedForceCalculator);
        this.gravitationalAcceleration = gravitationalAcceleration;
    }

    /**
     * This Method calculates the force of gravity acting on a given vertex towards the center of the coordinate space.
     *
     * @param currentVertex vertex the force is acting on
     * @param graph         graph that contains the vertexL
     * @return the calculated force acting on the given vertex
     */
    @Override
    public Vector calculateForceActingOnVertexFrom(@NotNull VertexType currentVertex,
                                                   @NotNull Graph<VertexType, EdgeType> graph) {
        Vector center = new Vector(0f, 0f);
        Vector vertexPosition = currentVertex.getPosition();
        Vector directionOfGravity = center.subtract(vertexPosition);
        if (directionOfGravity.getX() == 0 && directionOfGravity.getY() == 0) {
            Vector noForce = new Vector(0f, 0f);
            return noForce;
        }
        Vector normalizedDirectionOfGravity = directionOfGravity.toUnitVector();
        Vector accelerationDueToGravity = normalizedDirectionOfGravity.multipliedBy(gravitationalAcceleration);
        Vector gravitationalForce = accelerationDueToGravity.multipliedBy(currentVertex.getWeight());

        Vector forceOfDecoratedForceCalculator =
                this.getDecoratedForceCalculator().calculateForceActingOnVertexFrom(currentVertex, graph);
        return forceOfDecoratedForceCalculator.add(gravitationalForce);
    }

    public float getGravitationalAcceleration() {
        return gravitationalAcceleration;
    }

    public void setGravitationalAcceleration(float gravitationalAcceleration) {
        this.gravitationalAcceleration = gravitationalAcceleration;
    }
}
