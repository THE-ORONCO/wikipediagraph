package physics.forceCalculators;

import geometry.Vector;
import graph.Graph;
import graph.edges.SpringLikeEdge;
import graph.verticies.WeightedVertex;
import org.jetbrains.annotations.NotNull;

// SPECIAL delegation of force application to Force Applier
public interface ForceCalculator<VertexType extends WeightedVertex, EdgeType extends SpringLikeEdge<VertexType>> {

    /**
     * This Method calculates all forces Acting on a given Vertex of the also given graph.
     *
     * @param currentVertex vertex the force is acting on
     * @param graph         graph that contains the vertexL
     * @return the calculated force acting on the given vertex
     */
    Vector calculateForceActingOnVertexFrom(@NotNull VertexType currentVertex,
                                            @NotNull Graph<VertexType, EdgeType> graph);

}
