package physics.forceCalculators;

import geometry.Vector;
import graph.Graph;
import graph.edges.SpringLikeEdge;
import graph.verticies.WeightedVertex;
import org.jetbrains.annotations.NotNull;

public abstract class ForceCalculatorDecorator<VertexType extends WeightedVertex, EdgeType extends SpringLikeEdge<VertexType>>
        implements ForceCalculator<VertexType, EdgeType> {
    private final ForceCalculator<VertexType, EdgeType> decoratedForceCalculator;

    public ForceCalculatorDecorator(ForceCalculator<VertexType, EdgeType> decoratedForceCalculator) {
        this.decoratedForceCalculator = decoratedForceCalculator;
    }

    @Override
    public Vector calculateForceActingOnVertexFrom(@NotNull VertexType currentVertex,
                                                   @NotNull Graph<VertexType, EdgeType> graph) {

        return decoratedForceCalculator.calculateForceActingOnVertexFrom(currentVertex, graph);
    }

    public ForceCalculator<VertexType, EdgeType> getDecoratedForceCalculator() {
        return decoratedForceCalculator;
    }
}
