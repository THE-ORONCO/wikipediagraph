package physics.forceCalculators;

import geometry.Vector;
import graph.Graph;
import graph.edges.SpringLikeEdge;
import graph.verticies.WeightedVertex;
import org.jetbrains.annotations.NotNull;

public class ConcreteForceCalculator<VertexType extends WeightedVertex, EdgeType extends SpringLikeEdge<VertexType>>
        implements ForceCalculator<VertexType, EdgeType> {
    /**
     * This Method returns a force of zero because this is the decorated component thus all the specific force
     * calculations are delegated to the implementations of the decorators.
     *
     * @param currentVertex vertex the force is acting on
     * @param graph         graph that contains the vertex
     * @return the calculated force acting on the given vertex
     */
    @Override
    public Vector calculateForceActingOnVertexFrom(@NotNull VertexType currentVertex,
                                                   @NotNull Graph<VertexType, EdgeType> graph) {
        return new Vector(0, 0);
    }
}
