package physics.forceCalculators;

import geometry.Vector;
import graph.Graph;
import graph.edges.SpringLikeEdge;
import graph.verticies.Vertex;
import graph.verticies.WeightedVertex;
import org.jetbrains.annotations.NotNull;

import java.util.Random;

public class SpringForceCalculatorDecorator<VertexType extends WeightedVertex, EdgeType extends SpringLikeEdge<VertexType>>
        extends ForceCalculatorDecorator<VertexType, EdgeType> {

    private float springConstant;

    public SpringForceCalculatorDecorator(float springConstant,
                                          @NotNull ForceCalculator<VertexType, EdgeType> decoratedForceCalculator) {
        super(decoratedForceCalculator);
        this.springConstant = springConstant;
    }

    /**
     * This Method calculates the spring forces acting on a given vertex from its connected SpringLikeEdges according to
     * Hooke's Law.
     * For more information on the math and stuff see
     * <a href="https://en.wikipedia.org/wiki/Hooke%27s_law#Vector_formulation">wikipedia</a>.
     *
     * @param currentVertex vertex the force is acting on
     * @param graph         graph that contains the vertex
     * @return the calculated force acting on the given vertex
     */
    @Override
    public Vector calculateForceActingOnVertexFrom(@NotNull VertexType currentVertex,
                                                   @NotNull Graph<VertexType, EdgeType> graph) {
        Vector cumulativeSpringForce = new Vector();
        for (SpringLikeEdge<? extends WeightedVertex> edge : graph.findAllEdgesConnectedTo(currentVertex)) {
            Vertex otherVertex = edge.getOtherVertex(currentVertex);
            Vector vectorialDistanceToOtherVertex = currentVertex.vectorialDistanceTo(otherVertex);

            // This sadly has to be inserted her because otherwise two vertices lying on top of each other wouldn't
            // separate without outside help.
            if (vectorialDistanceToOtherVertex.length() == 0) {
                Random random = new Random();
                float verySmallFloatValue = 0.00000001f;
                float smallRandomX = verySmallFloatValue * (random.nextFloat() - 0.5f);
                float smallRandomY = verySmallFloatValue * (random.nextFloat() - 0.5f);
                vectorialDistanceToOtherVertex = new Vector(smallRandomX, smallRandomY);
            }

            Vector springDirection = vectorialDistanceToOtherVertex.toUnitVector();
            float deviationFromNaturalSpringLength = edge.length() - edge.getNaturalSpringLength();
            Vector displacementVectorFromRelaxedPosition = springDirection.multipliedBy(deviationFromNaturalSpringLength);
            Vector springForceFromCurrentEdge =
                    displacementVectorFromRelaxedPosition.multipliedBy(edge.getWeight() * springConstant);
            cumulativeSpringForce = cumulativeSpringForce.add(springForceFromCurrentEdge);
        }

        Vector forceOfDecoratedForceCalculator =
                this.getDecoratedForceCalculator().calculateForceActingOnVertexFrom(currentVertex, graph);

        return forceOfDecoratedForceCalculator.add(cumulativeSpringForce);
    }

    public float getSpringConstant() {
        return springConstant;
    }

    public void setSpringConstant(float springConstant) {
        this.springConstant = springConstant;
    }
}
