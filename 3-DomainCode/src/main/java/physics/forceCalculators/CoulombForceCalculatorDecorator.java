package physics.forceCalculators;

import geometry.Vector;
import graph.Graph;
import graph.edges.SpringLikeEdge;
import graph.verticies.WeightedVertex;
import org.jetbrains.annotations.NotNull;

import java.util.Random;

public class CoulombForceCalculatorDecorator<VertexType extends WeightedVertex, EdgeType extends SpringLikeEdge<VertexType>>
        extends ForceCalculatorDecorator<VertexType, EdgeType> {
    private float coulombConstant;

    public CoulombForceCalculatorDecorator(float coulombConstant,
                                           @NotNull ForceCalculator<VertexType, EdgeType> decoratedForceCalculator) {
        super(decoratedForceCalculator);
        this.coulombConstant = coulombConstant;
    }

    /**
     * This Method calculates the coulomb forces acting on a given vertex from the other vertices of the given graph.
     * For more info on the math and stuff see
     * <a href="https://en.wikipedia.org/wiki/Coulomb%27s_law#System_of_discrete_charges">wikipedia</a>.
     *
     * @param currentVertex vertex the force is acting on
     * @param graph         graph that contains the vertexL
     * @return the calculated force acting on the given vertex
     */
    @Override
    public Vector calculateForceActingOnVertexFrom(@NotNull VertexType currentVertex,
                                                   @NotNull Graph<VertexType, EdgeType> graph) {

        Vector coulombForcesAccumulation = new Vector();
        for (WeightedVertex otherVertex : graph.findAllVerticesExceptFor(currentVertex)) {
            Vector vectorFromOtherVertexToCurrentVertex = otherVertex.vectorialDistanceTo(currentVertex);

            // This check is sadly necessary because in the small edge case that vertices are exactly on top of each
            // other and thus have the distance of zero, there is a chance for a division by zero in the force
            // calculation. To fix that a very small distance in a random direction is used instead of the zero
            // distance. That is not good for testing but necessary to prevent error throwing!
            float distanceOfPossiblyZero = vectorFromOtherVertexToCurrentVertex.length();
            if (distanceOfPossiblyZero == 0f) {
                final float verySmallFloatValue = 0.0000001f;
                distanceOfPossiblyZero = verySmallFloatValue;
                Random random = new Random();
                float x = (float) (random.nextDouble() - 0.5f);
                float y = (float) (random.nextDouble() - 0.5f);
                vectorFromOtherVertexToCurrentVertex = new Vector(verySmallFloatValue * x, verySmallFloatValue * y);
            }
            float distanceSquared = (float) Math.pow(distanceOfPossiblyZero, 2);

            Vector coulombForceForCurrentVertex =
                    vectorFromOtherVertexToCurrentVertex
                            .dividedBy(distanceSquared)
                            .multipliedBy(otherVertex.getWeight());

            coulombForcesAccumulation = coulombForcesAccumulation.add(coulombForceForCurrentVertex);
        }

        Vector coulombForce = coulombForcesAccumulation
                .multipliedBy(currentVertex.getWeight())
                .multipliedBy(coulombConstant);

        Vector forceOfDecoratedForceCalculator =
                this.getDecoratedForceCalculator().calculateForceActingOnVertexFrom(currentVertex, graph);
        return forceOfDecoratedForceCalculator.add(coulombForce);
    }

    public float getCoulombConstant() {
        return coulombConstant;
    }

    public void setCoulombConstant(float coulombConstant) {
        this.coulombConstant = coulombConstant;
    }
}
