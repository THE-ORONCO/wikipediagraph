package physics;

public interface Simulatable {
    void step();
    float getTimeStep();
    void setTimeStep(float timeStep);
}
