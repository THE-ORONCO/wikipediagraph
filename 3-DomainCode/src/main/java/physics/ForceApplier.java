package physics;

import geometry.Vector;
import graph.verticies.WeightedVertex;
import org.jetbrains.annotations.NotNull;

// SPECIAL delegation of force application to Force Applier
public class ForceApplier {

    /**
     * Delegation class for applying forces to vertices.
     */
    public ForceApplier() {
    }

    /**
     * This method applies a Force to a given Vertex over a given time step and thus accelerating it in the direction of
     * the force and changing its velocity.
     *
     * @param forceActingOnVertex the force acting on the given vertex
     * @param givenVertex         vertex the force should be applied on
     * @param timeStep            time step of the acceleration
     */
    public void applyForceToVertexOverTimeStep(@NotNull Vector forceActingOnVertex, @NotNull WeightedVertex givenVertex, float timeStep) {
        Vector currentVertexVelocity = givenVertex.getVelocity();
        Vector velocityDelta =
                forceActingOnVertex.multipliedBy(timeStep)
                        .dividedBy(givenVertex.getWeight());
        Vector newVertexVelocity = currentVertexVelocity.add(velocityDelta);

        givenVertex.setVelocity(newVertexVelocity);
    }
}
