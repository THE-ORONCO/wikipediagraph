package physics;

public enum DefaultPhysicalConstants {
    PHYSICS_TIME_STEP(0.5f),
    DAMPING_COEFFICIENT(0.05f),
    COULOMB_CONSTANT(300f),
    SPRING_CONSTANT(0.01f),
    GRAVITATIONAL_ACCELERATION(1f);

    private final float constant;

    DefaultPhysicalConstants(float constant) {
        this.constant = constant;
    }

    public float value(){
        return constant;
    }
}
