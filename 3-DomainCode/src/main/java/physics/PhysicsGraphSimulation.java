package physics;

import geometry.Vector;
import graph.Graph;
import graph.edges.SpringLikeEdge;
import graph.verticies.WeightedVertex;
import org.jetbrains.annotations.NotNull;
import physics.forceCalculators.ForceCalculator;
import physics.vertexMovers.VertexMover;

public class PhysicsGraphSimulation<VertexType extends WeightedVertex, EdgeType extends SpringLikeEdge<VertexType>>
        implements Simulatable {
    private final Graph<VertexType, EdgeType> simulatedGraph;
    private float timeStep;
    private ForceCalculator<VertexType, EdgeType> forceCalculator;
    private ForceApplier forceApplier;
    private VertexMover vertexMover;

    /**
     * This class wraps an instance of a graph and applies forces to the vertices.
     *
     * @param simulatedGraph  graph that should be used as a simulation target
     * @param timeStep        time step the simulations should occur over
     * @param forceCalculator force calculator that calculates the forces in the graph
     * @param forceApplier    force applier that applies the forces to all the vertices in the graph
     * @param vertexMover     vertex mover that moves the vertices according to their speed and movement direction
     */
    public PhysicsGraphSimulation(@NotNull Graph<VertexType, EdgeType> simulatedGraph,
                                  float timeStep,
                                  @NotNull ForceCalculator<VertexType, EdgeType> forceCalculator,
                                  @NotNull ForceApplier forceApplier,
                                  @NotNull VertexMover vertexMover) {
        this.simulatedGraph = simulatedGraph;
        this.timeStep = timeStep;
        this.forceCalculator = forceCalculator;
        this.forceApplier = forceApplier;
        this.vertexMover = vertexMover;
    }

    /**
     * Do a step in the physics simulation of all vertices.
     */
    @Override
    public void step() {
        synchronized (simulatedGraph) {
            calculateAndApplyForcesActingOnEachVertex();
            moveVerticesAccordingToTheirCurrentVelocity();
        }
    }

    @Override
    public float getTimeStep() {
        return this.timeStep;
    }

    @Override
    public void setTimeStep(float timeStep) {
        this.timeStep = timeStep;
    }

    /**
     * This method calculates the forces acting on all vertices with the given ForceCalculator and applies them with
     * the given ForceApplier to the vertices thus accelerating them.
     */
    private void calculateAndApplyForcesActingOnEachVertex() {
        for (VertexType currentVertex : simulatedGraph.getVertices()) {
            Vector forceActingOnCurrentVertex = forceCalculator.calculateForceActingOnVertexFrom(currentVertex, simulatedGraph);
            forceApplier.applyForceToVertexOverTimeStep(forceActingOnCurrentVertex, currentVertex, timeStep);
        }
    }

    /**
     * This method moves changes the positions of all vertices according to the acceleration acting on all vertices with
     * the given vertexMover.
     */
    private void moveVerticesAccordingToTheirCurrentVelocity() {
        for (VertexType movedVertex : simulatedGraph.getVertices())
            vertexMover.moveVertexAccordingToHisCurrentVelocityOverTimeStep(movedVertex, timeStep);
    }

    public Graph<VertexType, EdgeType> getSimulatedGraph() {
        return simulatedGraph;
    }

    public ForceCalculator<VertexType, EdgeType> getForceCalculator() {
        return forceCalculator;
    }

    public void setForceCalculator(ForceCalculator<VertexType, EdgeType> forceCalculator) {
        this.forceCalculator = forceCalculator;
    }

    public ForceApplier getForceApplier() {
        return forceApplier;
    }

    public void setForceApplier(ForceApplier forceApplier) {
        this.forceApplier = forceApplier;
    }

    public VertexMover getVertexMover() {
        return vertexMover;
    }

    public void setVertexMover(VertexMover vertexMover) {
        this.vertexMover = vertexMover;
    }
}
