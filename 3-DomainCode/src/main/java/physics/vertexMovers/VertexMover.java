package physics.vertexMovers;

import geometry.Vector;
import graph.verticies.WeightedVertex;
import org.jetbrains.annotations.NotNull;

// SPECIAL delegation of vertex moving to VertexMover
public class VertexMover {

    private float dampingCoefficient;

    /**
     * Delegation class for moving weighted vertices according to their current velocity.
     *
     * @param dampingCoefficient the damping coefficient that slows the vertex down on each physics calculation
     */
    public VertexMover(float dampingCoefficient) {
        this.dampingCoefficient = dampingCoefficient;
    }

    /**
     * This method moves a single vertex according to its velocity over a given time step.
     *
     * @param vertex   vertex that should be moved
     * @param timeStep time step the movement should occur over
     */
    public void moveVertexAccordingToHisCurrentVelocityOverTimeStep(@NotNull WeightedVertex vertex, float timeStep) {
        Vector currentVelocity = vertex.getVelocity();

        Vector movementDuringTimeStep = currentVelocity.multipliedBy(timeStep);
        vertex.translate(movementDuringTimeStep);

        Vector velocityAfterMovement = currentVelocity.multipliedBy(dampingCoefficient);
        vertex.setVelocity(velocityAfterMovement);
    }

    public float getDampingCoefficient() {
        return dampingCoefficient;
    }

    public void setDampingCoefficient(float dampingCoefficient) {
        this.dampingCoefficient = dampingCoefficient;
    }
}
