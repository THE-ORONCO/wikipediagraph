package physics.vertexMovers;

import geometry.Vector;
import graph.verticies.WeightedVertex;
import org.jetbrains.annotations.NotNull;

public class ConstrainedVertexMover extends VertexMover {
    private float xBound;
    private float yBound;

    /**
     * Delegation class for moving weighted vertices according to their current velocity with a boundary constraint.
     *
     * @param xBound             describes the right (xBound) and left (-xBound) most positions a Vertex can be moved to
     * @param yBound             describes the top (yBound) and bottom (-yBound) most positions a Vertex can be moved to
     * @param dampingCoefficient the damping coefficient that slows the vertex down on each physics calculation
     */
    public ConstrainedVertexMover(float dampingCoefficient, float xBound, float yBound) {
        super(dampingCoefficient);
        this.xBound = xBound;
        this.yBound = yBound;
    }

    /**
     * This method moves a single vertex according to its velocity over a given time step while keeping it within the
     * defined bounds.
     *
     * @param vertex   vertex that should be moved
     * @param timeStep time step the movement should occur over
     */
    @Override
    public void moveVertexAccordingToHisCurrentVelocityOverTimeStep(@NotNull WeightedVertex vertex, float timeStep) {
        Vector currentVelocity = vertex.getVelocity();

        Vector oldPosition = vertex.getPosition();
        Vector movementDuringTimeStep = currentVelocity.multipliedBy(timeStep);
        Vector newPosition = clamp(oldPosition.add(movementDuringTimeStep));

        vertex.setPositionTo(newPosition);

        Vector velocityAfterMovement = currentVelocity.multipliedBy(this.getDampingCoefficient());
        vertex.setVelocity(velocityAfterMovement);
    }

    /**
     * @param value value that should be clamped
     * @param max   maximum the value should be under
     * @param min   minimum the value should be above
     * @return the clamped value
     */
    private float clamp(float value, float min, float max) {
        return Math.max(min, Math.min(max, value));
    }

    private Vector clamp(Vector position) {
        float xPosition = clamp(position.getX(), -xBound, xBound);
        float yPosition = clamp(position.getY(), -yBound, yBound);
        return new Vector(xPosition, yPosition);
    }

    public float getxBound() {
        return xBound;
    }

    public float getyBound() {
        return yBound;
    }

    public void setxBound(float xBound) {
        this.xBound = xBound;
    }

    public void setyBound(float yBound) {
        this.yBound = yBound;
    }
}
