package physics;

import graph.Graph;
import graph.edges.SpringLikeEdge;
import graph.edges.WeightedEdge;
import graph.verticies.WeightedVertex;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * This represents a force directed graph. It uses weighted vertices and edges and applies natural forces on the
 * vertices. All vertices repel each other given by the coulomb force and all edges pull vertices together like springs
 * given by hooke's law.
 * See <a href="https://en.wikipedia.org/wiki/Force-directed_graph_drawing">wikipedia</a> for more information.
 */
public class ForceDirectedGraph<VertexType extends WeightedVertex, EdgeType extends SpringLikeEdge<VertexType>>
        implements Graph<VertexType, EdgeType> {

    // IDEA make this a Map of ID and Vertex -> would need every vertex to have an ID
    private final Set<VertexType> vertices;
    private final Set<EdgeType> edges;

    public ForceDirectedGraph(@NotNull List<VertexType> vertices,
                              @NotNull List<EdgeType> edges) {
        this.vertices = new HashSet<>(vertices);
        this.edges = new HashSet<>(edges);
    }

    @Override
    public boolean add(@NotNull VertexType vertex) {
        return vertices.add(vertex);
    }


    @Override
    public boolean add(@NotNull EdgeType edge) {
        add(edge.getOriginVertex());
        add(edge.getTargetVertex());
        return edges.add(edge);
    }

    @Override
    public boolean remove(@NotNull VertexType vertex) {
        for (EdgeType edge : this.findAllEdgesConnectedTo(vertex))
            remove(edge);

        return this.vertices.remove(vertex);
    }

    @Override
    public boolean remove(@NotNull EdgeType edge) {
        return edges.remove(edge);
    }

    @Override
    public boolean contains(@NotNull VertexType vertex) {
        return vertices.contains(vertex);
    }

    @Override
    public boolean contains(@NotNull EdgeType edge) {
        return edges.contains(edge);
    }


    @Override
    public List<VertexType> getVertices() {
        return new ArrayList<>(vertices);
    }

    @Override
    public List<EdgeType> getEdges() {
        return new ArrayList<>(this.edges);
    }


    @Override
    public List<EdgeType> findEdgesBetween(@NotNull VertexType originVertex, @NotNull VertexType targetVertex) {
        List<EdgeType> relevantEdges = new ArrayList<>();
        for (EdgeType edge : edges)
            if (edge.getOriginVertex().equals(originVertex) && edge.getTargetVertex().equals(targetVertex) ||
                    edge.getOriginVertex().equals(targetVertex) && edge.getTargetVertex().equals(originVertex))
                relevantEdges.add(edge);

        return relevantEdges;
    }

    @Override
    public List<EdgeType> findAllEdgesConnectedTo(@NotNull VertexType givenVertex) {
        List<EdgeType> edgesConnectedToGivenVertex = new ArrayList<>();
        for (EdgeType edge : edges) {
            if (givenVertex.equals(edge.getOriginVertex()) || givenVertex.equals(edge.getTargetVertex())) {
                edgesConnectedToGivenVertex.add(edge);
            }
        }
        return edgesConnectedToGivenVertex;
    }

    @Override
    public List<EdgeType> findAllEdgesOriginatingFrom(VertexType originVertex) {
        return edges.stream().filter(edge -> edge.getOriginVertex().equals(originVertex))
                .collect(Collectors.toList());
    }

    @Override
    public List<EdgeType> findAllEdgesGoingTo(VertexType targetVertex) {
        return edges.stream().filter(edge -> edge.getTargetVertex().equals(targetVertex))
                .collect(Collectors.toList());
    }

    @Override
    public List<VertexType> findAllNeighbouringVertices(@NotNull VertexType vertex) {
        List<VertexType> originNeighbours = findAllEdgesOriginatingFrom(vertex).stream().map(WeightedEdge::getTargetVertex).collect(Collectors.toList());
        ArrayList<VertexType> neighbours = new ArrayList<>(originNeighbours);
        List<VertexType> targetNeighbours = findAllEdgesGoingTo(vertex).stream().map(WeightedEdge::getOriginVertex).collect(Collectors.toList());
        neighbours.addAll(targetNeighbours);
        return neighbours;
    }

    /**
     * This method returns all vertices except for
     *
     * @param unwantedVertex the vertex that should be excluded
     * @return all vertices in the graph except the one given in form of a list
     */
    @Override
    public List<VertexType> findAllVerticesExceptFor(@NotNull VertexType unwantedVertex) {
        return vertices.stream().filter(vertex -> !vertex.equals(unwantedVertex)).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "ForceDirectedGraph{" +
                ", " + vertices.size() + " vertices: " + vertices +
                ", " + edges.size() + " edges: " + edges +
                '}';
    }

}
