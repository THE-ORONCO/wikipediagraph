package factories.forcedirectedComponents;

import graph.edges.SpringLikeEdge;
import graph.verticies.WeightedVertex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import physics.ForceDirectedGraph;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ForceDirectedGraphFactoryTest {
    private ForceDirectedGraphFactory<WeightedVertex, SpringLikeEdge<WeightedVertex>> testedFactory;

    private List<WeightedVertex> weightedVertices;
    private List<SpringLikeEdge<WeightedVertex>> springLikeEdges;
    @Mock
    private WeightedVertex originVertexInVertexList;
    @Mock
    private WeightedVertex targetVertexInVertexList;
    @Mock
    private WeightedVertex originVertexNotInVertexList;
    @Mock
    private WeightedVertex targetVertexNotInVertexList;
    @Mock
    private SpringLikeEdge<WeightedVertex> edgeWithBothVerticesInVertexList;
    @Mock
    private SpringLikeEdge<WeightedVertex> edgeWithNoVertexInVertexList;

    private List<WeightedVertex> vertexList;

    private List<SpringLikeEdge<WeightedVertex>> edgeListWithAllVerticesInVertexList;
    private List<SpringLikeEdge<WeightedVertex>> edgeListWithDanglingOrLonelyEdges;


    @BeforeEach
    void setup() {
        testedFactory = new ForceDirectedGraphFactory<>();
        originVertexInVertexList = mock(WeightedVertex.class);
        targetVertexInVertexList = mock(WeightedVertex.class);
        originVertexNotInVertexList = mock(WeightedVertex.class);
        targetVertexNotInVertexList = mock(WeightedVertex.class);

        edgeWithBothVerticesInVertexList = mock(SpringLikeEdge.class);
        when(edgeWithBothVerticesInVertexList.getOriginVertex()).thenReturn(originVertexInVertexList);
        when(edgeWithBothVerticesInVertexList.getTargetVertex()).thenReturn(targetVertexInVertexList);

        edgeWithNoVertexInVertexList = mock(SpringLikeEdge.class);
        when(edgeWithNoVertexInVertexList.getOriginVertex()).thenReturn(originVertexNotInVertexList);
        when(edgeWithNoVertexInVertexList.getTargetVertex()).thenReturn(targetVertexNotInVertexList);

        vertexList = new ArrayList<>() {{
            add(originVertexInVertexList);
            add(targetVertexInVertexList);
        }};
        edgeListWithAllVerticesInVertexList = new ArrayList<>() {{
            add(edgeWithBothVerticesInVertexList);
        }};
        edgeListWithDanglingOrLonelyEdges = new ArrayList<>() {{
            add(edgeWithBothVerticesInVertexList);
            add(edgeWithNoVertexInVertexList);
        }};

    }

    @Test
    @DisplayName("test if a graph can be created without any parameters")
    void testCreateWithoutParameters() {
        ForceDirectedGraph<WeightedVertex, SpringLikeEdge<WeightedVertex>> createdGraph = testedFactory.create();
        assertNotNull(createdGraph.getEdges());
        assertNotNull(createdGraph.getVertices());
    }

    @Test
    @DisplayName("test if a graph is created with the given parameters")
    void testCreate() {
        ForceDirectedGraph<WeightedVertex, SpringLikeEdge<WeightedVertex>> createdGraph
                = testedFactory.create(vertexList, edgeListWithAllVerticesInVertexList);
        assertTrue(createdGraph.contains(originVertexInVertexList));
        assertTrue(createdGraph.contains(targetVertexInVertexList));
        assertTrue(createdGraph.contains(edgeWithBothVerticesInVertexList));
        assertEquals(createdGraph.getVertices().size(), vertexList.size());
        assertEquals(createdGraph.getEdges().size(), edgeListWithAllVerticesInVertexList.size());
    }

    /**
     * BUMP
     * This test found out that the implementation for adding missing vertices was problematic because it altered the
     * given list of vertices and added the missing vertices thus changing stuff out of the scope of the factory.
     */
    @Test
    @DisplayName("test if Vertices that are in edges but not in the given vertex list are still added on creation")
    void testCreateWithVerticesInEdgesButNotInVertexList() {
        ForceDirectedGraph<WeightedVertex, SpringLikeEdge<WeightedVertex>> createdGraph
                = testedFactory.create(vertexList, edgeListWithDanglingOrLonelyEdges);
        assertTrue(createdGraph.contains(originVertexInVertexList));
        assertTrue(createdGraph.contains(targetVertexInVertexList));

        assertTrue(createdGraph.contains(edgeWithBothVerticesInVertexList));
        assertTrue(createdGraph.contains(edgeWithNoVertexInVertexList));

        assertTrue(createdGraph.contains(originVertexNotInVertexList));
        assertTrue(createdGraph.contains(targetVertexNotInVertexList));

        assertTrue(createdGraph.getVertices().size() > vertexList.size());
        assertEquals(createdGraph.getEdges().size(), edgeListWithDanglingOrLonelyEdges.size());
    }
}