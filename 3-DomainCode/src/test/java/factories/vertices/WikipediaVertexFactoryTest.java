package factories.vertices;

import geometry.Vector;
import information.WikipediaArticle;
import information.WikipediaVertex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

class WikipediaVertexFactoryTest {
    final float oneX = 1;
    final float twoY = 2;
    final Vector oneTwoVector = new Vector(oneX, twoY);
    final float weightOfOne = 1f;

    private WikipediaVertexFactory testedFactory;

    @Mock
    private WikipediaArticle mockedArticle;

    @BeforeEach
    void setUp() {
        mockedArticle = mock(WikipediaArticle.class);
        testedFactory = new WikipediaVertexFactory();
    }

    @Test
    @DisplayName("test if the factory can create a vertex when only an article is given")
    void testCreateWithOnlyArticle() {
        WikipediaVertex createdVertex = testedFactory.create(mockedArticle);
        assertEquals(createdVertex.getAssociatedArticle(), mockedArticle);
        assertNotNull(createdVertex.getPosition());
        assertNotNull(createdVertex.getVelocity());
        createdVertex.getArticleId();
        verify(mockedArticle, atLeastOnce()).getPageID();
        createdVertex.getArticleTitle();
        verify(mockedArticle, atLeastOnce()).getTitle();
    }

    @Test
    @DisplayName("test if the factory can create a vertex when only an article is given")
    void testCreateWith() {
        WikipediaVertex vertexCreatedWithVector = testedFactory.create(oneTwoVector, weightOfOne, mockedArticle);
        WikipediaVertex vertexCreatedWithValues = testedFactory.create(oneX, twoY, weightOfOne, mockedArticle);
        assertEquals(vertexCreatedWithValues.getPosition(), vertexCreatedWithVector.getPosition());
        assertEquals(vertexCreatedWithValues.getWeight(), vertexCreatedWithVector.getWeight());

        assertEquals(vertexCreatedWithValues.getPosition(), oneTwoVector);
        assertEquals(vertexCreatedWithVector.getPosition(), oneTwoVector);

        assertEquals(vertexCreatedWithValues.getX(), vertexCreatedWithVector.getX());
        assertEquals(vertexCreatedWithValues.getY(), vertexCreatedWithVector.getY());

        assertEquals(vertexCreatedWithValues.getX(), oneX);
        assertEquals(vertexCreatedWithValues.getY(), twoY);

        assertEquals(vertexCreatedWithVector.getX(), oneTwoVector.getX());
        assertEquals(vertexCreatedWithVector.getY(), oneTwoVector.getY());

        assertEquals(vertexCreatedWithValues.getWeight(), weightOfOne);
        assertEquals(vertexCreatedWithVector.getWeight(), weightOfOne);

        assertEquals(vertexCreatedWithValues.getAssociatedArticle(), vertexCreatedWithVector.getAssociatedArticle());
    }

}