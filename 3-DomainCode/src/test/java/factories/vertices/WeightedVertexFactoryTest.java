package factories.vertices;

import geometry.Vector;
import graph.verticies.WeightedVertex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class WeightedVertexFactoryTest {
    final float zeroX = 0;
    final float zeroY = 0;
    final Vector zeroZeroVector = new Vector(zeroX, zeroY);
    final float oneX = 1;
    final float twoY = 2;
    final Vector oneTwoVector = new Vector(oneX, twoY);
    final float weightOfOne = 1f;

    private WeightedVertexFactory testedFactory;

    @BeforeEach
    void  setup(){
        testedFactory = new WeightedVertexFactory();
    }

    @Test
    @DisplayName("test if vertices without arguments are created at zero zero")
    void testCreateVertexWithoutArguments() {
        WeightedVertex createdVertex =  testedFactory.createVertex();
        assertEquals(createdVertex.getX(), zeroX);
        assertEquals(createdVertex.getY(), zeroY);
        assertEquals(createdVertex.getPosition(), zeroZeroVector);
        assertEquals(createdVertex.getWeight(), weightOfOne);
    }

    @Test
    @DisplayName("test if vertices created with vectors and values that are the same as the vector are the same")
    void testCreateVertexWithVectorAndCoordinates() {
        WeightedVertex vertexCreatedWithVector =  testedFactory.create(oneTwoVector, weightOfOne);
        WeightedVertex vertexCreatedWithValues =  testedFactory.create(oneX, twoY, weightOfOne);
        assertEquals(vertexCreatedWithValues.getPosition(), vertexCreatedWithVector.getPosition());
        assertEquals(vertexCreatedWithValues.getWeight(), vertexCreatedWithVector.getWeight());

        assertEquals(vertexCreatedWithValues.getPosition(), oneTwoVector);
        assertEquals(vertexCreatedWithVector.getPosition(), oneTwoVector);

        assertEquals(vertexCreatedWithValues.getX(), vertexCreatedWithVector.getX());
        assertEquals(vertexCreatedWithValues.getY(), vertexCreatedWithVector.getY());

        assertEquals(vertexCreatedWithValues.getX(), oneX);
        assertEquals(vertexCreatedWithValues.getY(), twoY);

        assertEquals(vertexCreatedWithVector.getX(), oneTwoVector.getX());
        assertEquals(vertexCreatedWithVector.getY(), oneTwoVector.getY());

        assertEquals(vertexCreatedWithValues.getWeight(), weightOfOne);
        assertEquals(vertexCreatedWithVector.getWeight(), weightOfOne);
    }
}