package factories;

import information.WikipediaArticle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class WikipediaArticleFactoryTest {
    private WikipediaArticleFactory testedFactory;
    private final String articleTitle = "Test Title";
    private final int articleID = 69;
    private final String articleExtract = "extract";
    private List<String> externalArticleLinks;
    private List<String> titlesOfLinkedArticles;
    @Mock
    private Image thumbnailImageMock = mock(Image.class);

    @BeforeEach
    private void setup() {
        testedFactory = new WikipediaArticleFactory();
        externalArticleLinks = new ArrayList<>() {{
            add("external_link_1");
            add("external_link_2");
        }};
        titlesOfLinkedArticles = new ArrayList<>() {{
            add("title1");
            add("title2");
        }};
    }

    @Test
    @DisplayName("test if article can be created when only a title is given")
    void testCreateWithOnlyTitle() {
        WikipediaArticle createdArticle = testedFactory.create(articleTitle);
        assertEquals(createdArticle.getTitle(), articleTitle);
        assertNotNull(createdArticle.getExternalLinks());
        assertNotNull(createdArticle.getTitlesOfLinkedArticles());
        assertNotNull(createdArticle.getThumbnail());
        assertTrue(createdArticle.getPageID() < 0);
    }

    @Test
    @DisplayName("test if an the factory can automatically add missing parameters when only given a title and an ID")
    void testCreateWithTitleAndID() {
        WikipediaArticle createdArticle = testedFactory.create(articleTitle, articleID);
        assertEquals(createdArticle.getTitle(), articleTitle);
        assertEquals(createdArticle.getPageID(), articleID);
        assertNotNull(createdArticle.getExternalLinks());
        assertNotNull(createdArticle.getTitlesOfLinkedArticles());
        assertNotNull(createdArticle.getThumbnail());
    }

    @Test
    @DisplayName("test if an the factory can create an article when all arguments are given")
    void testCreateWithAllParameters() {
        WikipediaArticle createdArticle = testedFactory.create(articleTitle,
                articleID,
                articleExtract,
                externalArticleLinks,
                titlesOfLinkedArticles,
                thumbnailImageMock);
        assertEquals(createdArticle.getTitle(), articleTitle);
        assertEquals(createdArticle.getPageID(), articleID);
        assertEquals(createdArticle.getExtract(), articleExtract);
        assertEquals(createdArticle.getExternalLinks(), externalArticleLinks);
        assertEquals(createdArticle.getTitlesOfLinkedArticles(), titlesOfLinkedArticles);
        assertEquals(createdArticle.getThumbnail(), thumbnailImageMock);
    }
}