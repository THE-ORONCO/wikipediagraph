package factories;

import graph.Graph;
import graph.edges.SpringLikeEdge;
import graph.verticies.WeightedVertex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import physics.ForceApplier;
import physics.PhysicsGraphSimulation;
import physics.forceCalculators.ForceCalculator;
import physics.vertexMovers.VertexMover;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

class PhysicsGraphSimulationFactoryTest {
    PhysicsGraphSimulationFactory<WeightedVertex, SpringLikeEdge<WeightedVertex>> testedFactory;

    @Mock
    private Graph<WeightedVertex, SpringLikeEdge<WeightedVertex>> graphMock;
    private final float timeStep = 10;
    @Mock
    private ForceCalculator<WeightedVertex, SpringLikeEdge<WeightedVertex>> forceCalculatorMock;
    @Mock
    private ForceApplier forceApplierMock;
    @Mock
    private VertexMover vertexMoverMock;

    @BeforeEach
    void setUp() {
        testedFactory = new PhysicsGraphSimulationFactory<>();
        graphMock = mock(Graph.class);
        forceCalculatorMock = mock(ForceCalculator.class);
        forceApplierMock = mock(ForceApplier.class);
        vertexMoverMock = mock(VertexMover.class);
    }

    @Test
    @DisplayName("test if a simulation can be created without any parameters")
    void testCreateWithoutArguments() {
        PhysicsGraphSimulation<WeightedVertex, SpringLikeEdge<WeightedVertex>> createdSimulation = testedFactory.create();
        assertNotNull(createdSimulation.getSimulatedGraph());
        assertNotNull(createdSimulation.getForceCalculator());
        assertNotNull(createdSimulation.getForceApplier());
        assertNotNull(createdSimulation.getVertexMover());
    }

    @Test
    @DisplayName("test if a simulation can be created without all parameters")
    void testCreateWithAllArguments() {
        PhysicsGraphSimulation<WeightedVertex, SpringLikeEdge<WeightedVertex>> createdSimulation
                = testedFactory.create(graphMock, timeStep, forceCalculatorMock, forceApplierMock, vertexMoverMock);
        assertNotNull(createdSimulation.getSimulatedGraph());
        assertEquals(createdSimulation.getSimulatedGraph(), graphMock);
        assertNotNull(createdSimulation.getForceCalculator());
        assertEquals(createdSimulation.getForceCalculator(), forceCalculatorMock);
        assertNotNull(createdSimulation.getForceApplier());
        assertEquals(createdSimulation.getForceApplier(), forceApplierMock);
        assertNotNull(createdSimulation.getVertexMover());
        assertEquals(createdSimulation.getVertexMover(), vertexMoverMock);
    }
}