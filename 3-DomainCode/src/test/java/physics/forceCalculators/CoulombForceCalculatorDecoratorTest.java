package physics.forceCalculators;

import factories.forcedirectedComponents.DefaultForceDirectedGraphComponentFactory;
import factories.forcedirectedComponents.ForceDirectedGraphFactory;
import geometry.Vector;
import graph.edges.SpringLikeEdge;
import graph.verticies.WeightedVertex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import physics.ForceDirectedGraph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CoulombForceCalculatorDecoratorTest {
    @Mock
    ForceCalculator<WeightedVertex, SpringLikeEdge<WeightedVertex>> forceCalculatorMock = mock(ForceCalculator.class);
    @Mock
    WeightedVertex targetVertexMock = mock(WeightedVertex.class);
    @Mock
    WeightedVertex otherVertexToTheTopRightMock = mock(WeightedVertex.class);
    @Mock
    WeightedVertex otherVertexToTheRightMock = mock(WeightedVertex.class);

    @Mock
    ForceDirectedGraph<WeightedVertex, SpringLikeEdge<WeightedVertex>> forceDirectedGraphMockWithOtherVertexToTheRight = mock(ForceDirectedGraph.class);
    @Mock
    ForceDirectedGraph<WeightedVertex, SpringLikeEdge<WeightedVertex>> forceDirectedGraphMockWithOtherVertexToTopTheRight = mock(ForceDirectedGraph.class);
    @Mock
    ForceDirectedGraph<WeightedVertex, SpringLikeEdge<WeightedVertex>> forceDirectedGraphMockWithBothOtherVertices = mock(ForceDirectedGraph.class);


    Vector neutralVector = new Vector(0f, 0f);


    private ForceDirectedGraph<WeightedVertex, SpringLikeEdge<WeightedVertex>> testedGraph;
    private ForceDirectedGraphFactory<WeightedVertex, SpringLikeEdge<WeightedVertex>> graphFactory;
    private DefaultForceDirectedGraphComponentFactory componentFactory;

    @BeforeEach
    void setup() {
        componentFactory = new DefaultForceDirectedGraphComponentFactory();
        graphFactory = new ForceDirectedGraphFactory<>();
        testedGraph = graphFactory.create();
    }

    /**
     * BUMP ended up uncovering that I calculated the force in the wrong direction
     */
    @Test
    @DisplayName("test if coulomb force is applied to vertices")
    void testCalculateForceActingOnVertexOfGraph() {
        when(forceCalculatorMock.calculateForceActingOnVertexFrom(targetVertexMock, forceDirectedGraphMockWithBothOtherVertices)).thenReturn(neutralVector);
        when(forceCalculatorMock.calculateForceActingOnVertexFrom(targetVertexMock, forceDirectedGraphMockWithOtherVertexToTheRight)).thenReturn(neutralVector);
        when(forceCalculatorMock.calculateForceActingOnVertexFrom(targetVertexMock, forceDirectedGraphMockWithOtherVertexToTopTheRight)).thenReturn(neutralVector);

        when(targetVertexMock.getWeight()).thenReturn(1f);
        when(otherVertexToTheTopRightMock.getWeight()).thenReturn(5f);
        when(otherVertexToTheRightMock.getWeight()).thenReturn(10f);

        when(otherVertexToTheTopRightMock.getPosition()).thenReturn(new Vector(1f, 1f));
        when(otherVertexToTheRightMock.getPosition()).thenReturn(new Vector(4f, 0f));
        when(targetVertexMock.getPosition()).thenReturn(new Vector(0f, 0f));

        when(targetVertexMock.vectorialDistanceTo(otherVertexToTheTopRightMock)).thenReturn(new Vector(1f, 1f));
        when(targetVertexMock.distanceTo(otherVertexToTheTopRightMock)).thenReturn((float) Math.sqrt(2));
        when(targetVertexMock.vectorialDistanceTo(otherVertexToTheRightMock)).thenReturn(new Vector(4f, 0f));
        when(targetVertexMock.distanceTo(otherVertexToTheRightMock)).thenReturn(4f);

        when(otherVertexToTheTopRightMock.vectorialDistanceTo(targetVertexMock)).thenReturn(new Vector(-1f, -1f));
        when(otherVertexToTheRightMock.vectorialDistanceTo(targetVertexMock)).thenReturn(new Vector(-4f, 0f));

        ArrayList<WeightedVertex> listWithVertexToTheRight = new ArrayList<>(Collections.singletonList(otherVertexToTheRightMock));
        when(forceDirectedGraphMockWithOtherVertexToTheRight.findAllVerticesExceptFor(targetVertexMock))
                .thenReturn(listWithVertexToTheRight);
        ArrayList<WeightedVertex> listWithVertexToTheTopRight = new ArrayList<>(Collections.singletonList(otherVertexToTheTopRightMock));
        when(forceDirectedGraphMockWithOtherVertexToTopTheRight.findAllVerticesExceptFor(targetVertexMock))
                .thenReturn(listWithVertexToTheTopRight);
        ArrayList<WeightedVertex> listWithBothVertices = new ArrayList<>(Arrays.asList(otherVertexToTheRightMock, otherVertexToTheTopRightMock));
        when(forceDirectedGraphMockWithBothOtherVertices.findAllVerticesExceptFor(targetVertexMock))
                .thenReturn(listWithBothVertices);

        final float coulombConstant = 2.3f;
        ForceCalculator<WeightedVertex, SpringLikeEdge<WeightedVertex>> coulombForceCalculator =
                new CoulombForceCalculatorDecorator<>(coulombConstant, forceCalculatorMock);

        Vector forceOfVertexToTheRightOfTargetVertex = coulombForceCalculator.calculateForceActingOnVertexFrom(targetVertexMock, forceDirectedGraphMockWithOtherVertexToTheRight);

        Vector forceOfVertexToTheTopRightOfTargetVertex = coulombForceCalculator.calculateForceActingOnVertexFrom(targetVertexMock, forceDirectedGraphMockWithOtherVertexToTopTheRight);

        Vector forceOfBothVerticesActingOnTargetVertex = coulombForceCalculator.calculateForceActingOnVertexFrom(targetVertexMock, forceDirectedGraphMockWithBothOtherVertices);

        assert forceIsDirectedToTheLeft(forceOfVertexToTheRightOfTargetVertex);
        assert forceIsOnlyHorizontally(forceOfVertexToTheRightOfTargetVertex);
        assert forceIsDirectedToTheBottomLeft(forceOfVertexToTheTopRightOfTargetVertex);

        Vector addedForcesOfBothVertices = forceOfVertexToTheRightOfTargetVertex.add(forceOfVertexToTheTopRightOfTargetVertex);
        assertEquals(addedForcesOfBothVertices, forceOfBothVerticesActingOnTargetVertex);
    }

    /**
     * BUMP this test helped find that if two vertices lie perfectly on top of each other they will exert no force on
     * each other which is not  how it is supposed to work. This test introduced some ugly code in
     * CoulombForceCalculator so that Vertices with distance zero are repelled in alternating directions. These
     * directions are fixed to the top left and bottom right! I could have implemented a small force in a random
     * direction but randomness doesn't go well with tests.
     */
    @Test
    @DisplayName("test what happens when two vertices share the same Position")
    void testSamePosition() {
        when(forceCalculatorMock.calculateForceActingOnVertexFrom(any(WeightedVertex.class), any(ForceDirectedGraph.class)))
                .thenReturn(neutralVector);

        final float coulombConstant = 3.4f;
        Vector samePosition = new Vector(0f, 0f);
        WeightedVertex vertex1 = componentFactory.createVertex(samePosition);
        WeightedVertex vertex2 = componentFactory.createVertex(samePosition);
        testedGraph.add(vertex1);
        testedGraph.add(vertex2);

        ForceCalculator<WeightedVertex, SpringLikeEdge<WeightedVertex>> coulombForceCalculator =
                new CoulombForceCalculatorDecorator<>(coulombConstant, forceCalculatorMock);
        Vector calculatedForceForVertex1 = coulombForceCalculator.calculateForceActingOnVertexFrom(vertex1, testedGraph);
        Vector calculatedForceForVertex2 = coulombForceCalculator.calculateForceActingOnVertexFrom(vertex2, testedGraph);

        assert forceIsBig(calculatedForceForVertex1);
        assert forceIsBig(calculatedForceForVertex2);

        Vector directionOfForceOnVertex1 = calculatedForceForVertex1.toUnitVector();
        Vector directionOfForceOnVertex2 = calculatedForceForVertex2.toUnitVector();

        assertNotEquals(directionOfForceOnVertex1, directionOfForceOnVertex2);
    }

    private boolean forceIsBig(Vector force) {
        return force.length() >= 1000f;
    }

    private boolean forceIsOnlyHorizontally(Vector forceVector) {
        return forceVector.getY() == 0;
    }

    private boolean forceIsDirectedToTheBottomLeft(Vector forceVector) {
        return forceVector.getX() <= 0;
    }

    private boolean forceIsDirectedToTheLeft(Vector forceVector) {
        return forceVector.getY() <= 0 && forceVector.getX() <= 0;
    }


}