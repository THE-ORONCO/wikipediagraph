package physics.forceCalculators;

import geometry.Vector;
import graph.edges.SpringLikeEdge;
import graph.verticies.WeightedVertex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import physics.ForceDirectedGraph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class SpringForceCalculatorDecoratorTest {
    Vector minusOneMinusOnePosition = new Vector(-1f, -1f);
    Vector oneOnePosition = new Vector(1f, 1f);
    Vector forceOfZero = new Vector(0f, 0f);
    Vector positionAtOrigin = new Vector(0f, 0f);
    Vector vectorialDistanceOfZero = new Vector(0f, 0f);
    Vector vectorFromOneOneToMinusOneMinusOne = new Vector(-2f, -2f);
    Vector vectorFromMinusOneMinusOneToOneOne = new Vector(2f, 2f);
    private final float weightOfFive = 5f;
    private final float springConstantOfFive = 5f;
    private final float naturalSpringLengthOfOne = 1f;
    private final float naturalSpringLengthOfZero = 0f;
    @Mock
    WeightedVertex originVertexMock;
    @Mock
    WeightedVertex targetVertexMock;
    @Mock
    ForceDirectedGraph<WeightedVertex, SpringLikeEdge<WeightedVertex>> graphMockWithTwoVertices;
    @Mock
    ConcreteForceCalculator<WeightedVertex, SpringLikeEdge<WeightedVertex>> concreteForceCalculatorMock;
    @Mock
    SpringLikeEdge<WeightedVertex> edgeMockBetweenVertices;

    @BeforeEach
    void setup() {
        originVertexMock = mock(WeightedVertex.class);
        targetVertexMock = mock(WeightedVertex.class);

        when(originVertexMock.getPosition()).thenReturn(oneOnePosition);
        when(originVertexMock.getWeight()).thenReturn(weightOfFive);
        when(originVertexMock.vectorialDistanceTo(targetVertexMock))
                .thenReturn(vectorFromOneOneToMinusOneMinusOne);

        when(targetVertexMock.getPosition()).thenReturn(minusOneMinusOnePosition);
        when(targetVertexMock.getWeight()).thenReturn(weightOfFive);
        when(targetVertexMock.vectorialDistanceTo(originVertexMock))
                .thenReturn(vectorFromMinusOneMinusOneToOneOne);

        edgeMockBetweenVertices = mock(SpringLikeEdge.class);
        when(edgeMockBetweenVertices.getNaturalSpringLength()).thenReturn(naturalSpringLengthOfOne);
        when(edgeMockBetweenVertices.getWeight()).thenReturn(weightOfFive);
        when(edgeMockBetweenVertices.getOtherVertex(originVertexMock)).thenReturn(targetVertexMock);
        when(edgeMockBetweenVertices.getOtherVertex(targetVertexMock)).thenReturn(originVertexMock);
        when(edgeMockBetweenVertices.length()).thenReturn(vectorFromMinusOneMinusOneToOneOne.length());
        when(edgeMockBetweenVertices.getOriginVertex()).thenReturn(originVertexMock);
        when(edgeMockBetweenVertices.getTargetVertex()).thenReturn(targetVertexMock);

        graphMockWithTwoVertices = mock(ForceDirectedGraph.class);
        when(graphMockWithTwoVertices.getVertices())
                .thenReturn(new ArrayList<>(Arrays.asList(originVertexMock, targetVertexMock)));
        when(graphMockWithTwoVertices.findAllEdgesConnectedTo(originVertexMock))
                .thenReturn(new ArrayList<>(Collections.singletonList(edgeMockBetweenVertices)));
        when(graphMockWithTwoVertices.findAllEdgesConnectedTo(targetVertexMock))
                .thenReturn(new ArrayList<>(Collections.singletonList(edgeMockBetweenVertices)));

        concreteForceCalculatorMock = mock(ConcreteForceCalculator.class);
        when(concreteForceCalculatorMock.calculateForceActingOnVertexFrom(any(WeightedVertex.class), any(ForceDirectedGraph.class)))
                .thenReturn(forceOfZero);
    }

    /**
     * BUMP because it turned out I calculated by getting the displacement vector of the edge witch is in only one
     * direction. The vertices need an displacement vector in different directions!!
     */
    @Test
    @DisplayName("test if the spring force is calculated correctly")
    void testCalculateForceActingOnVertexOfGraph() {
        ForceCalculator<WeightedVertex, SpringLikeEdge<WeightedVertex>> springForceCalculator =
                new SpringForceCalculatorDecorator<>(springConstantOfFive, concreteForceCalculatorMock);

        Vector forceActingOnTargetVertex = springForceCalculator
                .calculateForceActingOnVertexFrom(targetVertexMock, graphMockWithTwoVertices);
        Vector forceActingOnOriginVertex = springForceCalculator
                .calculateForceActingOnVertexFrom(originVertexMock, graphMockWithTwoVertices);

        assertEquals(forceActingOnTargetVertex, forceActingOnOriginVertex.multipliedBy(-1));
        assert forceIsDirectedTowardsOtherVertex(forceActingOnOriginVertex, originVertexMock, targetVertexMock);
        assert forceIsDirectedTowardsOtherVertex(forceActingOnTargetVertex, targetVertexMock, originVertexMock);
    }

    /**
     * BUMP because this test showed that the force calculation broke down when the vertices were on top of each other.
     * This time it was because a vector of length zero has no direction.
     */
    @Test
    @DisplayName("test if springs can handle being crushed into a length of zero")
    void testCrushingToZeroLength() {
        when(originVertexMock.getPosition()).thenReturn(positionAtOrigin);
        when(originVertexMock.vectorialDistanceTo(targetVertexMock)).thenReturn(vectorialDistanceOfZero);
        when(targetVertexMock.getPosition()).thenReturn(positionAtOrigin);
        when(targetVertexMock.vectorialDistanceTo(originVertexMock)).thenReturn(vectorialDistanceOfZero);
        when(edgeMockBetweenVertices.length()).thenReturn(naturalSpringLengthOfZero);

        ForceCalculator<WeightedVertex, SpringLikeEdge<WeightedVertex>> springForceCalculator =
                new SpringForceCalculatorDecorator<>(springConstantOfFive, concreteForceCalculatorMock);
        Vector forceActingOnTargetVertex = springForceCalculator
                .calculateForceActingOnVertexFrom(targetVertexMock, graphMockWithTwoVertices);
        Vector forceActingOnOriginVertex = springForceCalculator
                .calculateForceActingOnVertexFrom(originVertexMock, graphMockWithTwoVertices);

        assert forceActingOnOriginVertex.length() > 0;
        assert forceActingOnTargetVertex.length() > 0;
    }

    private boolean forceIsDirectedTowardsOtherVertex(Vector forceOnVertex1, WeightedVertex vertex1, WeightedVertex vertex2) {
        Vector forceDirection = forceOnVertex1.toUnitVector();
        Vector vectorFromVertex1ToVertex2 = vertex1.vectorialDistanceTo(vertex2);
        Vector directionFromVertex1ToVertex2 = vectorFromVertex1ToVertex2.toUnitVector();
        return vectorsAreEqualWithinError(forceDirection, directionFromVertex1ToVertex2, 0.00001f);
    }

    private boolean vectorsAreEqualWithinError(Vector v1, Vector v2, float epsilon) {
        return v1.getX() <= v2.getX() + epsilon
                && v1.getX() >= v2.getX() - epsilon
                && v1.getY() <= v2.getY() + epsilon
                && v1.getY() >= v2.getY() - epsilon;
    }


}