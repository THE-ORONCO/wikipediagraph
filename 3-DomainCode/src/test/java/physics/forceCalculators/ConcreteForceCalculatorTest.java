package physics.forceCalculators;

import geometry.Vector;
import graph.edges.SpringLikeEdge;
import graph.verticies.WeightedVertex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import physics.ForceDirectedGraph;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

class ConcreteForceCalculatorTest {
    private ConcreteForceCalculator<WeightedVertex, SpringLikeEdge<WeightedVertex>> testedForceCalculator;
    private final Vector forceOfZero = new Vector(0f, 0f);
    @Mock
    private WeightedVertex mockedVertex;
    @Mock
    private ForceDirectedGraph<WeightedVertex, SpringLikeEdge<WeightedVertex>> mockedGraph;

    @BeforeEach
    void setup() {
        testedForceCalculator = new ConcreteForceCalculator<>();
        mockedVertex = mock(WeightedVertex.class);
        mockedGraph = mock(ForceDirectedGraph.class);
    }

    @Test
    @DisplayName("test if the ConcreteForceCalculator returns a force equal to zero")
    void calculateForceActingOnVertexFrom() {
        Vector calculatedForce = testedForceCalculator.calculateForceActingOnVertexFrom(mockedVertex, mockedGraph);
        assertEquals(calculatedForce, forceOfZero);
    }
}