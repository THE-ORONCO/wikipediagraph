package physics.forceCalculators;

import geometry.Vector;
import graph.edges.SpringLikeEdge;
import graph.verticies.WeightedVertex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import physics.ForceDirectedGraph;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ForceOfGravityCalculatorDecoratorTest {
    @Mock
    ForceCalculator<WeightedVertex, SpringLikeEdge<WeightedVertex>> forceCalculatorMock = mock(ForceCalculator.class);
    @Mock
    WeightedVertex targetVertexMockAtCenter = mock(WeightedVertex.class);
    @Mock
    WeightedVertex targetVertexMockAtOneOne = mock(WeightedVertex.class);
    @Mock
    WeightedVertex targetVertexMockTwoToTheRight = mock(WeightedVertex.class);

    @Mock
    ForceDirectedGraph<WeightedVertex, SpringLikeEdge<WeightedVertex>> forceDirectedGraphMock = mock(ForceDirectedGraph.class);

    Vector neutralVector = new Vector(0f, 0f);
    Vector centerPosition = new Vector(0f, 0f);
    Vector oneOnePosition = new Vector(1f, 1f);
    Vector twoToTheRightPosition = new Vector(2f, 0f);

    @BeforeEach
    public void setUp() {
        when(forceCalculatorMock.calculateForceActingOnVertexFrom(targetVertexMockAtCenter, forceDirectedGraphMock)).thenReturn(neutralVector);
        when(forceCalculatorMock.calculateForceActingOnVertexFrom(targetVertexMockAtOneOne, forceDirectedGraphMock)).thenReturn(neutralVector);
        when(forceCalculatorMock.calculateForceActingOnVertexFrom(targetVertexMockTwoToTheRight, forceDirectedGraphMock)).thenReturn(neutralVector);

        when(targetVertexMockAtCenter.getWeight()).thenReturn(1f);
        when(targetVertexMockAtOneOne.getWeight()).thenReturn(1f);
        when(targetVertexMockTwoToTheRight.getWeight()).thenReturn(1f);

        when(targetVertexMockAtCenter.getPosition()).thenReturn(centerPosition);
        when(targetVertexMockAtOneOne.getPosition()).thenReturn(oneOnePosition);
        when(targetVertexMockTwoToTheRight.getPosition()).thenReturn(twoToTheRightPosition);
    }

    @Test
    void calculateForceActingOnVertexOfGraph() {
        float gravitationalAcceleration = 9.81f;
        ForceCalculator<WeightedVertex, SpringLikeEdge<WeightedVertex>> forceCalculator =
                new ForceOfGravityCalculatorDecorator<>(gravitationalAcceleration, forceCalculatorMock);

        Vector gravitationalForceOnCenterVertex = forceCalculator.calculateForceActingOnVertexFrom(targetVertexMockAtCenter, forceDirectedGraphMock);
        Vector gravitationalForceOnOneOneVertex = forceCalculator.calculateForceActingOnVertexFrom(targetVertexMockAtOneOne, forceDirectedGraphMock);
        Vector gravitationalForceOnTwoToTheRightVertex = forceCalculator.calculateForceActingOnVertexFrom(targetVertexMockTwoToTheRight, forceDirectedGraphMock);

        assert forceIsZero(gravitationalForceOnCenterVertex);
        assert forceIsDirectedToTheLeft(gravitationalForceOnTwoToTheRightVertex);
        assert forceIsDirectedToTheBottomLeft(gravitationalForceOnOneOneVertex);
    }

    private boolean forceIsDirectedToTheBottomLeft(Vector forceVector) {
        return forceVector.getX() <= 0 && forceVector.getY() <= 0;
    }

    private boolean forceIsDirectedToTheLeft(Vector forceVector) {
        return forceVector.getX() <= 0;
    }

    private boolean forceIsZero(Vector forceVector) {
        return forceVector.getX() == 0 && forceVector.getY() == 0;
    }
}