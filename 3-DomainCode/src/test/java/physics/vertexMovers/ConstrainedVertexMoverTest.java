package physics.vertexMovers;

import factories.vertices.WeightedVertexFactory;
import geometry.Vector;
import graph.verticies.Vertex;
import graph.verticies.WeightedVertex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ConstrainedVertexMoverTest {
    private final float dampeningCoefficient = 0.3f;
    private final float xBound = 10f;
    private final float yBound = 10f;
    private final float timeStep = 10f;
    private final Vector positionInBounds = new Vector(0f, 0f);
    private final Vector positionOutOfBounds = new Vector(xBound + 10f, yBound + 10f);
    private final Vector positionAtTheRightBound = new Vector(xBound, 0f);
    private final Vector velocityToTheRight = new Vector(1f, 0f);

    private WeightedVertex testVertex;
    private WeightedVertexFactory vertexFactory;
    private ConstrainedVertexMover constrainedVertexMover;

    @BeforeEach
    void setup() {
        vertexFactory = new WeightedVertexFactory();
    }

    @Test
    @DisplayName("test if a vertex is moved correctly")
    void moveVertexAccordingToHisCurrentVelocity() {
        testVertex = (WeightedVertex) vertexFactory.createVertex(positionInBounds);
        testVertex.setVelocity(velocityToTheRight);
        Vector originalPosition = testVertex.getPosition();
        Vector originalVelocity = testVertex.getVelocity();

        constrainedVertexMover = new ConstrainedVertexMover(dampeningCoefficient, xBound, yBound);
        constrainedVertexMover.moveVertexAccordingToHisCurrentVelocityOverTimeStep(testVertex, timeStep);

        Vector newPosition = testVertex.getPosition();
        Vector dampenedVelocity = testVertex.getVelocity();

        assert originalPosition.getX() <= newPosition.getX();
        assert originalPosition.getY() <= newPosition.getY();

        assert originalVelocity.getX() >= dampenedVelocity.getX();
        assert originalVelocity.getY() >= dampenedVelocity.getY();
    }

    @Test
    @DisplayName("test if a vertex stays within it's boundaries when moved")
    void moveVertexAccordingToHisCurrentVelocityWithinBounds() {
        testVertex = (WeightedVertex) vertexFactory.createVertex(positionAtTheRightBound);
        testVertex.setVelocity(velocityToTheRight);
        constrainedVertexMover = new ConstrainedVertexMover(dampeningCoefficient, xBound, yBound);

        constrainedVertexMover.moveVertexAccordingToHisCurrentVelocityOverTimeStep(testVertex, timeStep);

        assert positionedWithinBounds(testVertex);
    }

    @Test
    @DisplayName("test if a vertex is moved into bounds if he is outside")
    void moveVertexInsideBounds() {
        testVertex = (WeightedVertex) vertexFactory.createVertex(positionOutOfBounds);
        testVertex.setVelocity(velocityToTheRight);
        constrainedVertexMover = new ConstrainedVertexMover(dampeningCoefficient, xBound, yBound);

        assert !positionedWithinBounds(testVertex);

        constrainedVertexMover.moveVertexAccordingToHisCurrentVelocityOverTimeStep(testVertex, timeStep);

        assert positionedWithinBounds(testVertex);

    }

    private boolean positionedWithinBounds(Vertex position) {
        boolean withinXBounds = position.getX() >= -xBound && position.getX() <= xBound;
        boolean withinYBounds = position.getY() >= -yBound && position.getY() <= yBound;

        return withinXBounds && withinYBounds;
    }
}