package physics.vertexMovers;

import geometry.Vector;
import graph.verticies.WeightedVertex;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VertexMoverTest {

    @Test
    @DisplayName("test if a weighted vertex is moved correctly")
    void moveVertexAccordingToItsCurrentVelocity() {
        float dampingCoefficient = 0.2f;
        float timeStep = 2f;
        VertexMover vertexMover = new VertexMover(dampingCoefficient);

        Vector originalVertexPosition = new Vector(0f, 0f);
        float vertexWeight = 5f;
        Vector originalVelocity = new Vector(5f, 10f);
        WeightedVertex vertex = new WeightedVertex(originalVertexPosition, vertexWeight);
        vertex.setVelocity(originalVelocity);

        vertexMover.moveVertexAccordingToHisCurrentVelocityOverTimeStep(vertex, timeStep);
        Vector firstVertexPosition = vertex.getPosition();
        Vector firstDampenedVelocity = vertex.getVelocity();

        vertexMover.moveVertexAccordingToHisCurrentVelocityOverTimeStep(vertex, timeStep);
        Vector secondVertexPosition = vertex.getPosition();
        Vector secondDampenedVelocity = vertex.getVelocity();

        Vector deltaBetweenOriginalAndFirstPosition = firstVertexPosition.subtract(originalVertexPosition);
        float firstDistanceMoved = deltaBetweenOriginalAndFirstPosition.length();
        Vector deltaBetweenSecondAndFirstPosition = secondVertexPosition.subtract(firstVertexPosition);
        float secondDistanceMoved = deltaBetweenSecondAndFirstPosition.length();

        assertTrue(firstDistanceMoved > secondDistanceMoved);

        assertTrue(originalVelocity.length() > firstDampenedVelocity.length());
        assertTrue(firstDampenedVelocity.length() > secondDampenedVelocity.length());

        assertNotEquals(originalVertexPosition, firstVertexPosition);
        assertNotEquals(firstVertexPosition, secondVertexPosition);
        assertNotEquals(secondVertexPosition, originalVertexPosition);
    }

    @Test
    @DisplayName("test if weighted vertex is not moved if there is no velocity")
    void moveVertexAccordingToItsCurrentVelocityWhichIsZero() {
        float dampingCoefficient = 0.2f;
        float timeStep = 2f;
        VertexMover vertexMover = new VertexMover(dampingCoefficient);

        Vector originalPosition = new Vector(5f, 3f);
        float vertexWeight = 5f;
        Vector velocityOfZero = new Vector(0f, 0f);
        WeightedVertex vertex = new WeightedVertex(originalPosition, vertexWeight);
        vertex.setVelocity(velocityOfZero);

        vertexMover.moveVertexAccordingToHisCurrentVelocityOverTimeStep(vertex, timeStep);
        Vector newPosition = vertex.getPosition();

        assertEquals(originalPosition, newPosition);
    }
}