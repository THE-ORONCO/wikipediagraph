package physics;

import factories.GraphComponentFactory;
import factories.PhysicsGraphSimulationFactory;
import factories.forcedirectedComponents.DefaultForceDirectedGraphComponentFactory;
import geometry.Vector;
import graph.Graph;
import graph.edges.SpringLikeEdge;
import graph.verticies.WeightedVertex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import physics.forceCalculators.ForceCalculator;
import physics.vertexMovers.VertexMover;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class PhysicsGraphSimulationTest {

    @Mock
    private Graph<WeightedVertex, SpringLikeEdge<WeightedVertex>> graphMock;
    private final float timeStep = 10;
    @Mock
    private ForceCalculator<WeightedVertex, SpringLikeEdge<WeightedVertex>> forceCalculatorMock;
    @Mock
    private ForceApplier forceApplierMock;
    @Mock
    private VertexMover vertexMoverMock;

    private PhysicsGraphSimulation<WeightedVertex, SpringLikeEdge<WeightedVertex>> testedSimulation;

    @BeforeEach
    void setup() {
        PhysicsGraphSimulationFactory<WeightedVertex, SpringLikeEdge<WeightedVertex>> factory = new PhysicsGraphSimulationFactory<>();
        GraphComponentFactory<WeightedVertex, SpringLikeEdge<WeightedVertex>> componentFactory = new DefaultForceDirectedGraphComponentFactory();
        List<WeightedVertex> vertices = new ArrayList<>() {{
            add(componentFactory.createVertex(1f, 1f));
            add(componentFactory.createVertex(2f, 2f));
            add(componentFactory.createVertex(2f, 2f));
        }};

        graphMock = mock(Graph.class);
        when(graphMock.getVertices()).thenReturn(vertices);

        forceCalculatorMock = mock(ForceCalculator.class);
        Vector forceOfZero = new Vector(0f, 0f);
        when(forceCalculatorMock.calculateForceActingOnVertexFrom(any(WeightedVertex.class), eq(graphMock))).thenReturn(forceOfZero);

        forceApplierMock = mock(ForceApplier.class);

        vertexMoverMock = mock(VertexMover.class);

        testedSimulation = factory.create(graphMock, timeStep, forceCalculatorMock, forceApplierMock, vertexMoverMock);
    }

    @Test
    @DisplayName("test if the physics simulation is calling the appropriate delegation objects to calculate the force")
    void testStep() {
        testedSimulation.step();

        verify(forceCalculatorMock, atLeast(3)).calculateForceActingOnVertexFrom(any(WeightedVertex.class), eq(graphMock));
        verify(forceCalculatorMock, atLeast(3)).calculateForceActingOnVertexFrom(any(WeightedVertex.class), eq(graphMock));
        verify(forceCalculatorMock, atLeast(3)).calculateForceActingOnVertexFrom(any(WeightedVertex.class), eq(graphMock));
        verify(forceApplierMock, atLeast(3)).applyForceToVertexOverTimeStep(any(Vector.class), any(WeightedVertex.class), eq(timeStep));
    }
}