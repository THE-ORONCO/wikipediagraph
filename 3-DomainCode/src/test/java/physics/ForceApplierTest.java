package physics;

import geometry.Vector;
import graph.verticies.WeightedVertex;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;


class ForceApplierTest {
    @Test
    @DisplayName("test if force is correctly applied to a vertex")
    void applyForceToVertexOverTimeStep() {
        float vertexWeight = 10f;
        Vector vertexPosition = new Vector(0f, 0f);
        WeightedVertex vertex = new WeightedVertex(vertexPosition, vertexWeight);
        Vector originalVelocity = vertex.getVelocity();

        Vector forceToBeAppliedToVertex = new Vector(5f, 5f);
        float overTimeStep = 2f;

        /* this comment contains all formulas needed for the impulse-momentum change theorem
         * F        = m * a
         * F        = m * (Δv / t)
         * daraus Folgt
         * F * t    =   m * Δv
         * I        =   change in momentum
         * Impulse  =   change in momentum
         *
         * but we want to know if the change in velocity was calculated correctly thus we solve for Δv
         * Δv       =   (F * t) / m
         *          =   I / m
         */
        Vector impulseAppliedToVertex = forceToBeAppliedToVertex.multipliedBy(overTimeStep);
        Vector expectedVelocityChange = impulseAppliedToVertex.dividedBy(vertexWeight);

        ForceApplier forceApplier = new ForceApplier();
        forceApplier.applyForceToVertexOverTimeStep(forceToBeAppliedToVertex, vertex, overTimeStep);
        Vector newVelocity = vertex.getVelocity();

        Vector actualVelocityChange = newVelocity.subtract(originalVelocity);

        float tolerance = 0.0001f;
        assertTrue(approximatelyEqual(expectedVelocityChange.getX(), actualVelocityChange.getX(), tolerance));
        assertTrue(approximatelyEqual(expectedVelocityChange.getY(), actualVelocityChange.getY(), tolerance));
    }

    private boolean approximatelyEqual(float value, float referenceValue, float tolerance) {
        float lowestAcceptableValue = referenceValue - tolerance;
        float largestAcceptableValue = referenceValue + tolerance;
        return lowestAcceptableValue <= value && value <= largestAcceptableValue;
    }
}