package physics;

import factories.forcedirectedComponents.DefaultForceDirectedGraphComponentFactory;
import factories.forcedirectedComponents.ForceDirectedGraphFactory;
import graph.edges.SpringLikeEdge;
import graph.verticies.WeightedVertex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ForceDirectedGraphTest {
    private ForceDirectedGraph<WeightedVertex, SpringLikeEdge<WeightedVertex>> testedGraph;

    private WeightedVertex targetVertex;
    private WeightedVertex connectedVertex;
    private WeightedVertex lonelyVertex1;
    private WeightedVertex lonelyVertex2;
    private List<WeightedVertex> neighbourTestVertices;
    private WeightedVertex notContainedVertex;

    private SpringLikeEdge<WeightedVertex> edgeFromTargetToConnectedVertex;
    private SpringLikeEdge<WeightedVertex> edgeFromConnectedVertexToTarget;
    private SpringLikeEdge<WeightedVertex> lonelyEdge;
    private List<SpringLikeEdge<WeightedVertex>> neighbourTestEdges;
    private SpringLikeEdge<WeightedVertex> notContainedEdge;

    @BeforeEach
    void setup() {
        ForceDirectedGraphFactory<WeightedVertex, SpringLikeEdge<WeightedVertex>> graphFactory = new ForceDirectedGraphFactory<>();
        testedGraph = graphFactory.create();
        DefaultForceDirectedGraphComponentFactory componentFactory = new DefaultForceDirectedGraphComponentFactory();

        targetVertex = componentFactory.createVertex(0f, 0f);
        connectedVertex = componentFactory.createVertex(1f, 0f);
        lonelyVertex1 = componentFactory.createVertex(1f, 3f);
        lonelyVertex2 = componentFactory.createVertex(3f, 3f);
        notContainedVertex = componentFactory.createVertex(5f, 5f);

        neighbourTestVertices = new ArrayList<>() {{
            add(targetVertex);
            add(connectedVertex);
            add(lonelyVertex1);
            add(lonelyVertex2);
        }};
        edgeFromTargetToConnectedVertex = componentFactory.createEdge(targetVertex, connectedVertex);
        edgeFromConnectedVertexToTarget = componentFactory.createEdge(connectedVertex, targetVertex);
        lonelyEdge = componentFactory.createEdge(lonelyVertex1, lonelyVertex2);
        notContainedEdge = componentFactory.createEdge(componentFactory.createVertex(10f, 10f),
                componentFactory.createVertex(11f, 11f));

        neighbourTestEdges = new ArrayList<>() {{
            add(edgeFromTargetToConnectedVertex);
            add(edgeFromConnectedVertexToTarget);
            add(lonelyEdge);
        }};
    }

    /**
     * BUMP because it made me realize that the check/conversion of the vertices worked the wrong way
     */
    @Test
    @DisplayName("test if vertices are added correctly")
    void testAddVertices() {
        assertFalse(testedGraph.getVertices().contains(targetVertex));

        testedGraph.add(targetVertex);

        assertTrue(testedGraph.contains(targetVertex));
        assertFalse(testedGraph.contains(notContainedVertex));
    }

    @Test
    @DisplayName("test if edges are added correctly")
    void testAddEdges() {
        assertFalse(testedGraph.contains(edgeFromTargetToConnectedVertex));

        testedGraph.add(edgeFromTargetToConnectedVertex);

        assertTrue(testedGraph.contains(edgeFromTargetToConnectedVertex));
        assertFalse(testedGraph.contains(notContainedEdge));
    }

    @Test
    @DisplayName("test if vertices which are not in the graph are added when they are contained in an edge that is added")
    void testAddVerticesInEdge() {
        assertFalse(testedGraph.contains(targetVertex));
        assertFalse(testedGraph.contains(connectedVertex));
        assertFalse(testedGraph.contains(edgeFromTargetToConnectedVertex));

        testedGraph.add(edgeFromTargetToConnectedVertex);

        assertTrue(testedGraph.contains(edgeFromTargetToConnectedVertex));
        assertTrue(testedGraph.contains(targetVertex));
        assertTrue(testedGraph.contains(connectedVertex));
    }

    @Test
    @DisplayName("test if vertices are removed correctly")
    void testRemoveVertices() {
        testedGraph.add(targetVertex);
        testedGraph.add(lonelyVertex1);
        assertTrue(testedGraph.contains(targetVertex));
        assertTrue(testedGraph.contains(lonelyVertex1));

        testedGraph.remove(targetVertex);

        assertFalse(testedGraph.contains(targetVertex));
        assertTrue(testedGraph.contains(lonelyVertex1));
    }

    @Test
    @DisplayName("test if edges are removed correctly")
    void testRemoveEdge() {
        testedGraph.add(targetVertex);
        testedGraph.add(connectedVertex);
        testedGraph.add(edgeFromTargetToConnectedVertex);

        assertTrue(testedGraph.contains(edgeFromTargetToConnectedVertex));

        testedGraph.remove(edgeFromTargetToConnectedVertex);

        assertFalse(testedGraph.contains(edgeFromConnectedVertexToTarget));
        assertTrue(testedGraph.contains(targetVertex));
        assertTrue(testedGraph.contains(connectedVertex));
    }

    @Test
    @DisplayName("test if edge is removed when a connected vertex is removed")
    void testRemoveVerticesAndConnectedEdges() {
        testedGraph.add(targetVertex);
        testedGraph.add(connectedVertex);
        testedGraph.add(lonelyVertex1);
        testedGraph.add(lonelyVertex2);
        testedGraph.add(edgeFromTargetToConnectedVertex);

        assertTrue(testedGraph.contains(edgeFromTargetToConnectedVertex));

        testedGraph.remove(targetVertex);

        assertFalse(testedGraph.contains(targetVertex));
        assertTrue(testedGraph.contains(connectedVertex));
        assertTrue(testedGraph.contains(lonelyVertex1));
        assertTrue(testedGraph.contains(lonelyVertex2));
        assertFalse(testedGraph.getEdges().contains(lonelyEdge));
    }

    @Test
    @DisplayName("test if an edge between two given vertices can be found")
    void testFindEdgesBetween() {
        testedGraph.add(targetVertex);
        testedGraph.add(connectedVertex);
        testedGraph.add(edgeFromTargetToConnectedVertex);
        testedGraph.add(edgeFromConnectedVertexToTarget);
        testedGraph.add(lonelyEdge);

        List<SpringLikeEdge<WeightedVertex>> edgesBetweenVertices = testedGraph.findEdgesBetween(targetVertex, connectedVertex);
        assertEquals(2, edgesBetweenVertices.size());
        assertTrue(edgesBetweenVertices.contains(edgeFromTargetToConnectedVertex));
        assertTrue(edgesBetweenVertices.contains(edgeFromConnectedVertexToTarget));
        assertFalse(edgesBetweenVertices.contains(lonelyEdge));
    }

    @Test
    @DisplayName("test if all edges connected to a vertex can be found")
    void testFindAllEdgesConnectedTo() {
        neighbourTestVertices.forEach(testedGraph::add);
        neighbourTestEdges.forEach(testedGraph::add);

        List<SpringLikeEdge<WeightedVertex>> connectedEdges = testedGraph.findAllEdgesConnectedTo(targetVertex);

        assertTrue(connectedEdges.contains(edgeFromConnectedVertexToTarget));
        assertTrue(connectedEdges.contains(edgeFromTargetToConnectedVertex));
        assertFalse(connectedEdges.contains(lonelyEdge));
    }

    @Test
    @DisplayName("test if all edges coming from a vertex can be count")
    void testFindAllEdgesOriginatingFrom() {
        neighbourTestVertices.forEach(testedGraph::add);
        neighbourTestEdges.forEach(testedGraph::add);

        List<SpringLikeEdge<WeightedVertex>> connectedEdges = testedGraph.findAllEdgesOriginatingFrom(targetVertex);

        assertTrue(connectedEdges.contains(edgeFromTargetToConnectedVertex));
        assertFalse(connectedEdges.contains(edgeFromConnectedVertexToTarget));
        assertFalse(connectedEdges.contains(lonelyEdge));
    }

    @Test
    @DisplayName("test if all edges going to a vertex can be count")
    void testFindSAllEdgesGoingTo() {
        neighbourTestEdges.forEach(testedGraph::add);
        neighbourTestVertices.forEach(testedGraph::add);

        List<SpringLikeEdge<WeightedVertex>> connectedEdges = testedGraph.findAllEdgesGoingTo(targetVertex);

        assertTrue(connectedEdges.contains(edgeFromConnectedVertexToTarget));
        assertFalse(connectedEdges.contains(edgeFromTargetToConnectedVertex));
        assertFalse(connectedEdges.contains(lonelyEdge));
    }

    /**
     * BUMP through this test a shoddy implementation of the neighbour finding was found
     */
    @Test
    @DisplayName("test if all neighboring vertices of a vertex can be found")
    void testFindAllNeighbouringVertices() {
        neighbourTestVertices.forEach(testedGraph::add);
        neighbourTestEdges.forEach(testedGraph::add);

        List<WeightedVertex> neighbouringVertices = testedGraph.findAllNeighbouringVertices(targetVertex);

        assertFalse(neighbouringVertices.contains(targetVertex));
        assertTrue(neighbouringVertices.contains(connectedVertex));
        assertFalse(neighbouringVertices.contains(lonelyVertex1));
        assertFalse(neighbouringVertices.contains(lonelyVertex2));
    }

    @Test
    @DisplayName("test if all vertices except for a given vertex can be found")
    void testFindAllVerticesExceptFor() {
        neighbourTestVertices.forEach(testedGraph::add);
        neighbourTestEdges.forEach(testedGraph::add);

        List<WeightedVertex> neighbouringVertices = testedGraph.findAllVerticesExceptFor(targetVertex);

        assertFalse(neighbouringVertices.contains(targetVertex));
        assertTrue(neighbouringVertices.contains(connectedVertex));
        assertTrue(neighbouringVertices.contains(lonelyVertex1));
        assertTrue(neighbouringVertices.contains(lonelyVertex2));
    }
}