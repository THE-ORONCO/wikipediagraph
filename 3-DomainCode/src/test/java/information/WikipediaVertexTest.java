package information;

import geometry.Vector;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;


class WikipediaVertexTest {

    private final float atXOne = 1f;
    private final float andYOne = 1f;
    private final Vector atOneOnePosition = new Vector(atXOne, andYOne);
    private final float withWeightOfOne = 1f;
    @Mock
    private WikipediaArticle assotiatedWithArticle;

    @BeforeEach
    void setup() {
        assotiatedWithArticle = mock(WikipediaArticle.class);
    }

    @Test
    @DisplayName("test if the creation of an wikipedia article vertex works correctly so that vertices with all creation" +
            " methods are created the same despite giving them the same parameters through different means")
    void testCreation() {
        WikipediaVertex wikiVertex1 = new WikipediaVertex(atXOne, andYOne, withWeightOfOne, assotiatedWithArticle);
        WikipediaVertex wikiVertex2 = new WikipediaVertex(atOneOnePosition, withWeightOfOne, assotiatedWithArticle);

        assertEquals(wikiVertex1.getPosition(), wikiVertex2.getPosition());
        assertEquals(wikiVertex1.getX(), wikiVertex2.getX());
        assertEquals(wikiVertex1.getY(), wikiVertex2.getY());
        assertEquals(wikiVertex1.getKineticEnergy(), wikiVertex2.getKineticEnergy());
        assertEquals(wikiVertex1.getVelocity(), wikiVertex2.getVelocity());
        assertEquals(wikiVertex1.getWeight(), wikiVertex2.getWeight());
        assertEquals(wikiVertex1.getAssociatedArticle(), wikiVertex2.getAssociatedArticle());
    }
}