# Wikipediagraph [![pipeline status](https://gitlab.com/THE-ORONCO/wikipediagraph/badges/master/pipeline.svg)](https://gitlab.com/THE-ORONCO/wikipediagraph/-/commits/master) 

This is an application created in the scope of a software engineering lecture at the DHBW Karlsruhe.

The goal of this application is to visually represent Wikipedia and the connection of its articles
as a force directed graph.

## Building
This project was created with MAVEN 3.8.1 and JDK 11 in mind. So you should be fine if your software versions are the 
same or higher.

`mvn clean install` will build the project. To get the jar output in the top most "target" directory "./target" you 
currently have to run it twice.

If you aren't scared of some digging you can find a working software build in "./0-Plugins/target" after just one run of 
`mvn clean install`. This is because the copying of the output jar does not work with the first run for some reason.

## Running
To run the Program just run the jar file that is created by maven (see **Building**).  

The command `java -jar 0-Plugins/target/0-Plugins-1.0-jar-with-dependencies.jar` should do the trick.

If you want to search for articles in another language (not english) or maybe even in another wiki 
(not wikipedia (!!! NOT TESTED !!!)) you can supply the api-endpoint to the program by giving it as an optional command
line parameter.

To use the german wikipedia for example you could use the command 

`java -jar 0-Plugins/target/0-Plugins-1.0-jar-with-dependencies.jar "https://en.wikipedia.org/w/api.php"`

When no parameter is given, the english wikipedia will be queried.

## Testing
To test the whole Project just rung `mvn test`. This should test the whole Project at once. To see concrete results look
into the "./*/target/surefire-reports/" folders of each domain for more information.

## HowTo
A few seconds after starting the software you will be greeted by the GUI. In the middle you will see the viewport where 
the graph will be displayed as soon as you load some articles. Everything in the viewport can be dragged and zoomed via
the mouse, and the mouse wheel. If you scroll too far off to the side or want to return to the original view for what 
ever reason you can always press the reset button which brings you back to the original view.  
On the right side of the GUI you will see a search bar where you can search for articles by either their title or id 
(to search for the ID you need to check the checkbox). As soon as enter, or the `search` button is pressed the query will 
be sent to Wikipedia (currently this is still not asynchronous and freezes the GUI a little while). The queried articles 
will be displayed by their title in the list below and can be added to the graph by selecting them and clicking 
`show selected`.  
To automatically orient the graph you can press the start button on the viewport. This will start a physics simulation 
of the graph in a parallel thread. If the graph gets too crowded, it is always possible to hide some vertices by 
selecting their associated article titles in the list on the right and clicking on `hide selected`.  
On the left there is a small overview of a selected article. When at least one article is loaded, you will be able to
select a specific article to inspect with the combobox on the very top of the left panel. This combobox can also be 
typed in to easier find your article when many are loaded. Below this combobox the thumbnail of the article (if it has 
one), a small extract (first 10 sentences), the articles this article links to, and the links to external sources will 
be displayed.  
To load articles the selected article links to you can just select them in the list below the extract and click on the 
`load selected articles` button.

## TODO
This is a list of possible ways this project could be improved upon.
- [ ] add `search and show` and `load and show` for the right and left panel respectively (better usability) 
- [ ] asynchronous loading of the articles (probably with the subscription construct in the Application Code)
    - [ ] implement the loading in parallel
    - [ ] make a loading bar for the queries
    - [ ] automatically add the articles to the list of loaded articles
- [ ] better performance in the viewport (it tanks even with only 30 Vertices)
- [ ] user-graph interaction
    - [ ] make the graph draggable
        - [ ] make vertices selectable
            - [ ] highlight the vertices when the mouse is over them
            - [ ] make the vertices clickable (the article should be displayed)
            - [ ] add keyboard interactions (linke delete for hiding the vertices and holding ctrl to select multiple)
            - [ ] multi selection through dragging
        - [ ] implement a moving system for the vertices (maybe another VertexMover that can take inputs from outside 
          for specified vertices)
        - [ ] dragging on vertices now moves them and not the background
    - [ ] double-clicking on vertex opens the article
    - [ ] dragging the titles in either the left or right list into the viewport adds them to the graph (and loads them 
      if they are not already loaded)
- [ ] rip out the physics stuff and make it its own plugin
    - [ ] create Physics plugin
    - [ ] make all the stuff injectable
    - [ ] extract the specific graph implementations --> wikipedia stuff now only holds references to the most basic version to a graph with its nodes and edges. Specific instances of the needed objects will be created via injectable factories.
- [ ] rewrite the whole thing with JavaScript and React because it is porbably easier that way
          
    

